<?php
/**
 * Classe d'abstraction de base de données
 *
 * génération de requêtes
 * exploitation de BDD : CRUD (create, read, update, delete)
 * administration de BDD (sauvegarde)
 * ...
 *
*/

    namespace Romualb\RbtClasses;
	use \PDO as PDO;

	class classeSqlPdo
	{

		// restrictions
		protected 	$basesInterdites = array(			// bases interdites pour cette classe
			"information_schema",
			"mysql",
			);

		// initialisation des paramètres de connexion
		protected   $driver="mysql";					// driver (mysql,...)
		protected 	$host="";							// adresse hôte
		protected 	$user="";							// utilisateur
		protected 	$pwd="";							// mot de passe de l'utilisateur
		protected 	$db="";								// base de données courante
		protected 	$table="";							// table courante

		// initialisation de la connexion
		protected 	$linkId=false;						// identifiant de connection retourné par la fonction connect (ressource)
		protected 	$pconnexion=false;					// connexion non persistante par défaut
		protected 	$res=false;							// résultat de la requete (ressource)

		// initialisation des variables de requête
		protected 	$newBase=array();					// nom de la base à créer
		protected 	$tablesSelect=array();				// liste des tables concernées par un SELECT
		protected 	$tableUpdate="";					// nom de la table concernée par un UPDATE
		protected 	$tableInsert="";					// nom de la table concernée par un INSERT
		protected 	$tableDelete="";					// nom de la table concernée par un DELETE
		protected 	$newTable=array();					// nom de la table à créer
		protected 	$tablesJoin=array();				// liste des tables concernées par les JOIN
		public 	  	$champsSelect=array();				// liste des champs du SELECT
		protected 	$varSqlCalcFoundRows=true;			// activation du SQL_CALC_FOUND_ROWS
		protected 	$varSqlStraightJoin=false;			// activation du STRAIGHT_JOIN
		protected 	$varOnDuplicateKey="ERROR";			// activation du ON DUPLICATE KEY lors d'insertion
		protected 	$noDuplicateKey = array();			// champ à ne pas dupliquer dans le ON DUPLICATE KEY
		protected 	$values=array();					// liste des couples champs/valeur mis à jour par le SET (INSERT, UPDATE)
		protected   $multiplesValues = [];				// liste de valeurs multiples
		protected 	$joins=array();						// liste des requetes de jointure
		protected 	$where="";							// condition WHERE
		protected 	$whereOk="";
		protected 	$group="";							// condition GROUP BY
		protected 	$order=array();						// liste des ORDER BY
		protected 	$limit="";							// bornes du LIMIT
		protected 	$having="";							// condition HAVING
		protected 	$curWhere="";						// WHERE courant
		protected 	$curOrder="";						// ORDER BY courant
		protected 	$curGroup="";						// GROUP BY courant
		protected 	$curJoin="";						// JOIN courant
		protected 	$listeChamps="";					// suite champs pour insertion multiple (val1, val2...)
		protected 	$listeValues="";					// suite valeurs pour insertion multiple (val1, val2...)
		protected 	$colonnes=array();					// liste de colonnes pour CREATE
		protected 	$withRollup = false;				// ajout de WIDTH ROLLUP
		protected 	$exportToFile = false;				// export du résultat vers un fichier
		protected 	$withColNames = false;				// ajout du nom des colonnes dans le résultat (pour export CSV par exemple)
		protected 	$exportFileName = "";				// nom du fichier export
		public 		$sql="";							// requête SQL générée
		protected 	$tagSql = "";						// tag sur la requete
		protected 	$primaryKey = null;					// clé primaire
		protected 	$valPrimaryKey = null;				// valeur de la clé primaire

		protected 	$preSql = "";						// requête préformatée pour affichage HTML
		protected 	$formatDisplaySql = "PRE";			// affichage preformaté par défaut

		// initialisation des variables de résultat de la requête
		public 		$result=array();					// lignes du résultat de la requête
		protected 	$numRes=0;							// ligne de résultat courante
		protected 	$nbResults=0;						// nombre de résultats de la requête
		protected 	$nbLignesAffectees=0;				// nombre de lignes affectées par la dernière requête
		protected 	$nbInsertsMax=0;					// nombre d'insertions attendues
		protected 	$nbInserts=0;						// nombre d'insertions réalisées

		// autres
		private 	$parenthesage=0;					// niveau de parenthésage
		private 	$max_allowed_packet=0;				// taille max d'une requete
		private 	$afficheErreurs=false;				// affichage des erreurs SQL
		protected 	$displaySql=false;					// ecriture de la requete
		private 	$executeSql=true;					// execution de la requete lors de update(), select()...
		private 	$messageNoExecuteSql=true;			// affichage du message "Requête non executée"

		protected 	$quotes=false;						// entoure les valeurs par des quotes

		// sous requetes
		protected 	$curSql = "";						// nom de la requête en cours
		protected 	$curSub = "";						// nom de la sous requête en cours
		protected 	$sqls = array();					// liste des requêtes et sous requêtes générées

		// dump
		protected $dumpTables = array();				// liste des tables à dumper
		protected $dumpStructure = true;				// dump de la structure
		protected $dumpDatas = true;					// dump des donnees

		// initialisations selon la version de classe
		protected 	$version="3";						// version de la classe


		// PDO
		private $pdo;
		private $fetchMode = PDO::FETCH_ASSOC;			// mode par défaut de retour de la fonction fetchAll
		private $paramsSql = array();						// tableau de parametres pour la requete préparée


/*_______________________________________________________________________________________________________________
																				METHODES PRIVEES
*/


/**
		 *		Fonction pour ajouter des cotes aux requetes avec des points ( TABLE.CHAMP => `TABLE`.`CHAMP`  )
		 *		$a_champ : Champ a transformer
*/
		private function _addQuotes($a_champ,$a_set)
		{
			// Ici on defini dans un tableau tout les caracteres ou l'on ne souhaite pas de cotes
			$no_escape_tab = array("*");
			if (!in_array($a_champ,$no_escape_tab) && $a_set==true && $this->quotes)
			{
					// on ajoute des cotes autour du champ passé en arg
					$a_champ = "`".$a_champ."`";
					// Si il contient un point on le cotérise aussi :D
					$point = ".";
					$pointCote = "`.`";
					$a_champ=str_replace($point,$pointCote,$a_champ);
			}
			return($a_champ);
		}



/**
 		* action par défaut en cas d'insertion d'une clé unique déja existante ( ON DUPLICATE KEY )
 		* $a_action : action à réaliser en cas de doublon (voir onDuplicateKey)
 		* $a_strUpdate : chaine des updates
 */
		private function _onDuplicateKey($a_action,$a_strUpdate)
		{
			switch ($a_action)
			{
				case "ERROR":
					$str = "";
					break;

				case "NONE":
					if (strlen($this->tableInsert)>0)
					{
						$champ = array_shift($this->values);
						$str = " ON DUPLICATE KEY UPDATE ".$champ['champ']."=".$champ['champ'];
					}
					break;

				case "UPDATE":
					$str = " ON DUPLICATE KEY UPDATE ";
						$str .= $a_strUpdate;
					break;
			}
			return $str;
		}


        /**
		 * ajoute des quotes aux valeurs
         * @param string $a_texte
         * @return string
         */
		private function _escapeVal($a_texte)
		{
			$txt = $this->pdo->quote($a_texte);
			return $txt;
		}





/*_______________________________________________________________________________________________________________
																				METHODES PUBLIQUES
*/


/**
		* constructeur
*/
		public function __construct()
		{
		}

/**
		* destruction
*/
		public function __destruct()
		{
			if (isset($this->res) && is_resource($this->res))
				$this->res->closeCursor();
			if ($this->pconnexion && isset($this->linkId) && is_resource($this->linkId))
				$this->linkId=null;
		}



/**
		* affichage des erreurs
*/
		public function displayErrors($a_bool=true)
		{
			$this->afficheErreurs = $a_bool;
		}

		/**
		 * Fonction d'alias de displayErrors
		 *
		 * @param  bool $a_bool
		 * @return void
		 */
		public function afficheErreurs($a_bool)
		{
			$this->displayErrors($a_bool);
		}


/**
		* connexion persistante
*/
		public function pconnect()
		{
			$this->pconnexion = true;
			/*
			if(!$this->linkId)
				$this->connect();
			*/
		}


/**
		* chargement des paramètres de connexion à partir d'un fichier
*/
		public function loadParametres($a_fichier)
		{

			if (!isset($a_fichier))
				return(false);

			require $a_fichier;
			$base = (isset($db) ? $db : false);
			$driver = (isset($driver) ? $driver : false);

			if (isset($host,$user,$pwd,$base))
				$this->setParametres($host,$user,$pwd,$base,$driver);
		}


/**
		* initialisation des paramètres de connexion
*/
		public function setParametres($a_host,$a_user,$a_pwd,$a_db=false,$a_driver=false)
		{
			if (is_resource($this->linkId))
			{
				$this->pdo = null;
        		$this->linkId = false;
      		}
			$this->host = $a_host;
			$this->user = $a_user;
			$this->pwd = $a_pwd;
			// si le driver est indiqué, on réinitialise (mysql par défaut)
			if($a_driver){
				$this->driver = $a_driver;
			}
			// si la base est indiquée, on la sélectionne par défaut
			if($a_db){
				$this->db = $a_db;
				$this->connect();
			}

		}


/**
		* selection de la base
		* renvoie false si la base n'existe pas (plus possible)
*/
		public function useBase($a_db)
		{
			$this->db = $a_db;
			if($this->linkId)
				$this->close();

			// connexion à la base de données
			$this->connect();
		}




/**
		* connexion au serveur
*/
		public function connect()
		{
			// déjà  connecté
			if ($this->linkId){
//				echo "Connexion déjà active";
			} else {
				// connexion à mysql
				try {
					if(is_null($this->pdo ))
					{
						// Les informations de la base de données
						$pdo = new PDO($this->driver.":host=".$this->host.";dbname=".$this->db, $this->user, $this->pwd);

						// Attribut pour définir les type de messages d'erreurs
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true); // mysql 5.7

						$this->pdo = $pdo;
						$this->linkId = true;
					}
				}
				catch (Exception $e){
					print "<br>Connexion impossible: ".$e->getMessage()."<br>";
					exit();
				}
			}
			// connexion à la base si indiquée
			//if (strlen($this->db)>0)
			//	$this->useBase($this->db);
		}

/**
		* déconnexion de la base
		* si connexion persistante
*/
		public function close()
		{
			if (isset($this->res) && is_resource($this->res))
				$this->res->closeCursor();

			$this->linkId = false;
			$this->pdo = null;
		}


/*
		FONCTIONS D'EXPLOITATION DE LA CONF MYSQL
*/


/**
		* valeur d'une variable mysql
*/
		public function getMysqlVar($a_var)
		{
			$sql = "SHOW VARIABLES LIKE '".$a_var."'";
			$this->setSql($sql);
			$res = $this->execSQL();
			$row = $res->fetch();
			return($row['Value']);
		}



/*
		FONCTIONS D'EXPLOITATION DE LA STRUCTURE DE LA BASE
*/





/**
		* selection de la table
*/
		public function useTable($a_table)
		{
			$this->table = $a_table;
		}


/**
		* récupération de la liste des bases
		* $a_prefixe : ne recherche aue les base commencant par $a_prefixe
*/
		public function getBases($a_prefixe="\.*")
		{
			$liste = array();
			if ($this->linkId)
			{
				$db_list = $this->pdo->query("SHOW DATABASES");
				while ($row = $db_list->fetch()) {
					if (!in_array($row[0],$this->basesInterdites) && preg_match("/^".$a_prefixe."/",$row[0]))
						array_push($liste,$row[0]);
				}
			}
			return $liste;
		}


/**
		* récupération des parametres de connexion courants
*/
		public function getCurrentParams()
		{
			return (array("host"=>$this->host,"db"=>$this->db,"user"=>$this->user));
		}


/**
		* récupération de la liste des tables de la base en cours
		* a_format : nom de table recherchée avec LIKE (de la forme %nom ou nom%)
*/
		public function getTables($a_format=false)
		{
			$liste = array();
			if ($this->linkId)
			{
				$this->initSql();
				$sql = "SHOW TABLES FROM ".$this->db;
				if ($a_format)
					$sql .= " LIKE '".$a_format."'";
				$this->setSql($sql);
				$tables_list = $this->execSQL();
				while ($row = $tables_list->fetch()) {
					array_push($liste,$row[0]);
				}
			}
			return $liste;
		}



/**
		* structure d'un table
		* a_table : nom de la table
*/
		public function showCreateTable($a_table='')
		{
			if (strlen($a_table)>0)
				$this->table = $a_table;
			try
			{
				if (strlen($this->table)==0)
					throw new Exception("Erreur :<br>(".$this->pdo->errorInfo().")");
			}
			catch (Exception $e){
				print "<br>".$e->getMessage();
				exit();
			}
			$this->initSql();
			$sql = "SHOW CREATE TABLE ".$this->table;
			$this->setSql($sql);
			// execution de la requête
			if ($res = $this->execSQL())
				$result = $res->fetchColumn(1);

			if (!$this->pconnexion)
				$this->close();
			return ($result);
		}




/**
		* récupération de la liste des champs d'une table
		* a_format : nom du champ recherché avec LIKE (de la forme %nom ou nom%)
*/
		public function getChamps($a_format=null)
		{
			$liste = array();
			if ($this->linkId)
			{
				$this->initSql();
				$sql = "SHOW COLUMNS FROM ".$this->table;
				if (!is_null($a_format))
					$sql .= " REGEXP ".$a_format;
				$this->setSql($sql);
				$tables_list = $this->execSQL();
				while ($row = $tables_list->fetch()) {
					array_push($liste,$row[0]);
				}
			}
			return $liste;
		}


/**
		* vérification de l'existence d'une base
		* a_base : nom de la base
*/

		function baseExiste($a_base)
		{
			$liste = array();
			$req = $this->pdo->query("SHOW DATABASES");
			while($db_list = $req->fetch()){
				array_push($liste,$db_list[0]);
			}
			if (in_array($a_base,$liste))
				return true;
			else
				return false;
		}


/**
		* vérification de l'existence d'une table
		* a_table : nom de la table
*/

		function tableExiste($a_table)
		{
			return (count($this->getTables($a_table))>0);
		}









/*
		FONCTIONS D'EXPLOITATION DES DONNEES
*/



/**
 * vidage d'une table
 */
		public function truncateTable($a_table)
		{
			$this->initSql();
			$this->setSql("TRUNCATE ".$a_table);
			// execution de la requête
			$res = $this->execSQL();

			if (!$this->pconnexion)
				$this->close();
			return $res;
		}





/**
		* récupération de la requete initiale
*/
		public function setInit()
		{
			$this->setSub("default");
		}


/**
		* changement de la requete courante (sous requete)
		* on met la requete en cours dans le tableau des requetes
		* $a_name : nom de la nouvelle requete
*/
		public function setSub($a_name)
		{
			$curSub = $a_name;
			// on enregistre les parametres de la requete en cours dans la liste des requetes
			$this->sqls[$this->curSql]['sql'] = $this->sql;
			$this->sqls[$this->curSql]['tablesSelect'] = $this->tablesSelect;
			$this->sqls[$this->curSql]['champsSelect'] = $this->champsSelect;
			$this->sqls[$this->curSql]['tableInsert'] = $this->tableInsert;
			$this->sqls[$this->curSql]['tableUpdate'] = $this->tableUpdate;
			$this->sqls[$this->curSql]['tableDelete'] = $this->tableDelete;
			$this->sqls[$this->curSql]['tablesJoin'] = $this->tablesJoin;
			$this->sqls[$this->curSql]['values'] = $this->values;
			$this->sqls[$this->curSql]['joins'] = $this->joins;
			$this->sqls[$this->curSql]['where'] = $this->where;
			$this->sqls[$this->curSql]['whereOk'] = $this->whereOk;
			$this->sqls[$this->curSql]['group'] = $this->group;
			$this->sqls[$this->curSql]['order'] = $this->order;
			$this->sqls[$this->curSql]['limit'] = $this->limit;
			$this->sqls[$this->curSql]['having'] = $this->having;
			$this->sqls[$this->curSql]['withRollup'] = $this->withRollup;
			$this->sqls[$this->curSql]['colonnes'] = $this->colonnes;
			$this->sqls[$this->curSql]['exportToFile'] = $this->exportToFile;
			$this->sqls[$this->curSql]['withColNames'] = $this->withColNames;
			$this->sqls[$this->curSql]['exportFileName'] = $this->exportFileName;

			// on récupère les parametres de la requete demandée s'ils existent
			// sinon on les réinitialise
			if (isset($this->sqls[$curSub]))
			{
				$this->sql = $this->sqls[$curSub]['sql'];
				$this->tablesSelect = $this->sqls[$curSub]['tablesSelect'];
				$this->champsSelect = $this->sqls[$curSub]['champsSelect'];
				$this->tableInsert = $this->sqls[$curSub]['tableInsert'];
				$this->tableUpdate = $this->sqls[$curSub]['tableUpdate'];
				$this->tableDelete = $this->sqls[$curSub]['tableDelete'];
				$this->tablesJoin = $this->sqls[$curSub]['tablesJoin'];
				$this->values = $this->sqls[$curSub]['values'];
				$this->joins = $this->sqls[$curSub]['joins'];
				$this->where = $this->sqls[$curSub]['where'];
				$this->whereOk = $this->sqls[$curSub]['whereOk'];
				$this->group = $this->sqls[$curSub]['group'];
				$this->order = $this->sqls[$curSub]['order'];
				$this->limit = $this->sqls[$curSub]['limit'];
				$this->having = $this->sqls[$curSub]['having'];
				$this->withRollup = $this->sqls[$curSub]['withRollup'];
				$this->colonnes = $this->sqls[$curSub]['colonnes'];
				$this->exportToFile = $this->sqls[$curSub]['exportToFile'];
				$this->withColNames = $this->sqls[$curSub]['withColNames'];
				$this->exportFileName = $this->sqls[$curSub]['exportFileName'];
			}
			else
				$this->initSql();

			$this->curSql = $curSub;

		}


/**
 * met un tag sur la requete
 * @param string $a_tag
 * @return void
 */
		public function tagSql($a_tag)
		{
			$this->tagSql = $a_tag;
		}




/**
		* réinitialisation des variables de la requete
		* $a_nom : nom de la requete à initialiser
*/
		public function initSql($a_name='default')
		{
			$this->curSql = $a_name;
			// liberation de la mémoire
			if (isset($this->res) && is_resource($this->res))
				$this->res->closeCursor();
			// réinitialisation de la requête
			$this->newBase = array();
			$this->varOnDuplicateKey = "ERROR";
			$this->valPrimaryKey = null;
			$this->varSqlCalcFoundRows = true;
			$this->varSqlStraightJoin = false;
			$this->sql = "";
			$this->tablesSelect = array();
			$this->champsSelect = array();
			$this->newTable = array();
			$this->tableInsert = "";
			$this->tableUpdate = "";
			$this->tableDelete = "";
			$this->tablesJoin = array();
			$this->values = array();
			$this->joins = array();
			$this->where = "";
			$this->curWhere = "";
			$this->whereOk = "";
			$this->group = "";
			$this->order = array();
			$this->limit = "";
			$this->having = "";
			$this->result=array();
			$this->withRollup=false;
			$this->colonnes=array();
			$this->exportToFile = false;
			$this->withColNames = false;
			$this->exportFileName = "";
			$this->nbLignesAffectees = 0;
			$this->multiplesValues = [];
			$this->paramsSql = array();
		}

/**
		* écriture manuelle de la requête
*/
		public function setSql($a_sql="")
		{
			$this->initSql();
			$this->sql = $a_sql;
		}



/**
		* ajoute des champs au SELECT
		* $a_champ : liste de champs
		* $a_as : renommage du champ
*/
		public function addSelect($a_champs,$a_cot=true,$a_as="")
		{
			// on réinitialise toute requête existante
			$this->sql="";

			if (!is_array($a_champs))
				$a_champs = array($a_champs);

			foreach ($a_champs as $champ)
			{
				// quotes inversées si demandées (par défaut)
				$val = $this->_addQuotes($champ,$a_cot);
				// renommage du champ si demandé
				if(strlen($a_as)>0)
					$val .= " AS ".$this->_addQuotes($a_as,$a_cot);
				if (!in_array($val,$this->champsSelect))
					array_push($this->champsSelect,$val);
			}

		}



/**
		* ajoute des tables à parcourir (SELECT ... FROM)
		* $a_tables : liste des tables
*/
		public function addFrom($a_tables)
		{
			if (!is_array($a_tables))
			{
				$a_tables = array($a_tables);
			}

			foreach ($a_tables as $table)
			{
				array_push($this->tablesSelect,$table);
			}

		}



/**
		* ajoute une condition WHERE
		* $a_where : instruction where (ex : champ='valeur' )
		* $a_sep : separateur à mettre avant (AND par défaut, OR)
*/
		public function addWhere($a_where,$a_sep="AND")
		{
			if ( strlen($this->where)>0 )
			{
				if (!preg_match("/\($/",$this->where))
					$this->where .= " ".$a_sep." ";
			}
			else
				$this->where .= preg_match("/^ WHERE /",$a_where) ? "" : " WHERE ";
			$this->where .= $a_where;
			$this->curAction = "where";
		}


/**
		* ajout d'une jointure entre 2 tables (table1 LEFT JOIN table2 ON table1.champ1=table2.champ1)
		* $a_table1 = première table de jointure
		* $a_table2 = deuxième table de jointure
		* $a_champ1 = champ de jointure de table 1
		* $a_champ2 = champ de jointure de table 2 (par défaut, le même que table 1
		* $a_type = type de jointure 'LEFT JOIN, RIGHT JOIN, INNER JOIN...)
*/
		public function addJoin($a_table1,$a_table2,$a_champ1,$a_champ2="",$a_type="LEFT JOIN")
		{
			$table2 = preg_replace("/.* AS (.*)$/",'${1}',$a_table2);
			if (!in_array($a_table2,$this->tablesJoin))
			{
				array_push($this->tablesJoin,$a_table2);
				array_push($this->joins," ".$a_type." ".$a_table2." ON ".$a_table1.".".$a_champ1."=".$table2.".".(strlen($a_champ2)>0 ? $a_champ2 : $a_champ1));
				$this->curAction = "join";
			}
		}

/**
		* ajoute des conditions ON à une jointure
		* $a_on : instruction on (ex : champ='valeur' )
		* $a_sep : separateur à  mettre avant (AND par défaut, OR)
*/
		public function addOn($a_on,$a_sep="AND")
		{

			$this->joins[(count($this->joins)-1)] .= " ".$a_sep." ".$a_on;
		}


/**
		* ouvre une parenthèse pour regroupement de conditions
		* $a_sep : separateur
		* si AND ou OR : separation entre 2 parenthèses
		* si WHERE ou ON : début de bloc d'instruction WHERE ou ON
*/
		public function groupeOn($a_sep="AND")
		{
			// début de bloc d'instruction
			if (in_array(strtolower($a_sep),array("where","on","having")))
				$this->curAction = strtolower($a_sep);
			// parenthésage
			switch($this->curAction)
			{
				case "where":
					$this->where .= ( strlen($this->where)>0 ? " ".$a_sep." " : " WHERE ")."\n"."(";
					break;
				case "having":
					$this->having .= ( strlen($this->having)>0 ? " ".$a_sep." " : " HAVING ")."\n"."(";
					break;
				case "join":
					$this->joins[(count($this->joins)-1)] = preg_replace("/ (ON)(.+)$/"," ON ($2) ",$this->joins[(count($this->joins)-1)]);
					break;
				default:
					break;
			}
		}

/**
		* ferme la parenthèse du dernier regroupement de conditions
*/
		public function groupeOff()
		{
			switch ($this->curAction)
			{
				case "where":					$this->where .= " )"."\n";				break;
				case "having":					$this->having .= " )"."\n";				break;
				case "join":					$this->joins .= " )";					break;
			}
		}


/**
		* ajoute un GROUP BY
		* $a_champ = champ à regrouper
*/
		// ajoute du groupe
		public function addGroup($a_champ)
		{
			$this->group .= strlen($this->group)>0 ? ", " : " GROUP BY ";
			$this->group .= $a_champ;
		}

/**
		* ajoute un tri à la requete ORDER
		* $a_champ = champ à trier
		* $a_sens = sens ASC ou DESC
		* NB : le champ à trier doit être dans la liste des champs du SELECT
*/
		public function addOrder($a_champ,$a_sens="ASC")
		{
			array_push($this->order,array('champ'=>$a_champ,'sens'=>$a_sens));
		}

/**
		* ajoute une limite (LIMIT min, nb)
		* $a_nb = nb de résultats
		* $a_min = premier résultat
*/
		public function addLimit($a_nb,$a_min=0)
		{
			$this->limit = " LIMIT ". ($a_min>0 ? $a_min . ", " : "") . $a_nb;
		}



/**
		* ajoute une clause HAVING
		* $a_having : instruction HAVING (ex : champ='valeur' )
		* $a_sep : separateur à mettre avant (AND par défaut, OR)
*/
		public function addHaving($a_having,$a_sep="AND")
		{
			if ( strlen($this->having)>0 )
			{
				if (!preg_match("/\($/",$this->having))
					$this->having .= " ".$a_sep." ";
			}
			else
				$this->having .= preg_match("/^ HAVING /",$a_having) ? "" : " HAVING ";
			$this->having .= $a_having;
		}



/**
		* ajoute l'instruction withRollup à la requête
*/
		public function withRollup()
		{
			$this->withRollup = true;
		}





/**
		* ajout d'une colonne pour CREATE TABLE
		* $a_nom : nom de colonne
		* $a_type : type de données ex : DATE, VARCHAR(80), ...
		* $a_options : options (AUTOINCREMENT, NULL...)
		* $a_index : type de d'index (PRIMARY KEY, UNIQUE, KEY|INDEX, FULLTEXT ...)
*/
		public function addColonne($a_nom, $a_type, $a_options="",$a_index="")
		{
			$colonne = array("nom"=>$a_nom,"type"=>$a_type,"options"=>$a_options,"index"=>$a_index);
			array_push($this->colonnes,$colonne);
		}



/**
		* ajout d'un couple champs/valeurs à mettre à jour pour requêtes INSERT / UPDATE ... SET champ='val'
		* $a_champ : champs
		* $a_val : valeur
		* $a_quotes : quotes autour du champ, activé par défaut
		* $a_fonction : fonction mysql - ex : MD5
*/
		public function setValue($a_champ, $a_valeur, $a_quotes=true, $a_fonction="")
		{
			$set = array('champ'=>$a_champ, 'valeur'=>$a_valeur, 'quotes'=>$a_quotes, 'fonction'=>$a_fonction,	'escape'=>true);
			$this->values[$a_champ] = $set;
		}


/**
 		* définit la liste de valeurs
 */
		public function setValues($a_values)
		{
			foreach ($a_values as $value)
			{
				foreach ($value as $champ=>$valeur)
				{
					$this->values[$champ] = array('champ'=>$champ, 'valeur'=>$valeur, 'quotes'=>true, 'fonction'=>'',	'escape'=>true);
				}
				array_push($this->multiplesValues,$this->values);
			}
			// 			print "<pre>";print_r($this->multiplesValues);print "</pre>";exit();
		}







/**
		* activation de SQL_CALC_FOUND_ROWS
*/
		public function sqlCalcFoundRows($a_actif=true)
		{
			$this->varSqlCalcFoundRows = $a_actif;
		}


/**
		* activation de SQL_CALC_FOUND_ROWS
*/
		public function sqlStraightJoin($a_actif=true)
		{
			$this->varSqlStraightJoin = $a_actif;
		}

/**
		* activation de ON DUPLICATE KEY
		* @param $a_action string action à réaliser en cas de doublon
		* @param $a_primary string nom de la clé primaire
 		* 	- ERROR : pas de gestion de doublon une erreur est retournée par MySql en cas de doublon
 		* 	- NONE : on fait rien (ON DUPLICATE KEY UPDATE clé=clé)
 		* 	- UPDATE : on met a jour les champs passés
*/
		public function onDuplicateKey($a_action,$a_primary=null)
		{
			$this->varOnDuplicateKey = $a_action;
			$this->primaryKey = $a_primary;
		}



/**
		* liste de champs à ne pas dupliquer dans le ON DUPLICATE KEY
		* @param string $a_champ champs à ne pas dupliquer
 */
		public function addNoDuplicateKey($a_champ)
		{
			array_push($this->noDuplicateKey,$a_champ);
		}




/**
  * génére et ecrit la requête
  * @param boolean $a_executeSql execute la requête si à true (par défaut), sinon ne fait que l'afficher
 */

		public function _displaySql()
		{
			if ($this->displaySql)
			{
				if ($this->formatDisplaySql=="PRE")
				{
					$this->formateSql();
					print $this->getPreSql();
				}
				else if ($this->formatDisplaySql=="TXT")
					print (strlen($this->tagSql)> 0 ? $this->tagSql.":\n " : "").$this->sql."\n";
			}
		}


/**
  * active l'écriture des requêtes
  * @param boolean $a_executeSql execute la requête si à true (par défaut), sinon ne fait que l'afficher
 */
		public function displaySql($a_executeSql=true)
		{
			$this->displaySql = true;
			$this->executeSql($a_executeSql);
		}


/**
 * execute ou pas la requete
 * @param $bool
 */
		public function executeSql($bool,$message=true)
		{
			$this->executeSql = $bool;
			$this->messageNoExecuteSql = $message;
		}


/**
 * explique la requête (EXPLAIN ...)
 */

		public function explainSql()
		{
			$this->displaySql();
			$sql = "EXPLAIN ".$this->sql;
			$this->setSql($sql);
			$res = $this->execSql();
			$this->getResults();
			return($this->result);
		}


/**
		* construction de la requête
*/
		public function makeSql()
		{

			if (count($this->newBase)>0)
				$this->sql = "CREATE DATABASE ".$this->newBase['nom'].";";

			// CREATE TABLE
			else if (count($this->newTable)>0)
			{
				$this->sql = "CREATE ".($this->newTable['temp'] ? "TEMPORARY " : ""). " TABLE `".$this->newTable['nom']."`";
				// COLONNES POUR CREATE TABLE
				if (count($this->colonnes)>0)
				{
					$indexes = array();

					// Création des colonnes
					$sqlCol = " (";
					foreach ($this->colonnes as $colonne)
					{
						$sqlCol .= (strlen($sqlCol)>2 ? ", " : " ").sprintf("`%s` %s %s",$colonne['nom'],$colonne['type'],$colonne['options']);
						if ($colonne['index']!="")
							array_push($indexes,array("nom"=>$colonne['nom'],"index"=>$colonne['index']));
					}

					// index sur les colonnes
					$sqlIndex = "";
					foreach ($indexes as $index)
					{
						$sqlIndex .= sprintf(", %s (%s)",$index["index"],$index["nom"]);
					}

					$sqlCol .= $sqlIndex.")";
					$this->sql .= $sqlCol;

				}
				// sinon une colonne par défaut
				else
				{
					$this->sql .=  " (uniqid INT AUTO_INCREMENT PRIMARY KEY)";
				}
				$this->sql .= $this->newTable['options']." ENGINE=".$this->newTable['moteur'];
			}

			// CRUD
			else
			{
				// on vérifie qu'il n'y a pas déjà  une requête d'initialisée
				// à cause de l'ancienne version de la classe SQL (retrocompatibilité)
				if (strlen($this->sql)==0)
				{
					// INSERT
					if (strlen($this->tableInsert)>0)
						$this->sql .= "INSERT INTO ".$this->tableInsert;

					// SELECT
					else if (count($this->champsSelect)>0)
					{
						$l_select = "";
						$l_colnames = "SELECT ";
						foreach ($this->champsSelect as $select)
						{
							if (strlen($l_select)>0)
								$l_select .= ", ";
							else
							{
								$l_select = " SELECT ";
								$l_select .= ($this->varSqlCalcFoundRows ? "SQL_CALC_FOUND_ROWS " : "");
								$l_select .= ($this->varSqlStraightJoin ? "STRAIGHT_JOIN " : "");
							}
							$l_select .= "\n".$select;
							if ($this->withColNames)
							{
								$l_colnames .= (strlen($l_colnames)>7 ? ", " : "")."'".preg_replace("/^(\w+\.)(\w+)/","\\2",$select)."'";
							}
						}
						$this->sql .= $l_select;
					}

					// UPDATE
					else if (strlen($this->tableUpdate)>0)
						$this->sql .= "UPDATE ".$this->tableUpdate;

					// DELETE
					else if (strlen($this->tableDelete)>0)
						$this->sql .= "DELETE FROM ".$this->tableDelete;


					// FROM
					if (count($this->tablesSelect)>0)
					{
						$l_from = "";
						foreach ($this->tablesSelect as $from)
							$l_from .= (strlen($l_from)>0 ? ", " : " FROM ") . $from;
						$this->sql .= "\n".$l_from;
					}

				}
				// SET, VALUES
				if (count($this->values)>0 && $this->pdo)
				{
					// insertion simple
					// INSERT INTO TABLE SET champ1=val1, champ2=val2...
					$l_set = "";
					$l_setDuplicateKey = "";
					foreach ($this->values as $set)
					{
						if (is_null($set['valeur']))
							$strValeur = 'NULL';
						else
						{
							// on traite une liste de valeurs, pour gérer les fonctions mySQL avec plusieurs arguments (ex : CONCAT(chaine1,chaine2))
							// si c'est un tableau, on passe les parametres quotes et escape pour chaque valeur
							if (is_array($set['valeur']))
								$valeurs =  $set['valeur'];
							else
								$valeurs[0] = array($set['valeur'],$set['quotes'],$set['escape']);
							$strValeur = "";
							$listeVals = "";

							foreach($valeurs as $valeur)
							{
								$val = (isset($valeur[2]) && $valeur[2]) ? $this->_escapeVal(trim($valeur[0])) : trim($valeur[0]);
								$val = trim ($val,"'");
								$listeVals .= (strlen($listeVals)>0 ? "," : "") . ($valeur[1] ? "\"".$val."\"" : $val);
							}
							if ($set['fonction'])
								$strValeur .= $set['fonction']."(".$listeVals.")";
							else
								$strValeur .= $listeVals;
						}
						$l_set .= ($l_set ? ", " : "") . $set['champ']."=".$strValeur;
						if (!in_array($set['champ'],$this->noDuplicateKey))
							$l_setDuplicateKey .= ($l_setDuplicateKey ? ", " : "") . $set['champ']."=".$strValeur;
						if (!is_null($this->primaryKey) && $set['champ']==$this->primaryKey)
							$this->valPrimaryKey = $set['valeur'];

					}
					$this->sql .= " SET ".$l_set;
					// gestion des clé uniques dupliquées
					if ($this->varOnDuplicateKey!="ERROR")
					{
						$this->sql .= $this->_onDuplicateKey($this->varOnDuplicateKey,$l_set);
					}
				}

				// JOIN
				if (count($this->joins)>0)
				{
					$this->curJoin = "";
					foreach ($this->joins as $join)
					{
						$this->curJoin .= " " . "\n".$join;
					}
					$this->sql .= $this->curJoin;
				}

				// WHERE
				$this->curWhere = strlen($this->whereOk)>0 ? $this->whereOk : ( strlen($this->where)>0 ? $this->where : "");
				$this->sql .= "\n".$this->curWhere;

				// GROUP
				$this->curGroup = $this->group;
				if (strlen($this->curGroup)>0)
					$this->sql .= "\n".$this->curGroup;

				// HAVING
				if (strlen($this->having)>0)
					$this->sql .= "\n".$this->having;

				// WITH ROLLUP
				if ($this->withRollup)
					$this->sql .= " WITH ROLLUP";

				// ORDER
				if (count($this->order)>0)
				{
					$this->curOrder="";
					foreach ($this->order as $order)
					{
						$this->curOrder .= $this->curOrder ? ", " : " ORDER BY ";
						$this->curOrder .= $order['champ']." ".($order['champ']!="RAND()" ? $order['sens'] : "");
					}
					$this->sql .= $this->curOrder;
				}

				// LIMIT
				if (strlen($this->limit)>0)
					$this->sql .= $this->limit;

				// EXPORT AVEC NOM DE COLONNES
				if ($this->exportToFile)
				{
					if ($this->withColNames)
						$this->sql = $l_colnames." UNION (".$this->sql;
					if (strlen($this->exportFileName)>0)
						$this->sql .= " INTO OUTFILE '".$this->exportFileName."' FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '\"'";
					$this->sql .= ")";
				}
			}

			$this->tagSql = "";

			return ($this->sql);
		}


/**
 * Formate la requête pour affichage HTML
 */
		public function formateSql()
		{
			$this->preSql = "";
 			$this->preSql .=  "<div class=\"displaysql\"><pre><code class=\"sql\">";
 			$this->preSql .=  "\n---------------------------------------------------------------------------------------------\n";
 			if (strlen($this->tagSql)>0)
 				$this->preSql .= "# ".$this->tagSql." sur ".$this->host."\n";
 			$this->preSql .=  classeTexts::valideTexte($this->sql);
 			$this->preSql .=  "\n---------------------------------------------------------------------------------------------\n\n";
 			$this->preSql .=  "</code></pre></div>";
		}


/**
 * retourne la requête formatée HTML
 * @return string
 */
		public function getPreSql()
		{
			return ($this->preSql);
		}

/**
 * format d'écriture des requêtes
 * @param string $a_format PRE (par défaut), TXT
 */
		public function setFormatDisplaySql($a_format)
		{
			$this->formatDisplaySql = $a_format;
		}


/**
		* retourne la requète
*/
		public function getSql()
		{
			return ($this->sql);
		}

/**
		* génère et retourne une sous requete
*/
		public function getSub($a_name="default")
		{
			$this->setSub($a_name);
			$this->makeSql();
			return($this->sql);
		}




/**
		* retourne WHERE courant
*/
		public function getWhere()
		{
			return $this->curWhere;
		}
/**
		* retourne ORDER courant
*/
		public function getOrder($a_liste=false)
		{
			if ($a_liste)
				return $this->order;
			else
				return $this->curOrder;
		}

/**
		* retourne GROUP BY courant
*/
		public function getGroup()
		{
			return $this->curGroup;
		}

/**
		* retourne JOIN courant
*/
		public function getJoin()
		{
			return $this->curJoin;
		}


        /**
		 * initialisation des parametres de la requête préparée
         * @param $a_params
         */
		public function setParamsSql($a_params)
		{
			if (is_array($a_params))
			{
				foreach($a_params as $param=>$value)
					$this->paramsSql[$param]=$value;
			}
		}


        /**
         * mise à jour de la requête avec les parametres
         */
		private function bindParamsSql()
		{
			if (is_array($this->paramsSql) && count($this->paramsSql)>0)
				foreach ($this->paramsSql as $param=>&$value)
					$this->res->bindParam(":".$param, $value);
		}






/**
		* execution de la requète
*/
		public function execSql()
		{

			$this->_displaySql();

			if (!$this->executeSql && $this->messageNoExecuteSql)
				print "Requête non executée";

			elseif ($this->executeSql && !empty($this->pdo))
			{
				$infos = "";
				$infos .= $this->sql;
				try {
					$this->res = $this->pdo->prepare($this->sql);
					$this->bindParamsSql();
					if ($this->res->execute()===false)
						throw new Exception("Erreur d'execution :<br>(".$this->pdo->errorInfo().")<br>".$infos);
				}
				catch (Exception $e){
					print "<p><b>ERREUR : Requ&ecirc;te invalide !</b></p>";
					if ($this->afficheErreurs)
					{
						print "<br>".$this->sql;
						print "<br>".$e->getMessage();
					}
					exit();
				}
				$this->nbLignesAffectees = $this->res->rowCount();
				return $this->res;
			}
		}





/**
		* execution de la REQUETE CREATE DATABASE
		* $a_base : nom de la base à créer
*/
		public function createDatabase($a_base)
		{
			$this->newBase = array("nom"=>$a_base);
			$this->makeSql();
			$ok = false;
			if ($res = $this->execSQL())
			{
				$ok = true;
				$this->useBase($a_base);
			}
			return($ok);
		}


/**
		* execution de la REQUETE CREATE TABLE
		* $a_nom : nom de la table à créer
		* $a_moteur : moteur
		* $a_options : options supplémentaires
		* $a_temp : TEMPORARY si true (false par défaut)
*/
		public function createTable($a_table,$a_moteur="MyISAM",$a_options="",$a_temp=false)
		{
			$this->newTable = array("nom"=>$a_table,"moteur"=>$a_moteur,"options"=>$a_options,"temp"=>$a_temp);
			$this->makeSql();
			$ok = false;
			if ($res = $this->execSQL())
			{
				$ok = true;
				$this->useTable($a_table);
			}
			return($ok);
		}



/**
		* execution de la REQUETE TRUNCATE TABLE
		* $a_nom : nom de la table à supprimer
*/
		public function deleteTable($a_table)
		{
			$this->sql = "DROP TABLE IF EXISTS ".$a_table;
			if ($res = $this->execSQL())
				$ok = true;
			return($ok);
		}




/**
		* execution de la REQUETE SELECT
*/
		public function select()
		{
			$this->result=array();
			$this->makeSql();
			$res = $this->execSQL();
			if ($res)
			{
				$nbRes = $this->res->rowCount();

				// nombre de résultats
				if ($this->varSqlCalcFoundRows)
				{
					$sql = "SELECT FOUND_ROWS()";
					$res2 = $this->pdo->query($sql);
					$this->nbResults = $res2->fetchColumn();
				}
				else
					$this->nbResults = $nbRes;

				// on récupère le résultat de la requête
				if ($this->nbResults>0)
					$this->getResults();

				// on pointe sur le premier resultat
				$this->numRes=0;

				// deconnexion
				if (!$this->pconnexion)
					$this->close();
			}
			return $this->result;
		}



/**
		* execution de la REQUETE SELECT et export du resultat vers un fichier CSV
		* $a_filename : nom du fichier CSV
		* $a_nomColonnes : exportation du nom des colonnes en première ligne (par défaut)
*/
		public function selectToCsv($a_filename,$a_nomColonnes=true)
		{
			$this->result=array();
			$this->exportToFile = true;
			$this->withColNames = $a_nomColonnes;
			$this->exportFileName = $a_filename;
			$this->sqlCalcFoundRows(false);
			$this->makeSql();
			$res = $this->execSQL();
		}


        /**
         * @param $a_style : style de retour de la fonction fetchAll
		 * PDO::FETCH_ASSOC par défaut
         */
		public function setFetchMode($a_style)
		{
			$this->setFetchMode($a_style);
		}





/**
		* récupération du résultat de l'execution de la REQUETE
*/
		public function getResults()
		{
			$records = array();
			if(!empty($this->pdo))
				$this->result = $this->res->fetchAll($this->fetchMode);
			return($this->result);
		}



/**
 * execution de la REQUETE INSERT
 * retourne soit
 * - la valeur de l'id primaire s'il y en a un
 * - sinon le nombre de lignes insérées ou modifiées avec ON DUPLICATE KEY
*/
		public function insert($a_table="")
		{
			if($this->pdo){
				$this->tableInsert = strlen($a_table)>0 ? $a_table : $this->table;
				$this->makeSql();
				if ($this->execSQL())
				{
					if ($this->varOnDuplicateKey!="UPDATE")
					{
						$lastid = $this->pdo->lastInsertId();
						if ($lastid>0)
							$result = $lastid;
						else if ($this->nbLignesAffectees>0)
							$result = $this->nbLignesAffectees;
						else
							$result = false;
					}
					else
					{
						if ($this->valPrimaryKey>0)
							$result = $this->valPrimaryKey;
						else if ($this->nbLignesAffectees>0)
							$result = $this->nbLignesAffectees;
						else
							$result = false;
					}
				}
				if (!$this->pconnexion)
					$this->close();
				return($result);
			}

		}




/**
		* nombre d'insertions prévues pour l'insertion multiple
*/
		public function setNbInserts($a_nombre)
		{
			$this->nbInsertsMax = $a_nombre;
		}



/**
		* execution de la REQUETE INSERT MULTIPLE
*/
		public function multipleInsert($a_table="",$a_exec=true)
		{

// 			print "<pre>";print_r($this->values);print "</pre>";

			// on récupère les valeurs avant la réinitialisation de la requête
			$values = $this->multiplesValues;
			$this->values=array();

			// taille max d'une requete
			if ($this->listeChamps=="")
			{
				if ($this->max_allowed_packet==0)
					$this->max_allowed_packet = 0.5 * $this->getMysqlVar('max_allowed_packet');
				$this->sql = "INSERT INTO ".$a_table." (";
				foreach ($values[0] as $champ)
					$this->listeChamps .= (strlen($this->listeChamps)>0 ? ", " : "") . $champ['champ'];
				$this->sql .= $this->listeChamps.") VALUES ";
			}

			// insertion multiple, de la forme
			// INSERT INTO TABLE (champ1, champ2....) VALUES (val1, val2...),(val1, val2...)
			// on crée un tableau de listes de valeurs (val1, val2...)
			// la requete est écrite avant l'execution
			foreach ($values as $sets)
			{
				$l_set = "(";
				foreach ($sets as $set)
				{
					if($this->pdo){
						// ecriture de la liste des valeurs
						$valeur = trim($set['valeur']);
						$valeur = $set['quotes'] ? "'".$valeur."'" : $valeur;
						$valeur = $set['fonction'] ? $set['fonction']."(".$valeur.")" : $valeur;
						$l_set .= (strlen($l_set)>1 ? ", " : "") . $valeur;
					}
				}
				$l_set .= ")";
				$this->nbInserts++;
				$this->listeValues .= (strlen($this->listeValues)>0 ? ", " : "") . $l_set;
			}
			print $this->listeValues."<br />";

			// construction de la requete définitive et execution
			$reqFinale = $this->sql.$this->listeValues;

			// si la taille de la requete est proche de la taille max (depasse les 90%)
			if ( strlen($reqFinale) <= $this->max_allowed_packet || $this->nbInsertsMax==$this->nbInserts)
			{
				$this->listeValues = "";
				$this->listeChamps = "";
				$this->setSql($reqFinale);
				print $this->sql."<br />";
				// execution de la requete si demandé
				if ($a_exec)
				{
					$this->execSQL();
					if ($this->pdo->lastInsertId())
						$result = $this->pdo->lastInsertId();
					else
						$result = $this->nbLignesAffectees;
					return ($this->nbLignesAffectees);
				}
				else
					return($this->sql);
			}
		}



/**
 		* import de données depuis un fichier CSV
 		* $a_fic : chemin du fichier csv
 		* $a_table : table à remplir
 		* $a_debut : numéro de la première ligne à importer, commence à 0 (1 par défaut si le fichier csv a une ligne d'entête à ne pas importer)
 		* $a_fieldTerm : caractère délimitant le champ (; par défaut)
 		* $a_lineTerm : caractère de fin de ligne (\n par défaut)
 		* retourne le nb de lignes insérées
 */
		public function importFromCSV($a_fichier,$a_table,$a_debut=1,$a_fieldTerm=";",$a_lineTerm="\n",$a_fieldEnclose="")
		{

			$this->initSql();
			$this->sql = "LOAD DATA INFILE '".$a_fichier."'";
			$this->sql .= " INTO TABLE `".$a_table."`";
			$this->sql .= " FIELDS TERMINATED BY '".$a_fieldTerm."'";
			if ($a_fieldEnclose!="")
				$this->sql .= " ENCLOSED BY '".$a_fieldEnclose."'";
			$this->sql .= " LINES TERMINATED BY '".$a_lineTerm."'";
			$this->sql .= " IGNORE ".$a_debut." LINES";

			if ($this->displaySql)
			{
				$this->formateSql();
				if ($this->formatDisplaySql=="PRE")
					print $this->getPreSql();
				else if ($this->formatDisplaySql=="TXT")
					print $this->sql;
			}

			$this->execSQL();
			if (!$this->pconnexion)
				$this->close();
			return ($this->nbLignesAffectees);
		}







/**
		* execution de la REQUETE UPDATE
*/
		public function update($a_table="")
		{
			$this->tableUpdate = strlen($a_table)>0 ? $a_table : $this->table;
			$this->makeSql();
			$this->execSQL();
			if (!$this->pconnexion)
				$this->close();
			return ($this->nbLignesAffectees);
		}


/**
		* execution de la REQUETE DELETE
*/
		public function delete($a_table="")
		{
			$this->tableDelete = strlen($a_table)>0 ? $a_table : $this->table;
			$this->makeSql();
			$this->execSQL();
			if (!$this->pconnexion)
				$this->close();
			return ($this->nbLignesAffectees);
		}





/**
		* execution d'une REQUETE COUNT
		* $a_table = table à parcourir
		* $a_champs = champs à compter
*/
		public function count($a_table,$a_champs="")
		{
				// réinitialisation du résultat
				unset($this->result);
				$this->sqlCalcFoundRows(false);
				$result=null;

				// construction de la requête
				$select = "COUNT(" . ($a_champs ? $a_champs : "*") . ")";
				if (!$a_champs) $select.=" AS COMPTE";
				$this->addSelect($select);
				$this->addFrom($a_table);
				$this->makeSql();

				// execution de la requête
				if ($res = $this->execSQL())
				{
					$result = intval($res->fetchColumn(0));
				}

				if (!$this->pconnexion)
					$this->close();
				return $result;
		}



/**
		* nb de résultats d'une requete
*/
		// retourne le nb de résultats d'une requete
		public function getNbResultats()
		{
			return($this->nbResults);
		}

		// retourne le nb de lignes affectées par la dernière requete
		public function getNbLignesAffectees()
		{
			return($this->nbLignesAffectees);
		}

/**
		* passe au premier résultat
*/
		public function premier()
		{
			$this->numRes=0;
		}

/**
		* passe au résultat suivant
*/
		public function suivant()
		{
			$this->numRes++;
			if ( $this->numRes <= $this->getNbResultats())
				return (true);
		}

/**
		* valeur d'un tableau de résultat
*/
		public function get($a_champs)
		{
			if (isset($this->numRes) && count($this->result)>0)
				return ($this->result[$this->numRes][$a_champs]);
		}




/*
		FONCTIONS D'ADMINISTRATION
*/


/**
 * liste des tables à dumper
 * @param array $a_table
 */
		public function setDumpTable($a_table)
		{
			$this->dumpTables = $a_table;
		}

/**
 * parametres du dump
 * @param bool $a_datas
 * @param bool $a_structure
 */
		public function setDumpParam($a_datas=true,$a_structure=true)
		{
			$this->dumpDatas = $a_datas;
			$this->dumpStructure = $a_structure;
		}

/**
		* dump de la base en cours
*/
		public function dump()
		{
			// tables non précisées : on dump toutes les tables
			$tables = count($this->dumpTables)==0 ? $this->getTables() : $this->dumpTables;
			$bufferStruct = "";
			$bufferDatas = "";

			foreach ($tables as $table)
			{
				//STRUCTURE DE LA TABLE
				if ($this->dumpStructure)
				{
					$struct = $this->showCreateTable($table);
					$struct = preg_replace("/,?[\n\r\s]*CONSTRAINT [^\n\r]*/","",$struct);
					$bufferStruct .= $struct.";\r\n\r\n";

					// CONTRAINTES
					preg_match_all("/CONSTRAINT [^\n\r]*/",$struct,$matches);
					if (count($matches[0])>0)
					{
						$contraintes[$table]=$matches[0];
						foreach ($contraintes as $table=>$contrainte)
						{
							foreach ($contrainte as $cont)
								$bufferStruct .= "ALTER TABLE `".$table."` ADD ".trim($cont,",").";\r\n\r\n";
						}
					}
				}


				// DONNEES
				if ($this->dumpDatas)
				{
					$this->initSql();
					$this->addSelect('*');
					$this->addFrom($table);
					$datas = $this->select();
					if (count($datas)>0)
					{
						$this->setNbInserts(count($datas));
						foreach ($datas as $data)
						{
							$this->setValues($data);
							$req = $this->multipleInsert($table);
							if (strlen($req)>0)
								$bufferDatas .= $req.";\r\n\r\n";
						}
					}
				}
			}
			$buffer = $bufferStruct.$bufferDatas;
			return($buffer);
		}

	}
