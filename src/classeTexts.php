<?php
/**
 * Fonctions d'exploitation des textes et chaines de caracteres
 *
 * Validation de formats
 * Conversions
 * Fonctions de cryptage
 * ...
 *
 */


	
    namespace Romualb\RbtClasses;

	
	class classeTexts
	{
	
		static $charset = "UTF-8";
		public $version = "2";
		
		
/*_______________________________________________________________________________________________________________	
																				METHODES PRIVEES				
*/
	
	

/*_______________________________________________________________________________________________________________	
																				METHODES PUBLIQUES				
*/
	
	
/**
		* constructeur
*/
		
//		public static function __construct()
//		{
//		}
	
/**
		* initialisation de la table d'encodage de caractère utilisée
		* @param $a_charset string UTF-8 (par défaut) iso-8859-1...
 */
		//  
		public static function setCharset($a_charset)
		{
			self::$charset=$a_charset;
		}
		

		
/*_______________________________________________________________________________________________________________	
																				TRANSFORMATION DE TEXTE
*/
		
		
/**
		* convertit le texte en html
		* @param $a_texte string Texte à convertir
		* @return string texte converti
 */
		public static function txt2html($a_texte)
		{
			// suppression des espaces avant et aprés
			$tmpString = trim($a_texte);
			// remplace les caractères accentués en html
			$tmpString = htmlentities($tmpString,ENT_QUOTES,self::$charset);
			// remplacement des caractères spéciaux courants non transformés
			$tmpString = str_replace("%", "&#37;", $tmpString);
			$tmpString = str_replace("€", "&euro;", $tmpString);
			// transformation des sauts de ligne en balise"<br>"
			$tmpString = str_replace(array("\r\n", "\n\r", "\n","\r"),"<br />",$tmpString);
			return $tmpString;
		}					

		
/**
		* convertit le html en texte pour csv
		* @param $a_texte string Texte à convertir
		* @param $a_escQuotes boolean quotes echappés par antislash par défaut
		* @return string texte converti
 */
		public static function html2csv($a_texte,$a_escQuotes=true)
		{
			// suppression des espaces avant et après
			$tmpString = trim($a_texte);
			// suppression des quotes
			$tmpString = self::remplaceQuotes($tmpString,"'");
			// mise au format texte
			$tmpString = html_entity_decode($tmpString,ENT_QUOTES,self::$charset);
			// correction des sauts de ligne
			$tmpString = str_replace(array("<br>","<br />","\n","\r"), " ", $tmpString);
			// correction des ;
			$tmpString = str_replace(array(";"), ", ", $tmpString);
			// ajout de slashes
			if ($a_escQuotes)
				$tmpString = addslashes($tmpString);
			return $tmpString;
		}					

		
/**
		* convertit le html en texte
		* @param string $a_texte Texte à convertir
 		* @param boolean $a_rmAccents on vire les accents si à true
        * @param boolean $a_sautligne on garde les sauts de ligne si true
		* @return string texte converti
 */
		public static function html2txt($a_texte,$a_rmAccents=false,$a_sautLigne=true)
		{
			// suppression des espaces avant et après
			$tmpString = trim($a_texte);
			// ajout d'espace avant le html
			$tmpString = str_replace(array("<"), " <", $tmpString);
			// suppression des accents si demandé
			if ($a_rmAccents)
				$tmpString = self::retireHtmlAccents($a_texte);
			//convert all types of single quotes
			$tmpString = self::remplaceQuotes($tmpString,"'");
			// mise au format texte
			$tmpString = html_entity_decode($tmpString,ENT_QUOTES,self::$charset);
			// correction des sauts de ligne
			$tmpString = str_replace(array("<br>","<br />"), ($a_sautLigne ? "\n" : ""), $tmpString);
			// ajouts de saut de ligne avant suppression des balises html
            if ($a_sautLigne)
            {
                $tmpString = preg_replace("/(<\/p>|<\/div>|<\/table>)/", "\\1\n",$tmpString);
                $tmpString = preg_replace("/(<p[^>]*>|<div[^>]*>|<table[^>]*>)/", "\n\\1",$tmpString);
            }
			// suppression des tags html
			$tmpString = strip_tags($tmpString);
			return $tmpString;
		}					
		
		
		
/**
		* convertit le html en texte compatible avec javascript
		* @param $a_texte string Texte à convertir
		* @return string texte converti
 */
		public static function html2js($a_texte)
		{
			$a_texte = str_replace(array("<br>","<br />"), " ", $a_texte);
			$a_texte = self::html2txt($a_texte);
			$a_texte = self::stripslashes($a_texte);
			return ($a_texte);
			
		}					
		

        /**
         * convertit une chaine XML en tableau
         * @param $a_xml
         * @return mixed
         */
        function xml2Array($a_xml)
        {
            return(json_decode(json_encode($a_xml),TRUE));
        }

		
		

/**
		* suppression des caractères accentués HTML
		* les remplace par le caractère non accentué (ex : $eacute; -> e)
		* @param $a_texte string Texte à convertir
		* @return string texte converti
 */
        public static function retireHtmlAccents($a_texte)
        {
        	$a_texte = preg_replace('/&(\w)(grave;|acute;|uml;|circ;|tilde;|cedil;)/','\\1',$a_texte);
			return ($a_texte);
        }

        
/**
		* remplace les quotes par un caractère/texte
		* @param $a_texte string Texte à convertir
		* @param $a_char string Texte de remplacement (vide par défaut)
		* @return string texte converti
 */
        public static function remplaceQuotes($a_texte,$a_char="")
        {
        	$a_texte = str_replace(array('&#039;','&#39;','&quot;',"'"),$a_char,$a_texte);
        	return ($a_texte);
        }
        
        
/**
		* ajoute un slash avant les apostrophes
		* @param $a_texte string Texte à convertir
		* @return string texte converti
 */
        public static function stripslashes($a_texte)
        {
        	$a_texte = self::remplaceQuotes($a_texte,"\'");
        	return ($a_texte);
        }
        
        
        
/**
		* mise en majuscule avec prise en compte des caractères HTML
		* @param $a_texte string Texte à convertir
		* @param $a_ucfirst boolean Seule la première lettre est mise en majuscule
		* @return string texte converti
 */
		public static function uppercase($a_texte,$a_ucfirst=false)
		{
			// on extrait les parties séparées par un -
			$parts = explode("-",$a_texte);
			$resTxt = "";
			foreach ($parts as $part)
				$resTxt .= (strlen($resTxt)> 0 ? "-" : "") . 
					($a_ucfirst ? mb_strtoupper(substr($part,0,1),self::$charset).mb_strtolower(substr($part,1)) : mb_strtoupper($part,self::$charset));
			return ($resTxt);
		}

		
		
/**
		* convertit les caractères accentués en octal
		* @param $a_texte string Texte à convertir
		* @return string texte converti
 */
		public static function txt2oct($a_texte)
		{
			$pattern = "/(\w+)/";
			$a_texte = self::txt2html($a_texte);
			$a_texte = self::stripslashes($a_texte);
			$a_texte = preg_replace_callback(
				$pattern, 
				function($res){
					return "\\".decoct(ord(html_entity_decode($res[1],ENT_QUOTES,(self::$charset!="UTF-8" ? self::$charset : ''))));
				},
				$a_texte);
			return ($a_texte);
		}

		
		
		
		
		
/**
 * convertit un nombre d'octets en valeur lisible
 * @param $a_size integer
 * @return string
 */		
	public static function octetsToHuman($a_size)
	{
	    $unit=array('oct','Ko','Mo','Go','To','Po');
	    return round($a_size/pow(1024,($i=floor(log($a_size,1024)))),2).' '.$unit[$i];
	}		
		
		
		
		
		
		
		
		
		
	
		// Vérifier la validité d'un adresse URL.
		// $a_url : l'url à  tester
		public static function isValideUrl($a_url)
		{
			return(@fopen($a_url, 'r'));
		}

		// récupère la source d'une url
		// $a_url : l'url Ã  copier
		// $a_fic : nom du fichier destination. si pas indiqué, le fichier dest ne sera pas créé
		public static function getUrl($a_url,$a_fic=false)
		{
			$url = fopen($a_url, 'r');
			$ret = "";
			if ($a_fic)
				$fic = fopen ($a_fic,'a');
			while ($buffer = fread($url,1024))
			{
				if ($a_fic)
					fwrite($fic,$buffer);
				else $ret .= $buffer;
			}
			if ($a_fic)
				fclose($fic);
			fclose($url);
			return($ret);
		}
		

		
		
	/**
 * ajoute des arguments à une url
 * @param string $a_url
 * @param string/array $a_args (chaine arg1=val1&arg2=$val2... ou liste {arg1=val1,arg2=val2...}
 * @return string
 */        
        public static function addUrlArgs($a_url,$a_args)
        {

			$url = parse_url($a_url);
			$path = $url['path'];
			// arguments à ajouter
			if (is_string($a_args))
				parse_str($a_args,$newArgs);
			else foreach ($a_args as $arg)
				$newArgs[$arg[0]] = $arg[1];
			$resArgs = array();
			
			if (isset($url['query']))
			{
				// arguments courants
				parse_str($url['query'],$curArgs);
				// on ne garde que les arguments précédents qui ne changent pas
				foreach ($curArgs as $arg=>$val)
				{
					if (!isset($newArgs[$arg]))
						array_push($resArgs,$arg."=".$val);
				}
			}
			// on ajoute les nouveaux
			foreach ($newArgs as $arg=>$val)
				array_push($resArgs,$arg."=".$val);

			$res = $path."?".implode("&",$resArgs);
			return ($res);        
        } 		
		
		


	// fonction qui transforme les url d'une chaine en lien actif
	// et les emails en mailto
	// gère l'impression ou pas : si impression, on ne fait pas de lien, on souligne juste
	public static function isoleUrls($a_str, $a_target="_blank", $a_imp=false )
	{
		$in = array(
		'`<a[^>]+href="([^"]+)"[^>]*>([^<]+?)</a>`',
		'`((www\.))(([[:alnum:]]-_.)+\.)*([[:alnum:]/\._-])*\?*([^ ]*=[^ ]*)*`',
		'`(http://)+((([[:alnum:]]-_.)+\.)*([[:alnum:]/\._-])*\?*([^ ]*=[^ <]*)*)`',
		'`([[:alnum:]]([-_.]?[[:alnum:]])*@[[:alnum:]]([-_.]?[[:alnum:]])*\.([a-z]{2,4}))`'
		);
		if (!$a_imp)
		{
			$out = array(
			' $2 ',
			'http://$0',
			' <a href="http://$2" target="'.$a_target.'">http://$2</a> ',
			' <a href="mailto:$1">$1</a> '
			);
		}
		else
			$out = array(
			'<u>$2</u>',
			'<u>$0</u>',
			'<u>$2</u>',
			'<u>$1<u>');

		$result = preg_replace($in,$out,$a_str);

		return($result);

	}



	// Fonction de resumé d'une chaine de caractères: limitation Ã  un nb de caractères max
	// coupe au prochain espace si coupure france pas forcée
	//
	//        $a_str        chaîne à  traiter
	//        $a_ncar		nb de caractères max
	//        $a_stop		forcer coupure franche
	// si on coupe en plein milieu d'un tag image, on referme le tag

	public static function strResume ($a_str, $a_ncar, $a_stop=false)
	{
			if (strlen($a_str)>=$a_ncar)
			{
				$a_str=substr($a_str,0,$a_ncar);
				$espace=strrpos($a_str," ");
				$strResume=substr($a_str,0,$espace)."...";
			}
			else
				$strResume=$a_str;
			
				return($strResume);
	}


	// récupère les éléments d'une url
	// $a_url : url à traiter
	// $a_arg : argument à  récupérer
	public static function getFromUrl($a_url,$a_arg)
	{
		preg_match("/[\/?&]".$a_arg."=([^&.]*)/",$a_url,$valeur);
		if (isset($valeur[1]))
		{
			$val = str_replace("+"," ",$valeur[1]);
			$val = urldecode($val);
		}
		$ret = isset($val) ? $val : $valeur;
		return($ret);
	}


	
	// surligne les mots clé dans un texte
	// ajoute un span class='surligne'
	// $a_texte = texte Ã  surligner
	// $a_motsCle = liste de mots clés Ã  mettre en évidence
	public static function surligne($a_texte,$a_motsCle)
	{
		
		if (!is_array($a_motsCle)) $a_motsCle = array($a_motsCle);
		foreach($a_motsCle as $motcle)
		{
			$patterns = array("/[^A-z]".$motcle."[^A-z]/i", "/^".$motcle."[^A-z]/i", "/[^A-z]".$motcle."$/i");
			$res = preg_replace($patterns," <span class='surligne'>".$motcle."</span> ",$a_texte);
			$a_texte=$res;
		}
		return($res);
	}
	
	
		// retire les caractères spéciaux d'une chaine et les remplace par un autre caractère (vide par défaut)
		public static function nettoieChaine($a_str,$a_char="")
		{
			$str = str_replace("&#039;",$a_char,$a_str);				// on transforme les guillements			
			$pattern = "/[^[:alnum:]]/";
			$str = preg_replace($pattern,$a_char,$str);
			//print $result;
			return($str);
		}	
	
	
		// convertit un titre en texte valide pour insérer dans une URL
		public static function titleToUrl ($a_texte="")
		{
//			$str = strlen($a_texte)>0 ? $a_texte : $this->m_texte;
			$str = $a_texte;
			$str = self::Db2Txt(trim($str));					// on remet le format texte, avec accents
			$str = self::txt2html($str);						// on enlève les accents
			$str = self::retireHtmlAccents($str);
			$str = self::remplaceQuotes($str,"_");				// on remplace les apostrophes par _
			$str = self::nettoieChaine($str,"_");				// on nettoie les caractères incompatibles
			$str = str_replace(' ','_',$str);					// on enlève les espaces
			$str = preg_replace("/[_]+/","_",$str);				// on regroupe les doublons _
			$str = strtolower($str);							// on met en minuscule
			return($str);
		}
	
	
	
	
	
		// fonction qui génère un mot de passe aléatoire
		// $a_longueur : longueur du mot de passe
		// $a_niveau : bas = chiffres + lettres minuscules + lettres majuscules
		// $a_niveau : haut = chiffres + lettres minuscules + lettres majuscules + autres caractères
		// $a_niveau : num = chiffres
		// $a_caracteresExclus = liste de codes ASCII des caractères à exclure (par défaut : " ' , ; `)
		public static function genereMotDePasse($a_longueur,$a_niveau="bas",$a_caracteresExclus=array(34,39,44,59,96)) 
		{
			$str="";
			// au cas ou $a_caracteresExclus n'est pas un tableau
			if (!is_array($a_caracteresExclus)) $a_caracteresExclus=array();
			//liste des caractères
			mt_srand(time());
			//print_r($a_caracteresExclus);
			while (strlen($str) < $a_longueur) 
			{
				$tmp = mt_rand(33,122); 
				// chiffres
				if ($a_niveau == "num" && ($tmp<48 || $tmp>57))
					continue;
				// chiffres + lettres
				else if ($a_niveau == "bas" && ($tmp<48 || ($tmp>57 && $tmp<65) || ($tmp>90 && $tmp<97)) )
					continue;
				// chiffres + lettres + caractères
				else if ($a_niveau == "haut" && in_array($tmp,$a_caracteresExclus) )
					continue;
		
				$str .= chr($tmp);
			}
			return $str;
		}
	



	    // json valide
        public static function isJson($string) {
            json_decode($string);
            return (json_last_error() == JSON_ERROR_NONE);
        }


	
		// reconnait ddddW
		public static function isApe($a_code)
		{
			return (preg_match("/^\d{4}[A-Z]$/",$a_code)==1);
		}
		// reconnait ddddddddd ou ddd ddd ddd
		public static function isSiren($a_code)
		{
			return (preg_match("/^(\d{3} ?){3}$/",$a_code)==1);
		}
		// reconnait dddddddddddddd avec ou sans espaces à l'interieur
		public static function isSiret($a_code)
		{
			return (preg_match("/^(\d ?){14}$/",$a_code)==1);
		}
		// vérifie si un email est valide
		public static function isValideEmail($a_mail)
		{
			//return (preg_match("/^[\w-]+[\.\w-]*[\w-]+@[\w-]+\.\w{2,7}$/",$a_mail)==1);
            return (filter_var($a_mail, FILTER_VALIDATE_EMAIL));
		}
		// vérifie si une chaine est un téléphone
		public static function isTelephone($a_tel,$a_pays="FR")
		{
		    if ($a_tel=="" || strpos($a_tel,"00000000")!==false)
		        return (false);
			//return (preg_match("/(^([\+]?\([\d]{2}\)[\d])([\.\s]*[\d]{2}[\.\s]*){4})$|^(([\d]{2}[\.\s]*){5})$/",$a_tel)==1);
			// format Belge
			// fixes : 			012345678			01 234 56 78		01.234.56.78			+32 1 234 56 78			00321 234 56 78
			// portables : 		0123 45 67 89		0123 45 67 89		+32 123 45 67 89		0032 123 45 67 89
			if ($a_pays=="BE")
				return (preg_match("/^((\+\(?32\)?)|(0032)|0)(([\.\s]*\d[\.\s]*\d{3}([\.\s]*\d{2}){2})|([\.\s]*\d{3}([\.\s]*\d{2}){3}))$/",$a_tel)==1);
			// format Français
			// 0123456789		01 23 45 67 89		01.23.45.67.89		+33 1 23 45 67 89		00331 23 45 67 89
			else
				return (preg_match("/^((\+\(?33\)?)|(0033)|0)[\.\s]*\d([\.\s]*\d{2}){4}$/",$a_tel)==1);
		}
		// vérifie si un téléphone est un mobile à partir du préfixe
        public static function isMobilePhone($tel,$pays="FR")
        {
            $tel = self::formateTel($tel,"FR");
            switch ($pays)
            {
                case "FR":
                    $isMobile = in_array(substr($tel,0,2),array("06","07"));
                    break;
                default:
                    $isMobile = false;
                    break;
            }
            return ($isMobile);
        }

	
		// format nombre en siren : ddd ddd ddd
		public static function formateSiren($a_str)
		{
			if (self::isSiren($a_str))
			{
				// on supprime tout ce qui n'est pas nombre
				$siren = preg_replace('/\D/','',$a_str);
				return (trim(preg_replace("/(\d{3})(\d{3})(\d{3})/","\\1 \\2 \\3",$siren)));
			}
			else 
				return ('');
		}
		// format nombre en siret : ddd ddd ddd ddddd
		public static function formateSiret($a_str)
		{
			if (self::isSiret($a_str))
			{
				$siret = preg_replace("/\D/","",$a_str);
				return (preg_replace("/(\d{3})(\d{3})(\d{3})(\d{5})/","\\1 \\2 \\3 \\4",$siret));
			}
			else 
				return ('');
		}
			// format nombre en téléphone dddddddddd
		public static function formateTel($a_tel,$a_pays="FR")
		{
			$tel = $a_tel;
			switch ($a_pays)
			{
				case "BE":
					$tel = preg_replace('/^0032/','',$tel);
					$tel = preg_replace("/^\+\(?\d{2}\)?/","",$tel);
					$tel = preg_replace("/\D/","",$tel);
					$tel = ltrim($tel,'0');
					if (strlen($tel)==9 || strlen($tel)==8)
						$tel = sprintf("0032%s",$tel);
					else 
						$tel = "";
					break;
				default:
				    $tel = preg_replace('/^\+?0033/','0',$tel);
					$tel = preg_replace("/^\+\(?\d{2}\)?/","",$tel);
					$tel = preg_replace("/\D/","",$tel);
					$tel = ltrim($tel,'0');
					if (strlen($tel)==9)
						$tel = sprintf("0%s",$tel);
					else 
						$tel = "";
					break;
			}
			return($tel);
		}
		// ecrit le no de tel au format 00 00 00 00 00
		public static function ecritTel($a_tel,$a_pays="FR")
		{
			if ($a_pays=="BE")
			{
				$tel = preg_replace('/^0(032)?/','',$a_tel);
				// tel fixe
				if (strlen($tel)==8)
					$tel = preg_replace("/(\d{2})/","\\1 ",$tel);
				// tel mobile
				else if (strlen($tel)==9)
					$tel = substr($tel,0,3).preg_replace("/(\d{2})/"," \\1",substr($tel,3));
				// ajout du prefixe national
				if (strlen($tel)>0)
					$tel = "0032 ".$tel;
			}
			else
				$tel = preg_replace("/(\d{2})/","\\1 ",$a_tel);
			return($tel);
		}	
		
		// ecrit le no de tel au format 00 00 00 00 00 avec lien html
		public static function ecritLienTel($a_tel)
		{
			$noTel = self::ecritTel($a_tel);
			$tel = "";
			if (strlen(trim($noTel))>0)
				$tel = "<a href=\"tel:".$noTel."\">".$noTel."</a>";
			return($tel);
		}	
		
		// vérifie qu'une IP est dans un plage d'IPs
		// $a_ip = IP recherchee
		// $a_ipDebut = IP de début
		// $a_ipFin = IP de fin (égale à celle du début)
		public static function inPlageIp($a_ip,$a_ipDebut,$a_ipFin="")
		{
			$ipMin = ip2long($a_ipDebut);
			$ipMax = $a_ipFin=="" ? $ipMin : ip2long($a_ipFin);
			$ip = ip2long($a_ip);
  			$res = ($ip >= $ipMin && $ip <= $ipMax);
  			return ($res);
		}
		
		

		// convertit un no de colonne en index au format Excel (A, B, AA...) 
		public static function intToxlscol($a_strColumnIndex)
		{
			if ($a_strColumnIndex<27)
				$xlColumnValue = Chr($a_strColumnIndex + 65 - 1);
			else if ($a_strColumnIndex % 26 != 0)
				$xlColumnValue = Chr($a_strColumnIndex / 26 + 65 - 1) . Chr($a_strColumnIndex % 26 + 65 - 1);
			else
				$xlColumnValue = Chr($a_strColumnIndex / 26 + 65 - 2) . Chr(90);
			return($xlColumnValue);		
		}
		// convertit un index de colonne au format Excel (A, B, AA...) en no
		public static function xlsToIntcol($a_strColumnIndex)
		{
			$xlColumnValue = ord($a_strColumnIndex) - 65 + 1;
			if (strlen($a_strColumnIndex) > 1)
				$xlColumnValue = ($xlColumnValue * 26) + (ord(substr($a_strColumnIndex, -1)) - 65 + 1);
			return($xlColumnValue);		
		}
		

		
		
		// génère une clé publique à partir d'une clé privée
		// $a_cleDEncryptage = clé privée
		public static function getPublicCle($a_texte,$a_cleDEncryptage)
		{
			$a_cleDEncryptage = md5($a_cleDEncryptage);
			$Compteur=0;
			$VariableTemp = "";
			for ($Ctr=0;$Ctr<strlen($a_texte);$Ctr++)
			{
				if ($Compteur==strlen($a_cleDEncryptage))
				$Compteur=0;
				$VariableTemp.= substr($a_texte,$Ctr,1) ^ substr($a_cleDEncryptage,$Compteur,1);
				$Compteur++;
			}
			return $VariableTemp;
		} 
				
		// crypte une chaine de caractères
		public static function crypte($a_texte,$a_cle)
		{
			srand((double)microtime()*1000000);
			$a_cleDEncryptage = md5(rand(0,32000) );
			$Compteur=0;
			$VariableTemp = "";
			for ($Ctr=0;$Ctr<strlen($a_texte);$Ctr++)
			{
				if ($Compteur==strlen($a_cleDEncryptage))
				  $Compteur=0;
				$VariableTemp.= substr($a_cleDEncryptage,$Compteur,1).(substr($a_texte,$Ctr,1) ^ substr($a_cleDEncryptage,$Compteur,1) );
				$Compteur++;
			}
			return base64_encode(self::getPublicCle($VariableTemp,$a_cle) );
		 }
		
		// decrypte une chaine de caractères
		public static function decrypte($a_texte,$a_cle)
		{
			$a_texte = self::getPublicCle(base64_decode($a_texte),$a_cle);
			$VariableTemp = "";
			for ($Ctr=0;$Ctr<strlen($a_texte);$Ctr++)
			{
				$md5 = substr($a_texte,$Ctr,1);
				$Ctr++;
				$VariableTemp.= (substr($a_texte,$Ctr,1) ^ $md5);
			}
			return $VariableTemp;
		} 
		
		  
		
/*_______________________________________________________________________________________________________________	
																				RETRO-COMPATIBILITE				
*/
	
		public static function valideTexte($a_texte)
		{
			return (self::txt2html($a_texte));	
		}
		
			
		public static function Db2Csv($a_texte)
		{
			return (self::html2csv($a_texte));
		}
		
		public static function Db2Txt($a_texte)
		{
			return(self::html2txt($a_texte));
		}					
		
	    public static function htmlAccentChars($a_texte, $a_accents=true)
        {
   			$a_texte = self::txt2html($a_texte);
       		if ($a_accents===false)
       			$a_texte = self::retireHtmlAccents($a_texte);
			return ($a_texte);
        }
		
		public static function txtToOct($a_texte)
		{
			return (self::txt2oct($a_texte));
		}
}	
