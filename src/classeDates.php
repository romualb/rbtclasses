<?php
/**
 * Classe de traitement des dates
 * regoupe les fonctions communes d'exploitation des dates
 * - convertion de dates en différents formats
 * - récupération des infos d'une date (année, mois, jour, heure)
 * - récupération d'une date à n jours d'intervalle
 * ...
 * formats de dates reconnus :
 * SQL		:	YYYY-MM-JJ H:i:s
 * STR		:	JJ/MM/YYYY H:i:s
 * FR		:	Jour JJ Mois YYYY
 * EN		:	Jour JJth Mois YYYY
 * UNX		:	timestamp unix
 * URL		:	YYYY/MM/JJ
 * USR		:	MM/JJ/YYYY (pour commande linux useradd)
 * RSS		:	Mon, 11 Aug 2008 14:18:58 (RFC822)
 * ORD		:	YYYYMMJJHHiiss (format de date ordonné)
 * ORD2		:   YYYY_MM_JJ_HH_ii_ss (format de date ordonné 2)

*/

	namespace Romualb\RbtClasses;

	setlocale (LC_TIME, 'fr_FR','fra');
	date_default_timezone_set("Europe/Paris");
	
	class classeDates
	{
	


//_______________________________________________________________________________________________________________	
//																				MEMBRES				
//

		protected $jours = array(
			'FR' => array(0=>"Dimanche",1=>"Lundi",2=>"Mardi",3=>"Mercredi",4=>"Jeudi",5=>"Vendredi",6=>"Samedi",7=>"Dimanche"),
			'EN' => array(0=>"Sunday",1=>"Monday",2=>"Tuesday",3=>"Wednesday",4=>"Thursday",5=>"Friday",6=>"Saturday",7=>"Sunday")
			);
		protected $joursC = array(
			'FR' => array (0=>"Dim.",1=>"Lun.",2=>"Mar.",3=>"Mer.",4=>"Jeu.",5=>"Ven.",6=>"Sam.",7=>"Dim."),
			'EN' => array(0=>"Sun.",1=>"Mon.",2=>"Tue.",3=>"Wed.",4=>"Thu.",5=>"Fri.",6=>"Sat.",7=>"Sun.")
			);
		protected $mois = array(
			'FR' => array(1=>"Janvier",2=>"Février",3=>"Mars",4=>"Avril",5=>"Mai",6=>"Juin",7=>"Juillet",8=>"Août",9=>"Septembre",10=>"Octobre",11=>"Novembre",12=>"Décembre"),
			'FRHTML' => array(1=>"Janvier",2=>"F&eacute;vrier",3=>"Mars",4=>"Avril",5=>"Mai",6=>"Juin",7=>"Juillet",8=>"Ao&ucirc;t",9=>"Septembre",10=>"Octobre",11=>"Novembre",12=>"D&eacute;cembre"),
			'EN' => array(1=>"January",2=>"February",3=>"March",4=>"April",5=>"May",6=>"June",7=>"July",8=>"August",9=>"September",10=>"October",11=>"November",12=>"December")
			);
		protected $moisC = array(
			'FR' => array(1=>"Janv.",2=>"Févr.",3=>"Mars",4=>"Avr.",5=>"Mai",6=>"Juin",7=>"Juil.",8=>"Août",9=>"Sept.",10=>"Oct.",11=>"Nov.",12=>"Déc."),
			'FRHTML' => array(1=>"Janv.",2=>"F&eacute;vr.",3=>"Mars",4=>"Avr.",5=>"Mai",6=>"Juin",7=>"Juil.",8=>"Ao&ucirc;t",9=>"Sept.",10=>"Oct.",11=>"Nov.",12=>"D&eacute;c."),
			'EN' => array(1=>"Jan.",2=>"Feb.",3=>"Mar.",4=>"Apr.",5=>"May",6=>"Jun.",7=>"Jul.",8=>"Aug.",9=>"Sep.",10=>"Oct.",11=>"Nov.",12=>"Dec.")
			);
		protected $joursOuvres = array(1,2,3,4,5,-2,-3,-4,-5,-6);		// jours ouvrés de la semaine 0(Dim)....6(Sam)
		protected $lng = "FR";
		
		private $charset = "UTF-8";
		public 	$date = "";
		public 	$jourCourt = false;
		public 	$moisCourt = false;
		public 	$heure = false;
		public 	$heureCourte = false;
		private $html = true;
		private $jourFerie = null;

		private $joursFeries = array();
	
//_______________________________________________________________________________________________________________	
//																				METHODES PRIVEES				
//
	
	
/**
		* convertit un nom de jour en numéro de jour de la semaine (lun.=1)
		* @param string $a_jour Nom du jour
		* @return int Numéro du jour (la semaine commence le lundi (1) et se termine le dimanche (7))
*/
		protected function _getNoJourSemaine($a_jour){
			$jour = strtolower($a_jour);
			switch ($jour)
			{
				case "lundi":		case "lun":
				case "monday":		case "mon":
					$num = 1;	break;
				case "mardi":		case "mar":
				case "tuesday":		case "tue":
					$num = 2;	break;
				case "mercredi":	case "mer":
				case "wednesday":	case "wed":
					$num = 3;	break;
				case "jeudi":		case "jeu":
				case "thursday":	case "thu":
					$num = 4;	break;
				case "vendredi":	case "ven":
				case "friday":		case "fri":
					$num = 5;	break;
				case "samedi":		case "sam":
				case "saturday":	case "sat":
					$num = 6;	break;
				case "dimanche":	case "dim":
				case "sunday":		case "sun":
					$num = 7;	break;
			}
			return ($num);
		}
		
		
		
		
		
/**
		* convertit un no de mois en nom (10=>octobre)
		* @param string $a_mois Numéro du mois
		* @return int Nom du mois
*/
		protected function _num2Month($a_mois)
		{
			$res = $this->moisCourt ? $this->moisC[$this->lng][$a_mois] : $this->mois[$this->lng][$a_mois];
			return($res);
		}
	


	


	

/*_______________________________________________________________________________________________________________	
																				METHODES PUBLIQUES				
*/


/**
		* constructeur
		* @param string $a_date date, si pas précisée, on prend en compte le membre $date à analyser (si la date n'est pas précisée, on prend en compte $this->date)
		* @param string $a_format Format d'écriture de la date
*/
		public function __construct($a_date="",$a_format="")
		{
			$this->jourCourt=false;
			$this->moisCourt=false;
			$this->heure=false;
			if (strlen($a_date)>0)
				$this->setDate($a_date,$a_format);
			$this->setHTML();
		}


		
/**
		* analyse une date
		* @param string $a_date date, si pas précisée, on prend en compte le membre $date à analyser (si la date n'est pas précisée, on prend en compte $this->date)
		* @return array {format,année,mois,jour,heure};
*/
		public function getInfosDate($a_date="")
		{
			$patternFr = "/^(lundi|lun|mardi|mar|mercredi|mer|jeudi|jeu|vendredi|ven|samedi|sam|dimanche|dim)?";
			$patternFr .= "\s*(\d{1,2})";
			$patternFr .= "\s*(janvier|jan|fevrier|février|f&eacute;vrier|fev|fév|f&eacute;v|mars|mar|avril|avr|mai|juin|jui|juillet|aout|août|ao&ucirc;t|aou|aoû|ao&ucirc;|septembre|sep|octobre|oct|novembre|nov|decembre|décembre|d&eacute;cembre|dec|déc|d&eacute;c)";
			$patternFr .= "\s*(\d{2,4})";
			$patternFr .= "\s?(\d{2}:\d{2}(:\d{2})*)?";
			$patternFr .= "$/i";

			$patternEn = "/^(monday|mon|tuesday|tue|wednesday|wed|thursday|thu|friday|fri|saturday|sat|sunday|sun)?";
			$patternEn .= "\s*(\d{1,2}(st|nd|rd|th))";
			$patternEn .= "\s*(january|jan|february|feb|march|mar|april|apr|may|june|july|august|aug|september|sep|october|oct|november|nov|december|dec)";
			$patternEn .= "\s*(\d{2,4})";
			$patternEn .= "\s?(\d{2}:\d{2}(:\d{2})*)?";
			$patternEn .= "$/i";
		
			$res=null;
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			if (strlen($date)>0)
				$res['date'] = $date;

			// timestamp
			if (is_numeric($date))
			{
				$res['format'] = "UNX";
				$res['annee'] = date("Y",$date);
				$res['mois'] = date("m",$date);
				$res['jour'] = date("j",$date);
				$res['heure'] = date("H:i:s",$date);
				$res['jourSemaine'] = date("w",$date);
				$res['jourMois'] = date("z",$date);
				$res['jourOuvre'] = in_array($res['jourSemaine'],$this->joursOuvres);
			}
			// SQL 					2008-08-11 15:30:21
			else if (preg_match("/^(\d{4})-(\d{2})-(\d{2})( \d{2}:\d{2}:\d{2})?$/",$date,$l_date))
			{
				$res['format'] = "SQL";
				$res['annee'] = $l_date[1];
				$res['mois'] = $l_date[2];
				$res['jour'] = $l_date[3];
				$res['heure'] = isset($l_date[4]) ? trim($l_date[4]) : "00:00:00";
			}
			// STR 					11/08/2008 ou 11/08/2008 12:20:30
			else if (preg_match("/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})(\s?\d{2}:\d{2}(:\d{2})?)?$/",$date,$l_date))
			{
				$res['format'] = "STR";
				$res['annee'] = $l_date[3];
				$res['mois'] = sprintf("%02d",$l_date[2]);
				$res['jour'] = sprintf("%02d",$l_date[1]);
				$res['heure'] = isset($l_date[4]) ? trim($l_date[4]) : "00:00:00";
			}
			// RSS					Mon, 11 Aug 2008 14:18:58
			else if (preg_match("/^(\w{3}), (\d{1,2}) (\w{1,3}) (\d{2,4}) (\d{2}:\d{2}:\d{2})$/i",$date,$l_date))
			{
				$res['format'] = "RSS";
				$res['annee'] = $l_date[4];
				$res['mois'] = $this->month2Num($l_date[3]);
				$res['jour'] = $l_date[2];
				$res['heure'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}

			// FR 					lundi 11 aout 2008 14:18:58
			else if (preg_match($patternFr,$date,$l_date))
			{
				$res['format'] = "FR";
				$res['annee'] = $l_date[4];
				$res['mois'] = $this->month2Num($l_date[3]);
				$res['jour'] = $l_date[2];
				$res['heure'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}


			// EN 					Monday 11th august 2008 14:18:58
			else if (preg_match($patternEn,$date,$l_date))
			{
				$res['format'] = "EN";
				$res['annee'] = $l_date[4];
				$res['mois'] = $this->month2Num($l_date[3]);
				$res['jour'] = $l_date[2];
				$res['heure'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}

			// FIL 					2008_08_11_14_18_58
			else if (preg_match("/^(\d{4})_(\d{2})_(\d{2})(_(\d{2})_(\d{2})_(\d{2}))?$/",$date,$l_date))
			{
				$res['format'] = "FIL";
				$res['annee'] = $l_date[1];
				$res['mois'] = $l_date[2];
				$res['jour'] = $l_date[3];
				$res['heure'] = isset($l_date[4]) ? $l_date[5].":".$l_date[6].":".$l_date[7] : "00:00:00";
			}

			// ISO 8601             2004-02-12T15:19:21+00:00 ou 2004-02-12T15:19:21+0000
            else if (preg_match("/^(\d{4})-(\d{2})-(\d{2})T((\d{2}):(\d{2}):(\d{2}))\+(\d{2}):?(\d{2})$/",$date,$l_date))
            {
				$res['format'] = "ISO";
				$res['annee'] = $l_date[1];
				$res['mois'] = $l_date[2];
				$res['jour'] = $l_date[3];
				$res['heure'] = isset($l_date[4]) ? $l_date[5].":".$l_date[6].":".$l_date[7] : "00:00:00";
            }

			if ($res!==false)
			{
			    if (isset($res['heure']))
				    $time = explode(":",$res['heure']);
				$res['H']=isset($time[0]) ? $time[0] : 0;
				$res['i']=isset($time[1]) ? $time[1] : 0;
				$res['s']=isset($time[2]) ? $time[2] : 0;
			}


			$this->infos = $res;
			return ($res);
			
		}



		
/**
		* définit la langue du calendrier
		* @param string $a_langue FR, EN
*/
		public function setLangue($a_langue)
		{
			$this->lng=$a_langue;
		}


/**
		* définit si le texte doit être écrit en HTML
		* @param bool $a_bool true par défaut
*/
		public function setHTML($a_bool=true)
		{
			$this->html=$a_bool;
			$this->mois['FR']=$this->mois['FRHTML'];
			$this->moisC['FR']=$this->moisC['FRHTML'];
		}

		
/**
		* définit / change la date en cours
		* @param string $a_date nouvelle date
		* @param string $a_format format de date 
*/
		public function setDate($a_date,$a_format="")
		{
			$infos = $this->getInfosDate($a_date);
			if ($infos['heure']!="00:00:00")
				$this->setHeure();
			$this->date = (strlen($a_format)>0) ? $this->convert($a_format,$a_date) : $a_date;
		}

		
/**
		* retourne le no du mois, en français et anglais
		* @param string $a_mois nom du mois
		* @return string numéro du mois
*/
		public function month2Num($a_mois)
		{
			$num="";
			switch (mb_strtolower($a_mois,$this->charset))
			{
				case "janvier":
				case "jan":
				case "january":
					$num = "01";		
					break;
					
				case "fevrier":				case "février":				case "f&eacute;vrier":
				case "fev":					case "fév":					case "f&eacute;v":
				case "february":
				case "feb":
					$num = "02";		
					break;
					
				case "mars":
				case "mar":
				case "march":
					$num = "03";		
					break;
					
				case "avril":
				case "avr":
				case "april":
				case "apr":
					$num = "04";		
					break;
					
				case "mai":
				case "may":
					$num = "05";		
					break;
					
				case "juin":
				case "jun":
				case "june":
					$num = "06";		
					break;
					
				case "juillet":
				case "jul":
				case "july":
					$num = "07";		
					break;
					
				case "aout":				case "août":				case "ao&ucirc;t":		
				case "aou":					case "aoû":					case "ao&ucirc;":
				case "august":
				case "aug":
					$num = "08";		
					break;
					
				case "septembre":
				case "sep":
				case "september":
					$num = "09";		
					break;
					
				case "octobre":
				case "oct":
				case "october":
					$num = "10";		
					break;
					
				case "novembre":
				case "nov":
				case "november":
					$num = "11";		
					break;
					
				case "decembre":			case "décembre":			case "d&eacute;cembre": 		
				case "dec":					case "déc":					case "d&eacute;c":
				case "december":
					$num = "12";		
					break;
			}
			return($num);

		}
		
			
		
		

/**
		* retourne la date en cours
		* @param string $a_format format de retour
		* @return string date
*/
		public function getDate($a_format="")
		{
			$date = $this->convert($a_format);
			return ($date);
		}

	
/**
		* retourne l'année d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string année
*/
		public function getAnnee($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$infos = $this->getInfosDate($date);
			if (isset($infos['annee']))
				return($infos['annee']);
		}
		
/**
		* retourne le no de mois d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string numéro du mois
*/
		public function getMois($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return(date("m",$date));
		}

/**
		* retourne le nom du mois d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string nom du mois
*/
		public function getNomMois($a_date="")
		{
			$noMois = intval($this->getMois($a_date));
			return ($this->moisCourt ? $this->moisC[$this->lng][$noMois] : $this->mois[$this->lng][$noMois]);
		}
		
/**
		* retourne le no de semaine d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string no de la semaine
*/
		public function getSemaine($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return(date("W",$date));
		}

/**
		* retourne le jour d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string numéro du jour dans le mois (01 à 31)
*/
		public function getJour($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$infos = $this->getInfosDate($date);
			return($infos['jour']);
		}

/**
		* retourne le nom du jour d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string nom du jour de la semaine
*/
		public function getNomJour($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return ($this->jourCourt ? $this->joursC[$this->lng][gmdate("w",$date)] : $this->jours[$this->lng][gmdate("w",$date)]);
		}



/**
		* retourne le numéro du jour de la semaine à partir d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return int numero du jour de la semaine
*/
		public function getNumJour($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return (gmdate("w",$date));
		}



		
/**
		* retourne l'heure d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string heure
*/
		public function getHeure($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$infos = $this->getInfosDate($date);
			return($infos['heure']);
		}		
		

		
		
		
/**
		* retourne le format d'une date
		* @param string $a_date date, si pas précisée, on prend en compte le paramètre $date
		* @return string format
*/
		public function getFormat($a_date="")
		{
			$date = strlen($a_date) ? $a_date : $this->date;
			$infos = $this->getInfosDate($date);
			return($infos['format']);
		}


/**
		* vérifie si une année est bisextile
		* @param string $a_annee année à tester, si elle n'est pas précisée, on teste sur le paramètre $date
		* @return boolean
*/
		public function isBisextile($a_annee="")
		{
			$date = strlen($a_annee) ? gmmktime(1, 0, 0, 1, 1, $a_annee) : $this->convert("UNX",$this->date);
			return(date("L",$date)==1);
		}

/**
		* nombre de jours dans le mois
		* @param string $a_mois no du mois ou mois littéral, si pas précisé on prend le mois du paramètre $date
		* @param string $a_annee année YYYY, si pas précisé on prend l'année du paramètre $date
		* @return int nombre de jours dans le mois
*/
		public function getJoursMois($a_mois="", $a_annee="")
		{
			// transformation du mois littéral
			if (strlen($a_mois)>0)
			{
				$mois = ($a_mois>=1 && $a_mois<=12) ? $a_mois : $this->month2Num($a_mois);
			}
			else 
				$mois = $this->getMois();
			$a_annee = (strlen($a_annee)>0 ? $a_annee : $this->getAnnee());
			$date = gmmktime(12, 0, 0, intval($mois), 1, $a_annee);
			return (date("t",$date));
		}
		




/**
		  * date correspondant à un jour d'une semaine (ex: le mardi de la 12è semaine) 
		  * @param string $a_semaine no de semaine
		  * @param string $a_jour jour recherché (par défaut, lundi)
		  * @param string $a_format unix par défaut
		  * @param string $a_annee année, par défaut celle du paramètre $date
		  * @return string date
*/  
		 
		public function getJourSemaine($a_semaine,$a_jour="lundi",$a_format="UNX",$a_annee="")
		{
			if (strlen($a_annee)==0)
				$a_annee = $this->getAnnee();
			if(strftime("%W",gmmktime(0,0,0,01,01,$a_annee))==1)
				$mon_mktime = gmmktime(0,0,0,01,(01+(($a_semaine-1)*7)),$a_annee);
			else
				$mon_mktime = gmmktime(0,0,0,01,(01+(($a_semaine)*7)),$a_annee);
			
			// le 04 janvier est toujours en première semaine
			// on cherche le jour du 04 janvier pour calculer le décalage
			$decalage = (date("w",$mon_mktime)-1)*60*60*24;
			$quatreJan = date("w",gmmktime(1,0,0,1,4,$a_annee));
		
			// si le 04 janvier tombe du jeudi au dimanche
			if ($quatreJan==0 || $quatreJan>=4)
			{
				// si le 1er janvier tombe un lundi
				if (date("w",gmmktime(1,0,0,1,1,$a_annee))==1)
					$decalage = $decalage;
				else 
					$decalage = $decalage + (7*60*60*24);
			}
			// sinon si le 1er janvier tombe un dimanche
			else if (date("w",gmmktime(1,0,0,1,1,$a_annee))==0)
			{
				$decalage = $decalage + (7*60*60*24);
			}	
			$noJour = $this->_getNoJourSemaine($a_jour)-1;
			$jour = $mon_mktime - $decalage + ($noJour*60*60*24);
			$date = $this->convert($a_format,$jour);
			
			return ($date);
		}
		



/**
		  * retourne un jour dans la même semaine que le jour demandé (ex: le lundi de la semaine du 15 aout 2016)
          * @param string $a_format de retour, unix par défaut
          * @param $a_jour jour recherché (par défaut, lundi)
		  * @param string $a_date date de référence
		  * @return string date
*/
		public function getJourOfWeek($a_jour='lundi',$a_format="UNX",$a_date='')
        {
            $a_date = strlen($a_date)>0 ? $a_date : $this->date;
            $noJour = $this->_getNoJourSemaine($a_jour);
            $jourRef = intval($this->_getNoJourSemaine($this->getNomJour($a_date)));
            $decalage = $noJour-$jourRef;
            $date = $this->getDateFrom($decalage,$a_format,$a_date);
            return ($date);
        }






		
/**
		* nombre de jours entre deux dates
		* @param string $a_dateFrom début de la période
		* @param string $a_dateTo fin de la période
		* @return int nombre de jours
*/		
		public function getJoursPeriode($a_dateFrom,$a_dateTo)
		{
			$a_dateFrom = $this->convert("UNX",$a_dateFrom);
			$a_dateTo = $this->convert("UNX",$a_dateTo);
			return ( (($a_dateTo-$a_dateFrom)/60/60/24) + 1);
		}
		
		
	
/**
		* convertion d'une date dans un autre format
		* @param string $a_format format de retour (par défaut, le même que la date)
		* @param string $a_date date à convertir, si pas précisée, on prend en compte le paramètre $date
		* @return int nombre de jours
*/		
		public function convert($a_format="",$a_date="")
		{
			$res = false;
			// analyse de la date saisie
			$l_date = $this->getInfosDate(strlen($a_date)>0 ? $a_date : '');

			if (strlen($a_format)==0) 
				$a_format = $l_date['format'];
			
			if (isset($l_date['format']) && $l_date['mois']>0 && $l_date['jour']>0)
			{
				$heures = explode(":",$l_date['heure']);
				if ($this->heure)
					$timestamp = gmmktime($heures[0],$heures[1],(isset($heures[2]) ? $heures[2] : 0),intval($l_date['mois']),intval($l_date['jour']),intval($l_date['annee']));
				else 
					$timestamp = gmmktime(0,0,0,intval($l_date['mois']),intval($l_date['jour']),intval($l_date['annee']));
				switch($a_format)
				{
					case "SQL":                                      
						$res = gmdate("Y-m-d".($this->heure ? " H:i:s": ""),$timestamp);
						break;
					case "STR":
						$res = gmdate("d/m/Y".($this->heure ? " H:i".(!$this->heureCourte ? ":s": ""): ""),$timestamp);
						break;
					case "FR":
						$res =  ($this->jourCourt ? $this->joursC['FR'][gmdate("w",$timestamp)] : $this->jours['FR'][gmdate("w",$timestamp)]) . 
								gmdate(" d ",$timestamp) . 
								($this->moisCourt ? $this->moisC['FR'][gmdate("n",$timestamp)] : $this->mois['FR'][gmdate("n",$timestamp)]) . 
								gmdate(" Y".($this->heure ? " H:i".(!$this->heureCourte ? ":s": ""): ""),$timestamp);
						break;
					case "EN":
						$day = gmdate(" d",$timestamp);
						if (preg_match("/1$/",$day))		$jour = $day."st ";
						else if (preg_match("/2$/",$day))	$jour = $day."nd ";
						else if (preg_match("/3$/",$day))	$jour = $day."rd ";
						else $jour = $day."th ";
						$res =  ($this->jourCourt ? $this->joursC['EN'][gmdate("w",$timestamp)] : $this->jours['EN'][gmdate("w",$timestamp)]) . 
								 $jour.
								($this->moisCourt ? $this->moisC['EN'][gmdate("n",$timestamp)] : $this->mois['EN'][gmdate("n",$timestamp)]) . 
								gmdate(" Y".($this->heure ? " H:i".(!$this->heureCourte ? ":s": ""): ""),$timestamp);
						break;
					case "UNX":
						$res = $timestamp;
						break;
					case "URL":
						$res = gmdate("Y/m/d",$timestamp);
						break;
					case "USR":
						$res = gmdate("m/d/Y",$timestamp);
						break;
					case "RSS":
						$res = gmdate("D, d M Y H:i:s",$timestamp);
						break;
					case "ORD":
						$res = gmdate("Ymd".($this->heure ? "His": ""),$timestamp);
						break;
					case "FIL":
						$res = gmdate("Y_m_d".($this->heure ? "_H_i_s": ""),$timestamp);
						break;
                    case "ISO":
                        $res = gmdate(DATE_ISO8601,$timestamp);
                        break;
						
				}
			}
			return($res);
		}


/**
		* prise en compte du format de jour court (Lun.)
		* @param bool $a_bool true par défaut
*/
		public function setJourCourt($a_bool=true)
		{
			$this->jourCourt = $a_bool;
		}

/**
		* prise en compte du format de mois court (Jan.)
		* @param bool $a_bool true par défaut
*/
		public function setMoisCourt($a_bool=true)
		{
			$this->moisCourt = $a_bool;
		}


/**
		* prise en compte de l'heure complète (HH:mm:ss)
		* @param bool $a_bool true par défaut
*/
		public function setHeure($a_bool=true)
		{
			$this->heure = $a_bool;
		}


/**
		* prise en compte de l'heure sans les secondes (HH:mm)
		* @param bool $a_bool true par défaut
*/
		public function setHeureCourte($a_bool=true)
		{
			$this->heure = true;
			$this->heureCourte = $a_bool;
		}


	/**
	 * corrige un ecart en prenant en compte les jours ouvrés
     * le décalage peut se faire dans les 2 sens
     *  L  M  M  J  V  S  D  L  M  M  J  V  S
     * -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6
	 * @param $a_ecart
	 * @param $a_date
	 * @return int
	 */
	private function _corrigeEcart($a_ecart,$a_date)
	{
		// numéro de jour initial
		$numJour = $this->getNumJour($a_date);
		$ecartCorrige = 0;
		$i=1;
		do
		{
		    if ($a_ecart>0)
            {
                $ecartCorrige++;
                $numJour++;
            }
            else
            {
                $ecartCorrige--;
                $numJour--;
            }
			if (abs($numJour)==7) $numJour=0;
			if (!in_array($numJour,$this->joursOuvres))
            {
				continue;
            }
			$i++;
		} while ($i<=abs($a_ecart));

		return $ecartCorrige;
	}





/**
		* retourne la date à n jours d'écart
		* @param int $a_ecart nb d'unites d'écart
		* @param string $a_format format de retour, si pas précisé on prend le même que le paramètre $date
		* @param string $a_date date de référence, si pas précisée on prend le paramètre $date
		* @param string $a_intervalle intervalle de temps au format date():M,D,H,i,s - Jour par défaut (D)
 		* @param bool $a_ouvres ne compte que les jours ouvrés
		* @return string date
*/
		public function getDateFrom($a_ecart,$a_format="",$a_date="",$a_intervalle="D",$a_ouvres=false)
		{
			$l_date = strlen($a_date)>0 ? $a_date : $this->date;
			$infos = $this->getInfosDate($l_date);
			// on corrige le nombre de jours d'écart en tenant compte des jours ouvrés de la semaine
			if ($a_ouvres && $a_intervalle=="D")
				$a_ecart = $this->_corrigeEcart($a_ecart,$l_date);

			// format de date
			if (strlen($a_format)==0)
				$a_format = $infos['format'];

			// on utilise pas gmmktime pour ne pas fausser les calculs sur les heures
			switch ($a_intervalle)
			{
				case "M":
					$unxNewDate = mktime($infos['H'], $infos['i'], $infos['s'], ($infos['mois']+$a_ecart), $infos['jour'], $infos['annee']);
					break;
				case "D":
					$unxNewDate = mktime($infos['H'], $infos['i'], $infos['s'], $infos['mois'], ($infos['jour']+$a_ecart), $infos['annee']);
					break;
				case "H":
					$unxNewDate = mktime(($infos['H']+$a_ecart), $infos['i'], $infos['s'], $infos['mois'], $infos['jour'], $infos['annee']);
					break;
				case "i":
					$unxNewDate = mktime($infos['H'], ($infos['i']+$a_ecart), $infos['s'], $infos['mois'], $infos['jour'], $infos['annee']);
					break;
				case "s":
					$unxNewDate = mktime($infos['H'], $infos['i'], ($infos['s']+$a_ecart), $infos['mois'], $infos['jour'], $infos['annee']);
					break;
			}
			$dateDecalee = $this->convert($a_format,$unxNewDate);

			// on vérifie qu'il n'y a pas un jour férié dans la période ou que l'on ne tombe pas sur un jour ferié
			if ($a_ouvres && $a_intervalle=="D")
			{
				$this->_addJoursFeries($infos['annee']);
				$infos2 = $this->getInfosDate($dateDecalee);
				$this->_addJoursFeries($infos2['annee']);
				$ecartSup = 0;
				foreach ($this->joursFeries as $jourF)
				{
					$ecartJourFerie = $this->getEcartDates($jourF['date'],$l_date);
					if ($ecartJourFerie["D"]>0 && $ecartJourFerie["D"]<=abs($a_ecart) && $jourF['jourOuvre'])
						$ecartSup++;
				}
				if ($ecartSup>0)
					$dateDecalee = $this->getDateFrom($ecartSup,$a_format,$dateDecalee,$a_intervalle="D",$a_ouvres=true);
			}




			return ($dateDecalee);
			
		}
	



/**
		* retourne le premier jour du nème mois d'écart
		* @param int $a_nbMois nb de mois d'écart
		* @param string $a_format format de retour, si pas précisée on prend le même que le paramètre $date
		* @param string $a_date date de référence, si pas précisée on prend le paramètre $date
		* @return string date
*/
		public function getMoisFrom($a_nbMois,$a_format="",$a_date="")
		{
			$l_date = strlen($a_date)>0 ? $a_date : $this->date;
			// format de date
			$infos = $this->getInfosDate($l_date);
			// format de date
			if (strlen($a_format)==0)
				$a_format = $infos['format'];

			$unxNewDate = gmmktime(0, 0, 0, $infos['mois']+$a_nbMois  , 1, $infos['annee']);
			return ($this->convert($a_format,$unxNewDate));
		}
	

/**
		* retourne le premier jour du mois
		* @param string $a_format format de retour, si pas précisée on prend le même que le paramètre $date
		* @param string $a_date date de référence, si pas précisée on prend le paramètre $date
		* @return string date
*/
		public function getPremierJourMois($a_format="",$a_date="")
		{
			$l_date = strlen($a_date)>0 ? $a_date : $this->date;
			// format de date
			$infos = $this->getInfosDate($l_date);
			// format de date
			if (strlen($a_format)==0)
			{
				$a_format = $infos['format'];
			}
			$unxNewDate = gmmktime(0, 0, 0, $infos['mois'], 1, $infos['annee']);
			return ($this->convert($a_format,$unxNewDate));
		}

/**
		* retourne le dernier jour du mois
		* @param string $a_format format de retour, si pas précisée on prend le même que le paramètre $date
		* @param string $a_date date de référence, si pas précisée on prend le paramètre $date
		* @return string date
*/
		public function getDernierJourMois($a_format="",$a_date="")
		{
			$l_date = strlen($a_date)>0 ? $a_date : $this->date;
			// format de date
			$infos = $this->getInfosDate($l_date);
			// format de date
			if (strlen($a_format)==0)
			{
				$a_format = $infos['format'];
			}
			$unxNewDate = gmmktime(0, 0, 0, $infos['mois']+1  , 0, $infos['annee']);
			return ($this->convert($a_format,$unxNewDate));
		}

		
		
/**
		* calcule l'ecart entre deux dates
		* si date1 est anterieure à date2, le resultat est négatif
		* @param string $a_date1 date 1
		* @param string $a_date2 date 2 
		* @return array {M,D,H,i,s} avec le nombre de MOIS(M), JOURS(D), HEURES(H), MINS(i), SECS(s)
*/
		public function getEcartDates($a_date1,$a_date2)
		{
			$nb = round( ($this->convert("UNX",$a_date1) - $this->convert("UNX",$a_date2)));
			$res["M"] = $nb/(60*60*24*30.5);
			$res["D"] = $nb/(60*60*24);
			$res["H"] = $nb/(60*60);
			$res["i"] = $nb/60;
			$res["s"] = $nb;
			return($res);
		}	



/**
		* retourne la date la plus grande d'une liste de dates
		* si une date de la liste n'est pas valide, elle n'est pas prise en compte
		* @param array $a_dates liste de dates à comparer
		* @param string $a_format format de retour demandé
		* @return false si toutes les valeurs passées ne sont pas des dates reconnues, 
		* retourne la date au format demandé, sinon celui de la date max
*/
		public function getDateMax($a_dates,$a_format="")
		{
			$dateMax = false;
			if (!is_array($a_dates))
				$dateMax = $this->getInfosDate($a_dates)===false ? false : $a_dates;
			else
			{
				foreach ($a_dates as $date)
				{
					if ($this->getInfosDate($date)!==false)
					{
						$curDate = $this->convert("UNX",$date);
						if (!isset($res) || $curDate>$res)
						{
							$res = $curDate;
							$dateMax = $date;
						}
						$dateMax = strlen($a_format)>0 ? $this->convert($a_format,$dateMax) : $dateMax; 
					}
				} 
			}
			return ($dateMax);
		}	
		
/**
		* retourne la date la plus petite d'une liste de dates
		* si une date de la liste n'est pas valide, elle n'est pas prise en compte
		* @param array $a_dates liste de dates à comparer
		* @param string $a_format format de retour demandé
		* @return false si toutes les valeurs passées ne sont pas des dates reconnues, 
		* retourne la date au format demandé, sinon celui de la date min
*/
		public function getDateMin($a_dates,$a_format="")
		{
			$dateMin = false;

			if (!is_array($a_dates))
				$dateMin = $this->getInfosDate($a_dates)===false ? false : $a_dates;
			else
			{
				foreach ($a_dates as $date)
				{
					if ($this->getInfosDate($date)!==false)
					{
						$curDate = $this->convert("UNX",$date);
						if (!isset($res) || $curDate<$res)
						{
							$res = $curDate;
							$dateMin = $date;
						}
						$dateMin = strlen($a_format)>0 ? $this->convert($a_format,$dateMin) : $dateMin;
					}
				} 
			}
			return ($dateMin);
		}	
		
		
		
		
/**
		* liste de date à intervalle régulier : n * Jours(D)|Mois(M)
		* @param string $a_date1 date de début
		* @param string $a_date2 date de fin
		* @param int $a_pas ecart entre les dates (n)
		* @param string $a_type type d'ecart : D/M
		* @return array {date(1),date(2),date(3),....date(n)} 
*/		
		function getDatesIntervalle($a_date1,$a_date2,$a_format,$a_pas=1,$a_type="M")
		{
			$dates = array();
			// format de date
			$this->setHeure(false);
			$a_date1 = $this->convert("UNX",$a_date1);
			$a_date2 = $this->convert("UNX",$a_date2);
			
			if ($a_date1<=$a_date2)
			{
				while ($a_date1<=$a_date2)
				{
					$date = $this->convert($a_format,$a_date1);
					if ($a_type=="D")
						$a_date1 = $this->getDateFrom($a_pas,"UNX",$a_date1);
					else if ($a_type=="M")
						$a_date1 = $this->getMoisFrom($a_pas,"UNX",$a_date1);
					array_push($dates,$date);
				}
			}
			return $dates;
		}

		
		
/**
		* convertir un nb de secondes en heure et jours
		* @param integer $a_secs nombre de secondes
		* @return array {jours,heures,minutes,secondes,duree)
*/		
		function secsToTime($a_secs)
		{
			$res["D"] = intval($a_secs/(60*60*24));
			$rest = $a_secs%(60*60*24);
			$res["H"] = intval($rest/(60*60));
			$rest = $rest%(60*60);
			$res["i"] = intval($rest/60);
			$rest = $rest%(60);
			$res["s"] = $rest;
			$res["duree"] = ($res["D"]>0 ? $res["D"]."j " : "") . ($res["H"]>0 ? $res["H"]."h " : "") . ($res["i"]>0 ? $res["i"]."mn " : "") . ($res["s"]>0 ? $res["s"]."s " : "");
			if (strlen($res["duree"])==0) $res["duree"]=(intval($a_secs*1000))."ms";
			return($res);
		}
		
		
/**
		* convertir une heure en nb de secondes
		* @param string $a_time heure au format HH:ii:ss ou HH:ii
		* @return array {heures,minutes,secondes)
*/		
		function timeToSecs($a_time)
		{
			preg_match("/(\d{2}):(\d{2})(:(\d{2}))?/",$a_time,$parts);
			$res = $parts[1]*60*60 + $parts[2]*60 + (isset($parts[4]) ? $parts[4] : 0);  
			return($res);
		}


	/**
	 * @param $a_annee
	 */
	private function _addJoursFeries($a_annee)
	{
		$easterDate = easter_date($a_annee);
		$easterDay = date('j', $easterDate);
		$easterMonth = date('n', $easterDate);
		$easterYear = date('Y', $easterDate);

		$joursFeries = array(
			// Dates fixes
			"1er janvier ".$a_annee => 			$this->getInfosDate(gmmktime(0, 0, 0, 1, 1, $a_annee)), // 1er janvier
			"Fête du travail ".$a_annee => 		$this->getInfosDate(gmmktime(0, 0, 0, 5, 1, $a_annee)), // Fête du travail
			"Victoire des alliés ".$a_annee => 	$this->getInfosDate(gmmktime(0, 0, 0, 5, 8, $a_annee)), // Victoire des alliés
			"Fête nationale ".$a_annee => 		$this->getInfosDate(gmmktime(0, 0, 0, 7, 14, $a_annee)), // Fête nationale
			"Assomption ".$a_annee => 			$this->getInfosDate(gmmktime(0, 0, 0, 8, 15, $a_annee)), // Assomption
			"Toussaint ".$a_annee => 			$this->getInfosDate(gmmktime(0, 0, 0, 11, 1, $a_annee)), // Toussaint
			"Armistice ".$a_annee => 			$this->getInfosDate(gmmktime(0, 0, 0, 11, 11, $a_annee)), // Armistice
			"Noël ".$a_annee => 				$this->getInfosDate(gmmktime(0, 0, 0, 12, 25, $a_annee)), // Noel
			// Dates variables
			"Pâques ".$a_annee => 				$this->getInfosDate(gmmktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear)), // pâques
			"Ascension ".$a_annee => 			$this->getInfosDate(gmmktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear)), // Ascension
			"Pentecôte ".$a_annee => 			$this->getInfosDate(gmmktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear)), // Pentecôte
		);
		$this->joursFeries = array_merge($this->joursFeries,$joursFeries);
	}



		/**
		 * indique si le jour en entrée est un jour férié
		 * @param string $date
		 * @return bool
		*/
		public function isFerie($a_date="")
		{

			$l_date = strlen($a_date)>0 ? $a_date : $this->date;

			$infosDate = $this->getInfosDate($l_date);
			$date = $this->convert("UNX",$l_date);
			$year = $infosDate["annee"];

			$this->_addJoursFeries($year);

			foreach ($this->joursFeries as $jour=>$jourF) {
				if ($jourF['date'] == $date) {
					$this->jourFerie = $jour;
					return(true);
				}
			}
			return (false);
		}


		/**
		 * bnom du jour férie en cours
		 * @return mixed
		 */
		public function getJourFerie()
		{
			return ($this->jourFerie);
		}



		/**
		 * vérifie qu'un jour est ouvré, selon la liste des $this->joursOuvres et jours fériés
		 * @param $a_date
		 */
		public function isJourOuvre($a_date)
		{
			$l_date = strlen($a_date)>0 ? $a_date : $this->date;
			return ( in_array($this->getNumJour($l_date),$this->joursOuvres) && !$this->isFerie($l_date) );
		}


}
