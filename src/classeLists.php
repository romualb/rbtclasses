<?php
/**
 * Classe de traitement des listes
 *
 * Affichage avancé de listes html avec tri et pagination
 * ...
 *
*/

	namespace Romualb\RbtClasses;

	if (!defined("CURRENT_PAGE"))
		define ("CURRENT_PAGE",			basename($_SERVER['PHP_SELF']));				// page courante

	class classeLists
	{

		// initialisation
		private $listeName = "defaut";				// nom de la liste
		private $url=CURRENT_PAGE;					// url de la page où se trouve la liste la liste
		private $pageOk = true;						// vérifie que l'url ne se termine pas par /
		private $jQuery = false;					// jQuery actif
		private $displayAll = false;				// booléen

		// variables de navigation
		private $pas = 30; 							// pas : nombre de réultats pas page
		private $selectPas = false; 				// code pour select du pas
		private $page = 1; 							// page

		// variables de tri
		private $currentOrd;						// champ actuellement trié
		private $currentSens;						// sens de tri actuel

		// variables de configuration de la liste
		private $messNoData = "Aucune donn&eacute;e";	// texte 0 résultat
		private $iconesTri = false;					// affichage des icones de tri
		private $navig = false;						// activation de la pagination
		private $header = true;						// affichage de l'entete de liste par défaut
		private $codeNavig = "";					// code de pagination
		private $codePas = "";						// code de select pas
		private $codeJquery = "";					// code jquery
		private $codeJqueryFilter = "";				// code jQuery pour les filtres
		private $codeListe = "";					// code de la liste;
		private $nbPagesNavig = 10;					// nombre de No de pages affichés dans la pagination
		private $arguments = "";							// arguments supplémentaires sur les liens de tri
		private $posNavig;							// position de la pagination
		private $listeClassCSS=array();				// liste des attributs CSS class à appliquer

		// liste
		private $liste = array();					// liste

		private $displayTotal=false;				// ligne de total
		private $displaySousTot=false;				// ligne de sous total
		private $displayRatioTot=false;				// ligne de ratioTotal
		private $displayFilters=false;				// ligne de filtres

		private $paramsLigneTotal = array();		// paramètres de la ligne de total
		private $paramsLigneSousTotal = array();	// paramètres de la ligne de sous total
		private $paramsLigneRatioTotal = array();	// paramètres de la ligne de ratioTotal
		private $paramsNumLignes = array();			// paramètres pour numérotation des lignes

		private $ligneSup = array();				// ligne supplémentaire ligneSup[no ligne]="code HTML"
		private $ligneSupFin = "";					// ligne supplémentaire à la fin du tableau

		// données
		private $datas = null;						// données à afficher
		private $sourceDatas = null;				// source des données SQL, ARRAY, JSON
		private $nbLignes;							// nombre de lignes à afficher
		private $colonnes = array();				// liste des colonnes à afficher
		private $genCols = array();					// colonnes à prendre en compte pour la génération

		// Etiquettes
//		private $nbLignesEtiquettes = 7; 			// Nombre de lignes dans une etiquette, comptabilise les lignes vides et les lignes avec des informations
		private $nbEtiquettesParPage = array(3,6);	// Nombre d'étiquettes par colonne (nb col * nb lig)
		private $labelsPrintPageCssFile = "";

		// export
		private $nomFichierExport = "export";		// nom du fichier exporté, sans l'extension
		private $sensExportPage = "P";				// sens de l'export : P(portrait) L(paysage)
		private $encodeExport = null;				// encoding pour export via html2pdf
		private $outputExport = "UPLOAD";			// type d'export : upload (UPLOAD) ou sauvegarde fichier (SAVE)
		private $feuillesXls = array();				// liste de feuilles pour l'export xls;
		private $exportDir = ".";					// repertoire d'export
		// parametres d'export vers liste de destinataires
		private $destsParams = array(
			'champNom'=>"nom",
			'champPrenom'=>"prenom",
			'champEmail'=>"email",
			'separateur'=>","
		);




/*_______________________________________________________________________________________________________________
																				METHODES PRIVEES
*/



/**
 * change le sens à indiquer dans le lien selon la valeur en session
 * @param string $a_sens sens par défaut
 * @param string $a_idColonne champ concerné
*/
		private function _setSens($a_sens, $a_idColonne)
		{
			$ord = "";
			$ord = $this->getSensListe($a_idColonne);
			if ($ord=="ASC" && $this->getConfListe('ord')==$a_idColonne)
				return("DESC");
			else
				return("ASC");
		}



/**
 * retourne le lien pour changer l'ordre des colonnes des listes
 * @param string $a_colone nom du champ concerné
 * @param string $a_sens sens (ASC/DESC)
 * @return string url
*/
		private function _getUrlOrdreListe($a_colone,$a_sens)
		{
			$this->addArguments("page=1&ord=".$a_colone."&sens=".$this->_setSens($a_sens,$a_colone)."&liste=".$this->listeName);
			$res = $this->_addUrlArgs();
			return ($res);
		}


/**
 * ajoute les arguments $this->arguments à l'url en cours
 * @return string url
 */
		private function _addUrlArgs()
		{
			$url = parse_url($_SERVER['REQUEST_URI']);
			$path = $url['path'];
			// arguments à ajouter
			parse_str($this->arguments,$newArgs);

			$resArgs = array();

			if (isset($url['query']))
			{
				// arguments courants
				parse_str($url['query'],$curArgs);
				// on ne garde que les arguments précédents qui ne changent pas
				foreach ($curArgs as $arg=>$val)
				{
					if (!isset($newArgs[$arg]))
						array_push($resArgs,$arg."=".$val);
				}
			}
			// on ajoute les nouveaux
			foreach ($newArgs as $arg=>$val)
				array_push($resArgs,$arg."=".$val);

			$res = $path.($this->pageOk ? "?" : "&").implode("&",$resArgs);

// 			$this->arguments = "";

			return ($res);
		}



/**
 * retourne le lien pour page de navigation
 * @param int $a_page numero de page
 * @return string lien de navigation
*/
		private function _getUrlPage($a_page)
		{
			$this->addArguments("page=".$a_page);
			$res = $this->_addUrlArgs();
			return ($res);

		}




/**
 * suppression d'arguments de l'url en cours
 * @param array $a_args liste d'arguments (arg1,arg2)
*/
		private function _removeArguments($a_args)
		{
			if (!is_array($a_args))
				$a_args = array($a_args);
			// suppression des arguments en double
			$url = parse_url($_SERVER['REQUEST_URI']);
			$path = $url['path'];

			$newArgs = "";
			$resArgs = array();

			// arguments courants
			if (isset($url['query']))
			{
				parse_str($url['query'],$curArgs);
				foreach ($curArgs as $arg=>$val)
				{
					if (!in_array($arg,$a_args))
						array_push($resArgs,$arg."=".$val);
				}
				$newArgs = implode("&",$resArgs);
			}
			return ($newArgs);
		}



/**
 * met à jour les valeurs de la liste en session pour le tri et la pagination
 * @return void
*/
		private function _updateSession()
		{
			//************************* TRI ************************
			// champs
			// en cas de changement d'ordre, on réinitialise la page
			if (isset($_GET['ord']) && (!isset($_GET['liste']) || $_GET['liste']==$this->listeName))
			{
				$this->_updateConfListe('ord',$_GET['ord']);
				$this->_updateConfListe('page',1);
			}
			// ordre de tri
			if (isset($_GET['sens']) && (!isset($_GET['liste']) || $_GET['liste']==$this->listeName))
			{
				$this->setSensListe($_GET['ord'],$_GET['sens']);
				$this->_updateConfListe('page',1);
			}



			//********************  PAGINATION  *********************
			// pas
			if (isset($_GET['pas']) && (!isset($_GET['liste']) || $_GET['liste']==$this->listeName))
			{
				$this->_updateConfListe('pas',max(10,intval($_GET['pas'])));
				$this->_updateConfListe('page',1);
			}
			else if (isset($this->pas) && !isset($_SESSION['classe_liste'][$this->listeName]['pas']))
				$this->_updateConfListe('pas',$this->pas);
			// no de page
			if (isset($_GET['page']) && (!isset($_GET['liste']) || $_GET['liste']==$this->listeName))
				$this->_updateConfListe('page',max(1,intval($_GET['page'])));
			else if (!isset($_SESSION['classe_liste'][$this->listeName]['page']))
				$this->_updateConfListe('page',1);

			// min
			$this->_setMin();



			//********************  FILTRES  *********************
			// pas
			if (isset($_GET['colfiltre'],$_GET['valfiltre']))
			{
				$curFiltres = isset($_SESSION['classe_liste']['filtres']) ? $_SESSION['classe_liste']['filtres'] : array();
				$curFiltres[$_GET['colfiltre']]=$_GET['valfiltre'];
				if ($_GET['valfiltre']=="NULL")
					unset($curFiltres[$_GET['colfiltre']]);
				if (count($curFiltres)==0)
					$this->_unsetConfListe('filtres');
				else
					$this->_updateConfListe('filtres',$curFiltres);
			}



		}


/**
 * initialisation du min sur liste en cours
 * @return void
*/
		private function _setMin()
		{
			// limite inf pour requete SQL (commence à 0)
			$val = ($this->getConfListe('page') * $this->getConfListe('pas')) - ($this->getConfListe('pas')-1)-1;
			$this->_updateConfListe('min',intval($val));
		}


/**
 * mise à jour d'une valeur d'une liste en session
 * @param string $a_key clé du tableau à mettre à jour
 * @param string $a_val nouvelle valeur
 * @return void
*/
		private function _updateConfListe($a_key,$a_val)
		{
			if (strlen($this->listeName)>0)
				$_SESSION['classe_liste'][$this->listeName][$a_key] = $a_val;
			else
				$_SESSION['classe_liste'][$a_key] = $a_val;
			// mise à jour de la liste en cours
			$this->changeListe($this->listeName);
		}


/**
 * suppression d'une valeur d'une liste en session
 * @param string $a_idColonne valeur à supprimer
 * @return void
*/
		private function _unsetConfListe($a_idColonne)
		{
			if (strlen($this->listeName)>0)
				unset($_SESSION['classe_liste'][$this->listeName][$a_idColonne]);
			unset($_SESSION['classe_liste'][$a_idColonne]);
		}




/**
 * remplace un motif [nomchamp] par sa valeur
 * @param string $a_format
 * @param array $a_data ligne de résultat
 * @param array $a_dataSup valeurs supplémentaires
 * @return valeur
 */
	private function argVal($a_motif,$a_data,$a_dataSup=null)
	{
// 		var_dump($this->paramsLigneSousTotal['prevChamps']);
		if (preg_match("/^\[.*\]$/",$a_motif)==1)
		{
			$arg = trim($a_motif,"[]");
			if (isset($a_dataSup[$arg]) && strlen($a_dataSup[$arg])>0 && in_array($arg,$this->paramsLigneSousTotal['prevChamps']))
				$data = $a_dataSup[$arg];
			else if (isset($a_data[$arg]) && strlen($a_data[$arg])>0)
				$data = $a_data[$arg];
			else
				$data = "";
		}
		else
			$data = $a_motif;
		return ($data);
	}




/**
 * formate le contenu à partir d'une fonction
 * @param string $a_fonction nom de la fonction locale
 * @param array $a_data liste des données pour le formatage
 * @param string $a_valeur valeur à formater
 * @param array $a_dataSup données supplémentaires
 * @return string chaine formatée
*/
		private function _setContenuFonction($a_fonction,$a_data,$a_valeur=null,$a_dataSup=null)
		{
			$contenu = "";
			// récupération nom de fonction + arguments
			$pattern = "/^([\w\d]*)\((.*)\);?$/";
			preg_match($pattern,$a_fonction,$matches);
			if (count($matches)>0)
			{
				$fonction = $matches[1];
				$args = explode(",",$matches[2]);
			}
			// tableau de valeurs
			$argsVal = array();
			foreach($args as $arg)
				array_push($argsVal,$this->argVal($arg,$a_data,$a_dataSup));
			if (isset($fonction) && count($args)>0)
				$contenu = call_user_func_array($fonction,$argsVal);
			// on remplace [DEFAULT_VALUE] par $a_valeur si >0
			if (intval($a_valeur)!=0)
				$contenu = preg_replace("/\[DEFAULT_VALUE\]/",$a_valeur,$contenu);
			else
				$contenu = preg_replace("/\[DEFAULT_VALUE\]/","",$contenu);
			return($contenu);
		}




/**
 * formate le contenu à partir d'un format de type sprintf (%s, %d...)
 * @param string $a_format format pour fonction php sprintf
 * @param string $a_data donnée à formater
 * @return string chaine formatée (résultat de la fonction sprintf($a_format,$a_data)
*/
		private function _setContenuFormat($a_format,$a_data)
		{
			$contenu = "";
			$contenu = sprintf($a_format,$a_data);
			return($contenu);
		}





/**
 * calcule d'une valeur d'après une formule
 * @param string $a_formule
 * @param string $a_colonne
 * @param array $a_datasLigne (valeurs de toutes les colonnes de la ligne)
 */
	private function _calculeValeur($a_formule,$a_colonne,$a_datasLigne)
	{
		$parts = null;
		$pattern = "/(SUM|RATIO|DIFF)\((.*)\)/";
		preg_match($pattern,$a_formule,$parts);

		$operateur = $parts[1];
		// recursivité
		if (preg_match("/(SUM|RATIO|DIFF)/",$parts[2]))
		{
			$res = $this->_calculeValeur($parts[2],$a_colonne,$a_datasLigne);
			$parts[2] = preg_replace($pattern,$res,$parts[2]);
		}

		$operandes = explode(";",$parts[2]);

		$value = 0;

		switch ($operateur)
		{
			case 'SUM':
				$this->colonnes[$a_colonne]['type']="TYPE_NOMBRE";
				foreach ($operandes as $val)
				{
					$val = isset($a_datasLigne[$val]) ? floatval($a_datasLigne[$val]) : (is_numeric($val) ? $val : $this->colonnes[$val]['contenu']['valeur']);
					$value += $val;
				}
				break;

			case 'DIFF':
				$this->colonnes[$a_colonne]['type']="TYPE_NOMBRE";
				$val1 = isset($a_datasLigne[$operandes[0]]) ? floatval($a_datasLigne[$operandes[0]]) : (is_numeric($operandes[0]) ? $operandes[0] : $this->colonnes[$operandes[0]]['contenu']['valeur']);
				$val1 = trim($val1,"-");
				$val2 = isset($a_datasLigne[$operandes[1]]) ? floatval($a_datasLigne[$operandes[1]]) : (is_numeric($operandes[1]) ? $operandes[1] : $this->colonnes[$operandes[1]]['contenu']['valeur']);
				$val2 = trim($val2,"-");
				$value = $val1-$val2;
				break;

			case 'RATIO':
				$this->colonnes[$a_colonne]['type']="TYPE_RATIO";
				$val1 = isset($a_datasLigne[$operandes[0]]) ? floatval($a_datasLigne[$operandes[0]]) : (is_numeric($operandes[0]) ? $operandes[0] : $this->colonnes[$operandes[0]]['contenu']['valeur']);
				$val2 = isset($a_datasLigne[$operandes[1]]) ? floatval($a_datasLigne[$operandes[1]]) : (is_numeric($operandes[1]) ? $operandes[1] : (isset($this->colonnes[$operandes[1]]['contenu']['valeur']) ? $this->colonnes[$operandes[1]]['contenu']['valeur'] : null));
				$value = $this->_ratio($val1,$val2);
				break;

			default:
				break;
		}
		return($value);
	}




/**
 * calcul un ratio
 * @param int $a_val1
 * @param int $a_val2
 * @return float
 */
	private function _ratio($a_val1,$a_val2)
	{
		$value = 0;
		if ($a_val2>0)
		{
			$ratio = $a_val1/$a_val2*100;
			if ($ratio>0)
				$value = $ratio;
		}
		return ($value);
	}



/**
 * génère la ligne de total
 * @return array
 */
	private function _ligneTotal()
	{
		$ligne = array('type'=>'total','datas'=>null);

		foreach ($this->colonnes as $champ=>$colonne)
		{
			// on crée un liste de valeurs pour les recalculs (ratio...)
			$datasTotal[$champ] = isset($colonne['total']['valeur']) ? $colonne['total']['valeur'] : 0;
			$datasLigne[$champ] = $colonne['contenu']['valeur'];
			if ($champ==$this->paramsLigneTotal['colonneLibelle'])
			{
				$ligne['datas'][$champ] = $this->paramsLigneTotal['format'];
				$valeur = $this->argVal($this->paramsLigneTotal['format'],$datasLigne);
 				$ligne['datasFormatees'][$champ] = strlen($valeur)>0 ? $valeur : $this->paramsLigneTotal['format'];
			}
			else if (isset($colonne['total']))
			{
				if (isset($colonne['total']['type']) && $colonne['total']['type']=="TOT_SOUSTOTAL")
					$colonne['total']['valeur'] = $this->colonnes[$champ]['sousTotal']['cumul'];

				$ligne['datas'][$champ] = $colonne['total']['valeur'];

				if (!is_null($colonne['total']['fonction']))
					$ligne['datasFormatees'][$champ] = $this->_setContenuFonction($colonne['total']['fonction'],$datasTotal,$colonne['total']['valeur']);
				else
					$ligne['datasFormatees'][$champ] = $this->_setContenuFormat($colonne['total']['format'],$colonne['total']['valeur']);
			}
			else
			{
				$ligne['datas'][$champ]="";
				$ligne['datasFormatees'][$champ] = "";
			}
		}

		// calculs sur la ligne
		foreach ($this->colonnes as $champ=>$colonne)
		{
			if (in_array($colonne['type'],array("TYPE_RATIO","TYPE_NOMBRE")) && isset($colonne['total']) && is_null($colonne['total']['fonction']))
			{
				$this->_formateValeur($champ,$datasTotal,'total');
				$ligne['datas'][$champ] = $this->colonnes[$champ]['total']['valeur'];
				$ligne['datasFormatees'][$champ] = $this->colonnes[$champ]['total']['valeurFormatee'];
			}
		}

		return ($ligne);
	}






/**
 * génère la ligne de sous total
 * @param array $a_curLigne donnée de la ligne courante
 * @return array
 */
	private function _ligneSousTotal($a_curLigne=null)
	{
		$ligne = array(
			'type'=>'sousTotal',
			'datas'=>null,
		);
		foreach ($this->colonnes as $champ=>$colonne)
		{
			// on crée un liste de valeurs pour les recalculs (ratio...)
			$datasSousTotal[$champ] = isset($colonne['sousTotal']['valeur']) ? $colonne['sousTotal']['valeur'] : 0;
			$datasLigne[$champ] = $colonne['contenu']['valeur'];

			// libelle de sous total
			if ($champ==$this->paramsLigneSousTotal['colonneLibelle'])
			{
				$ligne['datas'][$champ] = "";
				$valeur = $this->argVal($this->paramsLigneSousTotal['format'],$datasLigne);
 				$ligne['datasFormatees'][$champ] = strlen($valeur)>0 ? $valeur : $this->paramsLigneSousTotal['format'];
			}
			// calcul du sous total
			else if (isset($colonne['sousTotal']))
			{
				$ligne['datas'][$champ] = $colonne['sousTotal']['valeur'];
				$this->colonnes[$champ]['sousTotal']['cumul'] += $colonne['sousTotal']['valeur'];
				if (!is_null($colonne['sousTotal']['fonction']))
				{
					$datasSup = isset($colonne['sousTotal']['cumul']) && intval($colonne['sousTotal']['cumul'])==0 ? $a_curLigne : null;
					$datasSup = $a_curLigne;
					$ligne['datasFormatees'][$champ] = $this->_setContenuFonction($colonne['sousTotal']['fonction'],$datasSousTotal,$colonne['sousTotal']['valeur'],$datasSup);
				}
				else if (strlen($colonne['sousTotal']['format'])>0)
					$ligne['datasFormatees'][$champ] = $this->_setContenuFormat($colonne['sousTotal']['format'],$colonne['sousTotal']['valeur']);
			}
			else
			{
				$ligne['datas'][$champ]="";
				$ligne['datasFormatees'][$champ] = "";
			}

		}


		foreach ($this->colonnes as $champ=>$colonne)
		{

			if (in_array($colonne['type'],array("TYPE_RATIO","TYPE_NOMBRE")) && is_null($colonne['sousTotal']['fonction']))
			{
				// mise à jour du ratio par rapport au sousTotal
				$pattern = "/(RATIO)\((.*)\)/";
				$patternRef = "/sousTotal#/";
				if (preg_match($patternRef,$colonne['contenu']['formule']) && preg_match($pattern,$colonne['contenu']['formule'],$matches))
				{
					$matches[2] = preg_replace($patternRef,"",$matches[2]);
					foreach ($this->liste as $lignesPrec)
					{
						$operandes = explode(";",$matches[2]);
						$val1 = $lignesPrec['datas'][$operandes[0]];
						$val2 = $ligne['datas'][$operandes[1]];
						$ratio = ($val1/$val2)*100;
						$lignesPrec['datas'][$champ] = $ratio;
						$lignesPrec['datasFormatees'][$champ] = $this->_setContenuFormat($this->colonnes[$champ]['contenu']['format'],$ratio);
					}
				}

				$this->_formateValeur($champ,$datasSousTotal,'sousTotal');

				$ligne['datas'][$champ] = $this->colonnes[$champ]['sousTotal']['valeur'];
				$ligne['datasFormatees'][$champ] = $this->colonnes[$champ]['sousTotal']['valeurFormatee'];
				$ligne['datasRef'] = null;
			}
		}

		// on remet à 0 les sous totaux de chaque champ
		foreach ($this->colonnes as $champ=>$colonne)
		{
			if (isset($this->colonnes[$champ]['sousTotal']))
			{
				$this->colonnes[$champ]['sousTotal']['valeur']=null;
				$this->colonnes[$champ]['sousTotal']['valeurFormatee']="";
			}
		}

		return ($ligne);
	}


/**
 * génère la ligne de ratioTotal
 * @param $a_ligneTotal ligne de total pour le calcul des ratios
 * @return array
 */
	private function _ligneRatioTotal($a_ligneTotal)
	{
// 		print "<pre>";print_r($this->colonnes['ratio']);print "</pre>";exit();

		$ligne = array('type'=>'ratioTotal','datas'=>null);
		$valRef = $a_ligneTotal['datas'][$this->paramsLigneRatioTotal['colRef']];
		foreach ($this->colonnes as $champ=>$colonne)
		{
			if ($champ==$this->paramsLigneRatioTotal['colonneLibelle'])
			{
				$ligne['datas'][$champ] = $this->paramsLigneRatioTotal['format'];
				$contenu = $this->argVal($this->paramsLigneRatioTotal['format'],$ligne['datas'][$champ]);
 				$ligne['datasFormatees'][$champ] = strlen($contenu)>0 ? $contenu : $this->paramsLigneRatioTotal['format'];
			}
			else if (isset($colonne['total'],$colonne['ratioTotal']))
			{
				$this->colonnes[$champ]['ratioTotal']['valeur'] = $this->_ratio($a_ligneTotal['datas'][$champ],$valRef);
				$ligneRatioTotal[$champ] = $ligne['datas'][$champ] = $this->colonnes[$champ]['ratioTotal']['valeur'];
 				$this->_formateValeur($champ,$ligneRatioTotal,'ratioTotal');
 				$ligne['datasFormatees'][$champ] = $this->colonnes[$champ]['ratioTotal']['valeurFormatee'];
			}
			else
			{
				$ligne['datas'][$champ] = null;
				$ligne['datasFormatees'][$champ] = "";
			}
		}
// 		var_dump($ligne);
		return ($ligne);
	}



/**
 * remplit la liste des filtres d'une colonne
 * @param string $a_idColonne
 * @param string $a_val valeur à ajouter dans la liste
 * @param array $a_curLigne ligne courante pour exclure des valeurs si on a des filtres sur plusieurs colonnes
 * @return void
 */
	private function _addColFilter($a_idColonne,$a_val,$a_curLigne)
	{
		if (is_array($this->colonnes[$a_idColonne]['filter']))
		{
			foreach ($a_curLigne as $col=>$data)
			{
				if (isset($_SESSION['classe_liste']['filtres'][$col]) && $_SESSION['classe_liste']['filtres'][$col]!=$a_idColonne && $data!=$_SESSION['classe_liste']['filtres'][$col])
					return;
			}
			if (!in_array($a_val,$this->colonnes[$a_idColonne]['filter']) )
			{
				array_push($this->colonnes[$a_idColonne]['filter'],$a_val);
				$this->displayFilters = true;
			}
			if ($this->displayFilters)
			{
				$argsSup = $this->_removeArguments(array('colfiltre','valfiltre'));
				$this->codeJqueryFilter = "$('.filtre').on('change',function(){\nlocation.href='".CURRENT_PAGE."?".(strlen($argsSup)>0 ? $argsSup."&" : "")."colfiltre='+$(this).data('col')+'&valfiltre='+$(this).children('option:selected').val();});";
			}
		}

	}



/**
 * génération du code jQuery
 * @return string
 */
	private function _genereJquery()
	{
		$code = "";
		$code .= "<script language=\"javascript\">";
		$code .= "$(document).ready(function(){";
		$code .= $this->codeJquery;
		$code .= $this->codeJqueryFilter;
		$code .= "});";
		$code .= "</script>";
		return $code;
	}




/**
 * formatage de la valeur (contenu d'une cellule)
 * @param string $a_idColonne
 * @param array $a_datas ligne de résultat
 * @param string $a_part (contenu, total, sousTotal...)
 * @return void
 */
	private function _formateValeur($a_idColonne,$a_datas,$a_part='contenu')
	{
		$valeurFormatee = "";
		$valeur = "";

		// valeur calculée à partir de plusieurs champs
		if (isset($this->colonnes[$a_idColonne]['contenu']['formule']) && strlen($this->colonnes[$a_idColonne]['contenu']['formule'])>0)
			$a_datas[$a_idColonne] = $this->_calculeValeur($this->colonnes[$a_idColonne]['contenu']['formule'],$a_idColonne,$a_datas);


		$this->colonnes[$a_idColonne][$a_part]['valeur']=null;
		$this->colonnes[$a_idColonne][$a_part]['valeurFormatee']="";

		if (isset($a_datas[$a_idColonne]))
		{
			// valeur récupérée
			$this->colonnes[$a_idColonne][$a_part]['valeur'] = $a_datas[$a_idColonne];
			$this->colonnes[$a_idColonne][$a_part]['valeurFormatee'] = $a_datas[$a_idColonne];
			// formatage du contenu par sprintf
			if ($a_part=='ratioTotal' && isset($this->colonnes[$a_idColonne]['ratioTotal']['format']))
				$valeurFormatee = $this->_setContenuFormat($this->colonnes[$a_idColonne]['ratioTotal']['format'],$a_datas[$a_idColonne]);
			else if (strlen($this->colonnes[$a_idColonne]['contenu']['format'])>0 && strlen($a_datas[$a_idColonne])>0)
				$valeurFormatee = $this->_setContenuFormat($this->colonnes[$a_idColonne]['contenu']['format'],$a_datas[$a_idColonne]);
		}

		// contenu généré par une fonction
		if ($a_part!='ratioTotal')
		{
			if (strlen($this->colonnes[$a_idColonne]['contenu']['fonction'])>0 )
				$valeurFormatee = $this->_setContenuFonction($this->colonnes[$a_idColonne]['contenu']['fonction'],$a_datas);
			if (isset($this->colonnes[$a_idColonne]['contenu']['fonctionValeur']) && !is_null($this->colonnes[$a_idColonne]['contenu']['fonctionValeur']) && strlen($this->colonnes[$a_idColonne]['contenu']['fonctionValeur'])>0)
				$valeur = $this->_setContenuFonction($this->colonnes[$a_idColonne]['contenu']['fonctionValeur'],$a_datas);
		}

		// les valeurs de type NOMBRE ou RATIO à 0 ne sont pas affichées
		switch($this->colonnes[$a_idColonne]['type'])
		{
			case "TYPE_NOMBRE":
			case "TYPE_RATIO":
				if (isset($this->colonnes[$a_idColonne][$a_part]['valeur']) && $this->colonnes[$a_idColonne][$a_part]['valeur']!=0)
					$this->colonnes[$a_idColonne][$a_part]['valeurFormatee']=$valeurFormatee;
				else
					$this->colonnes[$a_idColonne][$a_part]['valeurFormatee']="";
				break;
			default:
				if (strlen($valeurFormatee)>0)
					$this->colonnes[$a_idColonne][$a_part]['valeurFormatee']=$valeurFormatee;
				if (strlen($valeur)>0)
					$this->colonnes[$a_idColonne][$a_part]['valeur']=$valeur;
				break;
		}
	}


/**
 * met à jour le sous total de la colonne
 * @param string $a_idColonne
 * @return void
 */
	private function _sousTotal($a_idColonne,$a_data,$a_ref)
	{
		if (isset($this->colonnes[$a_idColonne]['sousTotal']))
		{
			// pour les TYPES_RATIO, on ne fait rien. le ratio sera recalculé au moment de l'affichage du sous-total
			if (!in_array($this->colonnes[$a_idColonne]['type'],array("TYPE_RATIO")))
			{
				if ($this->colonnes[$a_idColonne]['sousTotal']['type']=="NB_LIGNES")
					$this->colonnes[$a_idColonne]['sousTotal']['valeur'] += 1;
				else if ($this->colonnes[$a_idColonne]['sousTotal']['type']=="MAX_VALEUR")
					$this->colonnes[$a_idColonne]['sousTotal']['valeur'] = max(intval($this->colonnes[$a_idColonne]['contenu']['valeur']),$this->colonnes[$a_idColonne]['sousTotal']['valeur']);
				else if ($this->colonnes[$a_idColonne]['sousTotal']['type']=="MIN_VALEUR")
				{
					if ($this->colonnes[$a_idColonne]['sousTotal']['valeur']>0)
						$this->colonnes[$a_idColonne]['sousTotal']['valeur'] = min(intval($this->colonnes[$a_idColonne]['contenu']['valeur']),$this->colonnes[$a_idColonne]['sousTotal']['valeur']);
					else
						$this->colonnes[$a_idColonne]['sousTotal']['valeur'] = intval($this->colonnes[$a_idColonne]['contenu']['valeur']);
				}
				else if ($this->colonnes[$a_idColonne]['sousTotal']['type']=="TOT_VALEURS" && intval($this->colonnes[$a_idColonne]['contenu']['valeur'])>0)
					$this->colonnes[$a_idColonne]['sousTotal']['valeur'] += intval($this->colonnes[$a_idColonne]['contenu']['valeur']);
				// formatage
				$this->colonnes[$a_idColonne]['sousTotal']['valeurFormatee'] = $this->colonnes[$a_idColonne]['sousTotal']['valeur'];
			}
		}
	}




/**
 * met à jour le total de la colonne
 * @param string $a_idColonne
 * @return void
 */
	private function _total($a_idColonne,$a_data)
	{
		if (isset($this->colonnes[$a_idColonne]['total']))
		{
			// pour les TYPES_RATIO, on ne fait rien. le ratio sera recalculé au moment de l'affichage du sous-total
			if (!in_array($this->colonnes[$a_idColonne]['type'],array("TYPE_RATIO")))
			{
				if ($this->colonnes[$a_idColonne]['total']['type']=="NB_LIGNES")
					$this->colonnes[$a_idColonne]['total']['valeur'] += 1;
				else if ($this->colonnes[$a_idColonne]['total']['type']=="MAX_VALEUR")
					$this->colonnes[$a_idColonne]['total']['valeur'] = max(intval($this->colonnes[$a_idColonne]['contenu']['valeur']),$this->colonnes[$a_idColonne]['total']['valeur']);
				else if ($this->colonnes[$a_idColonne]['total']['type']=="MIN_VALEUR")
				{
					if ($this->colonnes[$a_idColonne]['total']['valeur']>0)
						$this->colonnes[$a_idColonne]['total']['valeur'] = min(intval($this->colonnes[$a_idColonne]['contenu']['valeur']),$this->colonnes[$a_idColonne]['total']['valeur']);
					else
						$this->colonnes[$a_idColonne]['total']['valeur'] = intval($this->colonnes[$a_idColonne]['contenu']['valeur']);
				}
				else if ($this->colonnes[$a_idColonne]['total']['type']=="TOT_VALEURS" && intval($this->colonnes[$a_idColonne]['contenu']['valeur'])>0)
					$this->colonnes[$a_idColonne]['total']['valeur'] += intval($this->colonnes[$a_idColonne]['contenu']['valeur']);
				else if ($this->colonnes[$a_idColonne]['total']['type']=="TOT_SOUSTOTAL" && intval($this->colonnes[$a_idColonne]['sousTotal']['valeur'])>0)
					$this->colonnes[$a_idColonne]['total']['valeur'] = intval($this->colonnes[$a_idColonne]['sousTotal']['cumul']);
				// formatage
				$this->colonnes[$a_idColonne]['total']['valeurFormatee'] = $this->colonnes[$a_idColonne]['total']['valeur'];

			}
		}
	}



/**
 * paramétrage d'une colonne
 * @param string $a_nom nom de la colonne
 * @param string $a_param nom du parametre
 * @param string $a_value valeur
 * @return void
 */
		private function _setColonne($a_nom,$a_param,$a_value)
		{
			if (!isset($this->colonnes[$a_nom]))
				$this->colonnes[$a_nom]=array(
					"libelle"=>"",
					"contenu"=>array(
						"format"=>"%s",
						"formule"=>"",
						"fonction"=>NULL,
						"fonctionValeur"=>NULL,
						"classes"=>"td_".$a_nom,
						"styles"=>"",
						"valeur"=>null,
						"valeurFormatee"=>""
					),
					"tri"=>true,
					"type"=>"TYPE_STRING",
					"headerClasse"=>"",
					"ligneRef"=>null,
					"colRef"=>null,
					"filter"=>null,
				);
			switch($a_param)
			{
				case "contenu":
					break;
				case "format":
				case "formule":
				case "fonction":
				case "fonctionValeur":
				case "classes":
				case "styles":
					$this->colonnes[$a_nom]["contenu"][$a_param]=$a_value;
					break;
				default:
					$this->colonnes[$a_nom][$a_param]=$a_value;
					break;
			}

		}




/**
 * Préparation de la liste avant affichage
 * @return void
 */
	private function _prepareListe()
	{
		$ligne=0;
		$exValSousTotal=null;
		$exValNumLigne=null;
		$dataPrec = null;


		if (is_null($this->datas))
			return (false);

		// génération du tableau de données
		foreach ($this->datas as $ref=>$data)
		{
			// par défaut on affiche toutes les lignes
			$displayLine = true;

			// prise en compte du filtre
			// s'il y en a un et que la valeur correspond, alors on affiche la ligne
			if ($this->displayFilters && isset($_SESSION['classe_liste']['filtres']))
			{
				$displayLine = false;
				foreach ($_SESSION['classe_liste']['filtres'] as $colFiltre=>$valFiltre)
					$displayLine = ($data[$colFiltre]==$valFiltre);
			}


			// si changement de valeur déclanchant l'affichage, on affiche une ligne de sous total
			if ($displayLine)
			{
				if ($this->displaySousTot && $ligne>0 && isset($data[$this->paramsLigneSousTotal['colMod']]) && $data[$this->paramsLigneSousTotal['colMod']]!=$exValSousTotal)
				{
					$this->liste[$ligne]=$this->_ligneSousTotal($dataPrec);
					$ligne++;
				}
				else if (isset($this->paramsLigneSousTotal['curTotalRefs']))
					array_push($this->paramsLigneSousTotal['curTotalRefs'],$ref);


				// nouvelle ligne
				$this->liste[$ligne]['type']='source';
				$this->liste[$ligne]['datasRef']=$ref;
				$this->liste[$ligne]['trClass']="";

			}



			// colonnes
			foreach($this->colonnes as $champ=>$colonne)
			{
 				if (!in_array($colonne['type'],array("TYPE_VIDE","TYPE_NUMLIGNE")))
				{
					// formatage de la donnée
					$this->_formateValeur($champ,$data);

					// calcul du sousTotal
					if ($displayLine && $this->displaySousTot)
					{
						// mise à jour du sous total
						$this->_sousTotal($champ,$data,$ref);
						if (isset($data[$this->paramsLigneSousTotal['colMod']]))
							$exValSousTotal = $data[$this->paramsLigneSousTotal['colMod']];
					}
					// calcul du total
					if ($displayLine && $this->displayTotal)
						$this->_total($champ,$data);
				}

				switch ($colonne['type'])
				{
					case "TYPE_NUMLIGNE":
						if ($displayLine)
						{
							// comptage du nombre de lignes au changement de valeur d'une colonne
							if ($this->paramsNumLignes[$champ]['action']=="ON_CHANGE")
							{
								if ($data[$this->paramsNumLignes[$champ]['colRef']]!=$exValNumLigne)
								{
									$this->liste[$ligne]['trClass'] = $this->paramsNumLignes[$champ]['trClass'];
									$this->colonnes[$champ]['contenu']['valeur']++;
								}
							}
							// comptage de toutes les lignes
							else if ($this->paramsNumLignes[$champ]['action']=="NUM_ALL")
							{
								$this->colonnes[$champ]['contenu']['valeur']++;
								$this->liste[$ligne]['trClass'] = $this->paramsNumLignes[$champ]['trClass'];
							}
							$this->liste[$ligne]['datas'][$champ] = $this->colonnes[$champ]['contenu']['valeur'];
							$this->liste[$ligne]['datasFormatees'][$champ]=$this->liste[$ligne]['datas'][$champ];
							if (!is_null($this->paramsNumLignes[$champ]['colRef']))
								$exValNumLigne = $data[$this->paramsNumLignes[$champ]['colRef']];
						}
						break;

					case "TYPE_NOMBRE":
						if ($colonne['contenu']['format']=="%d")
							$val = intval($this->colonnes[$champ]['contenu']['valeur']);
						else if ($colonne['contenu']['format']=="%.02f")
							$val = floatval($this->colonnes[$champ]['contenu']['valeur']);
						if ($displayLine)
							$this->liste[$ligne]['datas'][$champ] = $val;
						$this->_addColFilter($champ,$val,$data);
						break;

					default:
						$val = $this->colonnes[$champ]['contenu']['valeur'];
						if ($displayLine)
							$this->liste[$ligne]['datas'][$champ] = $val;
						$this->_addColFilter($champ,$val,$data);
						break;
				}
				if ($displayLine && $colonne['type']!="TYPE_NUMLIGNE")
					$this->liste[$ligne]['datasFormatees'][$champ] = $this->colonnes[$champ]['contenu']['valeurFormatee'];
			}

			if ($displayLine)
			{
				$ligne++;
				$dataPrec = $data;
			}
		}


		// dernière ligne de sous total
		if ($this->displaySousTot)
			$this->liste[$ligne++] = $this->_ligneSousTotal($dataPrec);


		// ajout de la ligne de total
		if ($this->displayTotal)
			$ligneTotal = $this->liste[$ligne++] = $this->_ligneTotal();

		// mise à jour des valeurs après calcule des totaux
		foreach ($this->colonnes as $champ=>$colonne)
		{
			if (isset($colonne['colRef'],$colonne['ligneRef']))
			{
				for ($i=0;$i<count($this->liste);$i++)
				{
					$this->liste[$i]['datas'][$champ] = $this->_ratio($this->liste[$i]['datas'][$colonne['colRef']],$ligneTotal['datas'][$colonne['colRef']]);
					$this->_formateValeur($champ, $this->liste[$i]['datas']);
 					$this->liste[$i]['datasFormatees'][$champ] = $this->colonnes[$champ]['contenu']["valeurFormatee"];
				}
			}
		}

		// ajout de la ligne de ration sur le total
		if ($this->displayRatioTot)
			$this->liste[$ligne++] = $this->_ligneRatioTotal($ligneTotal);



//  		print "<pre>";print_r($this->liste);print "</pre>";
// 		exit();
	}







/*_______________________________________________________________________________________________________________
																				METHODES PUBLIQUES
*/


/**
 * constructeur
 */
		public function __construct($a_name='',$a_ord='',$a_sens='')
		{
			// nom de la liste et valeurs de tri par défaut
			if (strlen($a_name)>0)
				$this->initListe($a_name,$a_ord,$a_sens);
			// mise à jour de la session
			else
				$this->_updateSession();
			$this->labelsPrintPageCssFile = __DIR__."/css/labelsPrintPage_3x6.css";
		}


/**
 * destructeur
 */
		public function __destruct()
		{
			ini_restore("memory_limit");
		}



/**
 * récupére la valeur de conf de la liste
 * @param string $a_idColonne
 * @param string $a_name
 * @return string
 */
		public function getConfListe($a_idColonne,$a_name='')
		{
			$val = NULL;
			if (strlen($this->listeName)>0)
			{
				if (isset($_SESSION['classe_liste'][(strlen($a_name)>0 ? $a_name : $this->listeName)][$a_idColonne]))
					$val = $_SESSION['classe_liste'][(strlen($a_name)>0 ? $a_name : $this->listeName)][$a_idColonne];
			}
			else
			{
				if (isset($_SESSION['classe_liste'][$a_idColonne]))
					$val = $_SESSION['classe_liste'][$a_idColonne];
			}
			return($val);
		}

/**
 * récupère le sens courant d'une colonne
 * @param string $a_idColonne
 * @param string $a_name
 * @return string
 */
		public function getSensListe($a_idColonne,$a_name='')
		{
			$val = NULL;
			if (strlen($this->listeName)>0)
			{
				if (isset($_SESSION['classe_liste'][(strlen($a_name)>0 ? $a_name : $this->listeName)]['sens'][$a_idColonne]))
					$val = $_SESSION['classe_liste'][(strlen($a_name)>0 ? $a_name : $this->listeName)]['sens'][$a_idColonne];
			}
			else
			{
				if (isset($_SESSION['classe_liste']['sens'][$a_idColonne]))
					$val = $_SESSION['classe_liste']['sens'][$a_idColonne];
			}
			return($val);
		}


		/**
		 * fichier css de formatage des pages pour l'impression au format étiquettes
		 * @param $a_url
		 */
		public function setUrlLabelsPrintPageCss($a_url)
		{
			$this->labelsPrintPageCssFile = $a_url;
		}



//___________________________________________________________________________________________________________
// 																FONCTIONS DE PARAMETRAGE DE LA LISTE




/**
 * remplace les infos de la liste en cours par la nouvelle liste
 * @param string $a_name nom de la liste
 * @return void
 */
		public function changeListe($a_name='')
		{
			$this->listeName = strlen($a_name)>0 ? $a_name : $this->listeName;
			$_SESSION['classe_liste']['currentListe'] = $this->listeName;
			foreach( array_keys($_SESSION['classe_liste'][$this->listeName]) as $cle)
			{
				$_SESSION['classe_liste'][$cle] = $_SESSION['classe_liste'][$this->listeName][$cle];
			}
		}




/**
 * définit le sens de tri pour un champ de la liste
 * @param string $a_idColonne
 * @param string $a_sens (ASC ou DESC)
 * @param string $a_name nom de la liste concernée. si pas indiqué, liste en cours
 * @return void
 */
		public function setSensListe($a_idColonne,$a_sens,$a_name='')
		{
			if (strlen($a_name)>0)
				$_SESSION['classe_liste'][$a_name]['sens'][$a_idColonne] = $a_sens;
			else if (strlen($this->listeName)>0)
				$_SESSION['classe_liste'][$this->listeName]['sens'][$a_idColonne] = $a_sens;
			$_SESSION['classe_liste']['sens'][$a_idColonne] = $a_sens;
		}



/**
 * ajout d'un attribut class à la liste
 * @param string $a_class
 * @return void
 */
		public function addClass($a_class)
		{
			if (is_array($a_class))
				array_merge($this->listeClassCSS,$a_class);
			else
				array_push($this->listeClassCSS,$a_class);
		}



/**
 * initialisation de la liste
 * @param string $a_name
 * @param string $a_ord champ à trier par défaut
 * @param string $a_sens (ASC ou DESC)
 * @return void
 */
		public function initListe($a_name,$a_ord='',$a_sens='ASC')
		{
			$this->listeName = $a_name;
			$this->arguments = "";
			$this->url = "";
			// on vide la table
			$this->datas = null;
			// on réinitialise tous les paramètres
			$this->codeListe = "";
			$this->codeNavig = "";
			$this->ligneSup = array();
			$this->ligneSupFin = "";
			$this->addClass($this->listeName);
			$this->colonnes = array();
			$this->liste = array();
			$this->displaySousTot = false;
			$this->displayTot = false;

			if (strlen($a_ord)>0 && !isset($_SESSION['classe_liste'][$this->listeName]))
			{
				$this->setSensListe($a_ord,$a_sens,$this->listeName);
				$this->_updateConfListe('ord',$a_ord);
			}
			// mise à jour de la session
			$this->_updateSession();
			$this->changeListe();
			$this->resetColonnes();
		}





/**
 * initialisation de l'url de la page pour liens de pagination
 * @param string $a_url
 * @return void
 */
		public function setUrlPage($a_url)
		{
			$this->url = $a_url;
			$this->pageOk = preg_match("/\.[\w]{2,4}\??$/",$this->url);					// l'url courante est une page et non un repertoire (ex : rewriteurl)
		}



/**
 * initialisation du numéro de page
 * @param int $a_page
 * @return void
 */
		public function setPage($a_page)
		{
			$this->page = abs(intval($a_page));
			$this->_updateConfListe('page',$this->page);
		}


/**
 * initialisation du pas (nombre de lignes maximum à afficher)
 * @param int $a_pas
 * @return void
 */
		public function setPas($a_pas)
		{
			$this->pas = abs(intval($a_pas));
			$this->_updateConfListe('pas',$this->pas);
			// min
			$this->_setMin();
		}


/**
 * met le pas à 0 : affichage de toute la liste
 * @return void
 */
		public function displayAll()
		{
			$this->_unsetConfListe('pas');
			// min
			$this->_updateConfListe('min',0);
			$this->displayAll = true;
		}


/**
 * active l'ecriture des fonctions jQuery
 * @return void
 */
		public function jQuery()
		{
			$this->jQuery = true;
		}



/**
 * message à afficher si pas de données
 * @param string $a_texte
 * @return void
 */
		public function setMessageNoData($a_texte)
		{
			$this->messNoData = $a_texte;
		}




/**
 * pas de ligne d'entête
 * @return void
 */
		public function noHeader()
		{
			$this->header=false;
		}



/**
 * pas de tri sur la colonne
 * @param string $a_idColonne
 * @return void
 */
		public function noTri($a_idColonne)
		{
			$this->colonnes[$a_idColonne]['tri']=false;
		}



/**
 * affichage des icones de tri
 * @param boolean $a_ico
 * @return void
 */
		public function setIconesTri($a_ico=true)
		{
			$this->iconesTri = $a_ico;
		}




/**
 * Ajoute un ou plusieurs styles css aux cellules d'une colonne
 * @param  string $a_idColonne
 * @param  string|array $a_styles
 */
	public function addStyleCellule($a_idColonne,$a_styles)
	{
		$finalStylesStr = "";
		if (!is_array($a_styles))
			$a_styles = array($a_styles);
		foreach ($a_styles as $style) {
			$finalStylesStr .= $style." ";
		}
		$this->_setColonne($a_idColonne,"styles",trim($finalStylesStr));
	}

/**
 * Retourne les styles additionnels d'une cellule/colonne
 * @param  string $a_idColonne
 * @return string styles
 */
	public function getStylesCellule($a_idColonne)
	{
		$styles = $this->colonnes[$a_idColonne]["contenu"]["styles"];
		return $styles;
	}


/**
 * Ajoute une ou plusieures classes css aux cellules d'une colonne
 * @param  string $a_idColonne
 * @param  string|array $a_classes
 * @return object $this
 */
	public function addClassCellule($a_idColonne,$a_classes)
	{
		$actualClassStr = $this->colonnes[$a_idColonne]["contenu"]["classes"];
		if(!is_array($a_classes))
			$a_classes = array($a_classes);
		$classesSup = implode(" ",$a_classes);
		$finalClassesStr = trim($actualClassStr." ".$classesSup);
		$this->_setColonne($a_idColonne,"classes",$finalClassesStr);
		return $this;
	}

/**
 * Retire une ou plusieures classes css aux classes déjà ajoutées d'une colonne
 * @param  string $a_idColonne
 * @param  string|array $a_classes
 * @return object $this
 */
	public function removeClassCellule($a_idColonne,$a_classes)
	{
		$actualClassStr = $this->colonnes[$a_idColonne]["contenu"]["classes"];
		if(!is_array($a_classes))
			$a_classes = array($a_classes);
		foreach ($a_classes as $classeRem)
			$actualClassStr = trim(str_replace($classeRem,"", $actualClassStr));
		$this->_setColonne($a_idColonne,"classes",$actualClassStr);
		return $this;
	}

/**
 * Retourne les classes additionnelles d'une cellule/colonne
 * @param  string $a_idColonne
 * @return string $classes
 */
	public function getClassCellule($a_idColonne)
	{
		$classes = $this->colonnes[$a_idColonne]["contenu"]["classes"];
		return $classes;
	}





/**
 * Ajoute une ou plusieures classes css aux entetes de colonne
 * @param  string $a_idColonne
 * @param  string|array $a_classes
 * @return object $this
 */
	public function addClassHeader($a_idColonne,$a_classes)
	{
		$actualClassStr = $this->colonnes[$a_idColonne]["headerClasse"];
		if(!is_array($a_classes))
			$a_classes = array($a_classes);
		$classesSup = implode(" ",$a_classes);
		$finalClassesStr = trim($actualClassStr." ".$classesSup);
		$this->_setColonne($a_idColonne,"headerClasse",$finalClassesStr);
		return $this;
	}

/**
 * Retire une ou plusieures classes css aux classes déjà ajoutées d'une colonne
 * @param  string $a_idColonne
 * @param  string|array $a_classes
 * @return object $this
 */
	public function removeClassHeader($a_idColonne,$a_classes)
	{
		$actualClassStr = $this->colonnes[$a_idColonne]["headerClasse"];
		if(!is_array($a_classes))
			$a_classes = array($a_classes);
		foreach ($a_classes as $classeRem)
			$actualClassStr = trim(str_replace($classeRem,"", $actualClassStr));
		$this->_setColonne($a_idColonne,"headerClasse",$actualClassStr);
		return $this;
	}

/**
 * Retourne les classes additionnelles d'une Header/colonne
 * @param  string $a_idColonne
 * @return string $classes
 */
	public function getClassHeader($a_idColonne)
	{
		$classes = $this->colonnes[$a_idColonne]["headerClasse"];
		return $classes;
	}



//___________________________________________________________________________________________________________
//																		ALIMENTATION DU TABLEAU

/**
 * réinitialise la liste des colonnes
 * @return void
*/
		public function resetColonnes()
		{
			$this->colonnes = array();
		}





/**
 * ajout de colonne à la liste
 * @param string $a_idColonne
 * @param string $a_libelle
 * @param string $a_format
 * @param string $a_type (TYPE_STRING, TYPE_NOMBRE, TYPE_QUOTA, TYPE_VIDE)
 * @return void
 */
		public function addColonne($a_idColonne,$a_libelle,$a_format="",$a_type="TYPE_STRING")
		{
			$this->_setColonne($a_idColonne,"libelle",$a_libelle);
			$this->_setColonne($a_idColonne,"format",(strlen($a_format)>0 ? $a_format : "%s"));
			$this->_setColonne($a_idColonne,"type",$a_type);
		}



/**
 * ajout d'une colonne vide
 * @return void
 */
		public function addColonneVide()
		{
			$this->_setColonne('colonneVide',"type","TYPE_VIDE");
		}


/**
 * ajout d'une colonne numéro de ligne
 * @return void
 */
		public function addColonneNumLigne($a_idColonne)
		{
			$this->_setColonne($a_idColonne,"type","TYPE_NUMLIGNE");
			$this->_setColonne($a_idColonne,"valeur",0);
			$this->setParamNumLigne($a_idColonne);
		}


/**
 * parametrage du numerotation des lignes
 * @param string $a_idColonne
 * @param string $a_action
 * - NUM_ALL : on compte toutes les lignes (par défaut)
 * - ON_CHANGE : incrementation quand la valeur de $a_colRef change
 * @param string $a_colRef
 * @param string $a_rowClass prefixe de la classe à appliquer à la ligne à chaque incrementation
 * @return void
 */
		public function setParamNumLigne($a_idColonne,$a_action="NUM_ALL",$a_colRef=null,$a_rowClass="newLine")
		{
			$this->paramsNumLignes[$a_idColonne]['action'] = $a_action;
			$this->paramsNumLignes[$a_idColonne]['colRef'] = $a_colRef;
			$this->paramsNumLignes[$a_idColonne]['trClass'] = $a_rowClass;
		}





/**
 * ajout de colonne dont la valeur est calculée à partir d'autres valeurs de la même ligne
 * @param string $a_idColonne
 * @param string $a_formule ex: SUM(col1,col2)
 * @param string $a_format
 * @param string $a_type
 * @return void
 */
		public function addColonneCalculee($a_idColonne,$a_libelle,$a_formule,$a_format="%s",$a_type="TYPE_STRING")
		{
			$this->_setColonne($a_idColonne,"libelle",$a_libelle);
			$this->_setColonne($a_idColonne,"format",$a_format);
			$this->_setColonne($a_idColonne,"type",$a_type);
			$this->_setColonne($a_idColonne,"formule",$a_formule);
		}




/**
 * ajout de colonne dont la valeur est le ratio calculé à partir du total de la même colonne
 * @param string $a_idColonne
 * @param string $a_formule ex: SUM(col1,col2)
 * @param string $a_format
 * @param string $a_type
 * @return void
 */
		public function addColonneRatioFromTotal($a_idColonne,$a_libelle,$a_ligneRef,$a_colRef,$a_format="%s")
		{
			$this->_setColonne($a_idColonne,"libelle",$a_libelle);
			$this->_setColonne($a_idColonne,"format",$a_format);
			$this->_setColonne($a_idColonne,"type","TYPE_RATIO");
			$this->_setColonne($a_idColonne,"ligneRef",$a_ligneRef);
			$this->_setColonne($a_idColonne,"colRef",$a_colRef);
		}




/**
 * ajout de contenu à la colonne, formaté par sprintf
 * @param string $a_idColonne
 * @param string $a_format
 * @return void
 */
		public function addContenu($a_idColonne,$a_format="%s")
		{
			$this->_setColonne($a_idColonne,"format",$a_format);
		}



/**
 * écriture de la valeur formatée à partir d'une fonction utilisateur
 * @param string $a_idColonne
 * @param string $a_fonction
 * @return void
 */
		public function addContenuFonction($a_idColonne,$a_fonction)
		{
			$this->_setColonne($a_idColonne,"fonction",$a_fonction);
		}

/**
 * écriture de la valeur à partir d'une fonction utilisateur
 * @param string $a_idColonne
 * @param string $a_fonction
 * @return void
 */
		public function addValeurFonction($a_idColonne,$a_fonction)
		{
			$this->_setColonne($a_idColonne,"fonctionValeur",$a_fonction);
		}



/**
 * ajout d'éléments à la colonne à partir d'une fonction de l'utilisateur
 * @param string $a_idColonne
 * @param string $a_fonction
 * @return void
 */
		public function addSousTotalFonction($a_idColonne,$a_fonction)
		{
			$this->colonnes[$a_idColonne]['sousTotal']['fonction']=$a_fonction;
		}





/**
 * ajout d'éléments à la colonne à partir d'une fonction de l'utilisateur
 * @param string $a_idColonne
 * @param string $a_fonction
 * @return void
 */
		public function addTotalFonction($a_idColonne,$a_fonction)
		{
			$this->colonnes[$a_idColonne]['total']['fonction']=$a_fonction;
		}




/**
 * ajout un filtre sur la colonne (liste HTML)
 * @param string $a_idColonne
 */
		public function addFilter($a_idColonne)
		{
			$this->_setColonne($a_idColonne,"filter",array());
		}
















/**
 * initialise les données à afficher
 * @param array $a_datas
 * @param string $a_source source de données : SQL, ARRAY (tableau php), JSON
 * @return void
 */
		public function setDatas($a_datas,$a_source="SQL",$attr=true)
		{
			$this->sourceDatas = $a_source;
			switch ($a_source)
			{
				case "SQL":
				case "ARRAY":
					$this->datas = $a_datas;
					break;
				case "JSON":
					$this->datas = json_decode($a_datas,true);
					break;
				case "XML":
					$dataArray = array();
					$i=0;
					foreach ($a_datas as $datas) {
						foreach($datas as $key => $value)
							$dataArray[$i][$key] = strval($value);
						if($attr){
							$tab = json_decode(json_encode($datas->attributes()), true);
							if (count($tab)>0)
							{
								$attrs = $tab['@attributes'];
								if(!empty($attrs) && is_array($attrs) && !empty($dataArray[$i]) && is_array($dataArray[$i]))
									$dataArray[$i] = array_merge($dataArray[$i], $attrs);
							}
						}
						$i++;
					}
					$this->datas = $dataArray;
					break;
			}
			// si plus de données sur la page, on recule d'une page
			if (count($this->datas)==0 && isset($_SESSION['classe_liste']['page']) && $_SESSION['classe_liste']['page']>1)
			{
				$this->addArguments("page=".($_SESSION['classe_liste']['page']-1));
				$url = $this->_addUrlArgs();
				$this->codeJquery .= "location.href = '".$url."';\n";
			}
// 			print "<pre>";print_r($this->datas);print "</pre>";
		}



/**
 * auto configuration du tableau directement depuis les données
 * @return void
 */
		public function autoBuild()
		{
			foreach ($this->datas[0] as $champ=>$valeur)
			{
				$this->addColonne($champ,$champ,"%s");
				$this->noTri($champ);
			}
			$this->_prepareListe();
		}





/**
 * trie les datas
 * @param array $cols (colonne1=>ASC, colonne2=>DESC, ...)
 * @return void
 */
		public function listeSort($cols)
		{
			$colarr = array();
		    foreach ($cols as $col => $order)
		    {
		        $colarr[$col] = array();
		        foreach ($this->datas as $k => $row)
		        	$colarr[$col]['_'.$k] = strtolower($row[$col]);
		    }
		    // construction de la commande
		    $eval = 'array_multisort(';
		    foreach ($cols as $col => $order) {
		        $eval .= '$colarr[\''.$col.'\'],SORT_'.$order.',';
		    }
		    $eval = substr($eval,0,-1).');';
		    eval($eval);

		    $ret = array();
		    foreach ($colarr as $col => $arr)
		    {
		        foreach ($arr as $k => $v)
		        {
		            $k = substr($k,1);
		            if (!isset($ret[$k])) $ret[$k] = $this->datas[$k];
		            $ret[$k][$col] = $this->datas[$k][$col];
		        }
		    }
		    $this->datas = $ret;
		}

/**
 * nombre de résultats total, pour pagination
 * @param int $a_nb
 * @return void
 */
		public function setNbResultats($a_nb)
		{
			$this->nbLignes = $a_nb;
		}



/**
 * ajout d'arguments aux liens de navigation
 * @param string $a_args liste d'arguments de la forme arg=val&arg2=val...
 * @return void
 */
		public function addArguments($a_args)
		{
			$this->arguments .= (strlen($this->arguments)>0 ? "&" : "") . $a_args;
		}




/**
 * parametrage de la ligne de total
 * @param string $a_idColonne colonne dans laquelle on ecrit le libelle (première colonne par défaut)
 * @param string $a_format
 * @return void
 */
		public function setLigneTotal($a_idColonne="",$a_format="Total")
		{
			$this->displayTotal = true;
			$colLibelle = strlen($a_idColonne)>0 ? $a_idColonne : key($this->colonnes);
			$this->paramsLigneTotal = array(
				"colonneLibelle"=>$colLibelle,
				"format"=>$a_format,
			);
		}



/**
 * ajout d'une colonne dans la ligne de total
 * @param string $a_idColonne
 * @param string $a_format
 * @param string $a_type
 * 	TOT_VALEURS = somme des valeurs
 * 	TOT_SOUSTOTAL = somme des valeurs des sous-totaux
 * 	NB_LIGNES = nombre de lignes
 *  CALCUL = valeur calculee
 *  MAX_VALEUR = valeur maximale
 *  MIN_VALEUR = valeur minimale
 * @param string $a_fonction
 * @return void
 */
		public function addTotal($a_idColonne,$a_format="%d",$a_type="TOT_VALEURS",$a_fonction=null)
		{
			$this->colonnes[$a_idColonne]['total']=array(
				"format"=>$a_format,
				"fonction"=>$a_fonction,
				"type"=>$a_type,
				"valeur"=>0,
				"valeurFormatee"=>""
			);
		}







/**
 * parametrage de la ligne de total
 * @param string $a_colMod colonne qui déclenche l'affichage de la ligne de sous total quand sa valeur est modifiée
 * @param string $a_idColonne colonne dans laquelle on ecrit le libelle (première colonne par défaut)
 * @param string $a_format
 * @return void
 */
		public function setLigneSousTotal($a_colMod,$a_idColonne="",$a_format="Sous total")
		{
			$this->displaySousTot = true;
			$colLibelle = strlen($a_idColonne)>0 ? $a_idColonne : key($this->colonnes);
			$this->paramsLigneSousTotal = array(
				"colMod"=>$a_colMod,
				"colonneLibelle"=>$colLibelle,
				"format"=>$a_format,
				"curTotalRefs"=>array(),
				"prevChamps"=>array(),
			);
		}

/**
 * liste de champs pour lesquels on récupère la valeur sur la ligne précédente
 * @param array $a_idColonnes
 * @return void
 */
		public function setPrevDatas($a_idColonnes)
		{
			$this->paramsLigneSousTotal['prevChamps'] = $a_idColonnes;
		}



/**
 * ajout d'une colonne dans la ligne de sous-total
 * @param string $a_idColonne
 * @param string $a_format
 * @param string $a_type
 * 	TOT_VALEURS = somme des valeurs
 * 	NB_LIGNES = nombre de lignes
 *  CALCUL = valeur calculee
 *  MAX_VALEUR = valeur maximale
 *  MIN_VALEUR = valeur minimale
 * @param string $a_fonction
 * @return void
 */
		public function addSousTotal($a_idColonne,$a_format="%d",$a_type="TOT_VALEURS",$a_fonction=null)
		{
			$this->colonnes[$a_idColonne]['sousTotal']=array(
				"format"=>$a_format,
				"fonction"=>$a_fonction,
				"type"=>$a_type,
				"valeur"=>0,
				"valeurFormatee"=>"",
				"cumul"=>0				// utilisé pour le calcul du total avec TOT_SOUSTOTAL
			);
		}



/**
 * parametrage de la ligne de ratioTotal
 * @param string $a_idColonne colonne dans laquelle on ecrit le libelle (première colonne par défaut)
 * @param string $a_colRef colonne contenant la valeur de référence pour le calcul du ratio
 * @param string $a_format
 * @return void
 */
		public function setLigneRatioTotal($a_idColonne,$a_colRef,$a_format="Ratio")
		{
			$this->displayRatioTot = true;
			$colLibelle = strlen($a_idColonne)>0 ? $a_idColonne : key($this->colonnes);
			$this->paramsLigneRatioTotal = array(
				"colonneLibelle"=>$colLibelle,
				"format"=>$a_format,
				"colRef"=>$a_colRef
			);
		}

/**
 * ajout d'une colonne dans la ligne de ratio total
 * @param string $a_idColonne
 * @param string $a_format
 * @param string $a_type
 * 	TOT_VALEURS = somme des valeurs
 * 	NB_LIGNES = nombre de lignes
 *  CALCUL = valeur calculee
 *  @return void
 */
		public function addRatioTotal($a_idColonne,$a_format="%.0f%%",$a_type="TYPE_RATIO")
		{
			$this->colonnes[$a_idColonne]['ratioTotal']=array(
				"format"=>$a_format,
				"fonction"=>null,
				"type"=>$a_type,
				"valeur"=>0,
				"valeurFormatee"=>""
			);
		}









//________________________________________________________________________________________________________________
//																			GENERATION DU TABLEAU



/**
 * génération du code HTML de l'entête de colonne avec lien pour le tri
 * @param string $a_idColonne
 * @param unknown $a_libelle
 * @param unknown $a_tri
 * @return string
 */
		private function _headListe($a_idColonne,$a_libelle,$a_tri)
		{
			$this->currentOrd = $this->getConfListe('ord');
			$this->currentSens = $this->getSensListe($this->currentOrd);

			$code = "\t\t<th class=\"".$this->getClassCellule($a_idColonne)." ".$this->getClassHeader($a_idColonne)."\" " . $this->getStylesCellule($a_idColonne)." >";
			if (strlen($a_libelle)>0)
			{
				// avec lien de tri
				if (strlen($a_idColonne)>0 && $a_tri==true )
				{
					if ($this->iconesTri==true)
					{
						if ($this->currentOrd == $a_idColonne && $this->currentSens=="ASC")
							$code .= "<div class=\"th_ico_tri_asc\"><a href=\"".$this->_getUrlOrdreListe($a_idColonne,"ASC")."\">".ucfirst($a_libelle)."</a></div>";
						else if($this->currentOrd == $a_idColonne && $this->currentSens=="DESC")
							$code .= "<div class=\"th_ico_tri_desc\"><a href=\"".$this->_getUrlOrdreListe($a_idColonne,"ASC")."\">".ucfirst($a_libelle)."</a></div>";
						else
							$code .= "<div class=\"th_ico_tri_none\"><a href=\"".$this->_getUrlOrdreListe($a_idColonne,"ASC")."\">".ucfirst($a_libelle)."</a></div>";
					}
					else
						$code .= "<div class=\"th_libelle\"><a href=\"".$this->_getUrlOrdreListe($a_idColonne,"ASC")."\">".ucfirst($a_libelle)."</a></div>";
				}
				// sans le lien de tri
				else
					$code .= "<div class=\"th_libelle\">".$a_libelle."</div>";
			}
			else $code .= "&nbsp;";
			$code .= "</th>\n";
			return ($code);
		}



/**
 * genere le select HTML de selection de filtre pour la colonne
 * @param string $a_idColonne
 * @return string
 */
		private function _filterColonne($a_idColonne)
		{
			$code = "<td>";
			if (is_array($this->colonnes[$a_idColonne]['filter']))
			{
				sort($this->colonnes[$a_idColonne]['filter']);
				$code .= "<select class=\"filtre filtre_".$a_idColonne."\" data-col=\"".$a_idColonne."\">";
				$code .= "<option value=\"NULL\">Filtrer</option>";
				foreach ($this->colonnes[$a_idColonne]['filter'] as $filtre)
				{
					$codeSelected = isset($_SESSION['classe_liste']['filtres'][$a_idColonne]) && $_SESSION['classe_liste']['filtres'][$a_idColonne]==$filtre ? " selected=\"selected\"" : "";
					$code .= "<option value=\"".$filtre."\"".$codeSelected.">".$filtre."</option>";
				}
				$code .= "</select>";
			}
			else
				$code .= "&nbsp;";
			$code .= "</td>";
			return ($code);
		}




/**
 * insert le code passé en paramètre avant une ligne
 * @param int $a_ligne (-1 avant le header, 0 avant la première ligne...)
 * @param string $a_code
 * @return void
 */
	public function insertLigne($a_ligne,$a_code)
	{
		$this->ligneSup[$a_ligne]=$a_code;
	}


/**
 * insert le code passé en paramètre à la fin du tableau
  * @param string $a_code
  * @return void
 */
	public function insertLigneFin($a_code)
	{
		$this->ligneSupFin=$a_code;
	}


/**
 * colonnes à prendre en compte pour la génération
 * @param array $a_listeCols
 * @return false
 */
	public function addGenCols($a_listeCols)
	{
		if (!is_array($a_listeCols))
			$a_listeCols = array($a_listeCols);
		$this->genCols = array_merge($this->genCols,$a_listeCols);
	}




/**
 * colonnes par défaut de la liste
 */
	private function _getDefaultCols()
	{
// 		print "<pre>";print_r($this->datas);print "</pre>";
		$colonnes = array();
		foreach ($this->datas[0] as $col=>$data)
			$this->addColonne($col,$col,$a_format="",$a_type="TYPE_STRING");
	}




/**
 * génération de la liste sous différentes formes
 * @param string $a_format (HTML, JSON, ARRAY (tableau PHP), DATATABLE (Google Charts) ,DEST_MAIL (liste destinataires pour mail : nom prenom<nom@email.com>, ...)
 * @return void
 */
	public function genereListe($a_format="HTML")
	{


		// **********************************************************
		// ERREURS
		// **********************************************************
		if (count($this->datas)==0)
		{
			$this->codeListe = $this->messNoData;
			// code jQuery
			if ($a_format=="HTML" && $this->jQuery)
				$this->codeListe .= $this->_genereJquery();
			return ($this->codeListe);
		}


		if (count($this->colonnes)==0)
		{
// 			$this->codeListe = "Aucune colonne !!";
// 			return ($this->codeListe);
			$this->_getDefaultCols();
		}


		// **********************************************************
		// Préparation des données
		// **********************************************************
		$this->_prepareListe();
// 		var_dump($this->liste); exit();


		// **********************************************************
		// Création de la liste
		// **********************************************************

		switch ($a_format)
		{
			case "HTML":

				// code jQuery
				if ($this->jQuery)
					$this->codeListe = $this->_genereJquery();

				// barre de navigation supérieure
				if ($this->navig==true && ($this->posNavig == "haut" || $this->posNavig == "double") )
					$this->codeListe .= $this->codeNavig;

				// <table>
				$this->codeListe .= "\n<table id=\"".$this->listeName."\" class=\"liste ".implode(" ",$this->listeClassCSS)."\">\n";

				// insertion de ligne
				if (isset($this->ligneSup[-1]))
					$this->codeListe .= $this->ligneSup[-1];

				// entêtes de colonne
				if ($this->header)
				{
					$this->codeListe .= "\t<tr>\n";

					foreach ($this->colonnes as $champ=>$colonne)
					{
// 						print "<pre>";print_r($colonne);print "</pre>";
						$this->codeListe .= $this->_headListe($champ,$colonne['libelle'],$colonne['tri']);
					}
					$this->codeListe .= "\t</tr>\n";
				}
				//filtres
				if ($this->displayFilters)
				{
					$this->codeListe .= "\t<tr>\n";
					foreach ($this->colonnes as $champ=>$colonne)
					{
						$this->codeListe .= $this->_filterColonne($champ);
					}
					$this->codeListe .= "\t</tr>\n";
				}
				// lignes
				$i=1;
				foreach ($this->liste as $ligne)
				{
					$trClasse = $ligne['type'];
					// ajout de classe à la ligne
					if (isset($ligne['trClass']))
						$trClasse .= " ".$ligne['trClass'];


					$this->codeListe .= "\t<tr class=\"".trim($trClasse)."\">\n";
					foreach($ligne['datasFormatees'] as $champ=>$valeur)
					{
						// selon le type de ligne, on ajoute/supprime des classes aux cellules
						$this->removeClassCellule($champ,array('sousTotal','total','ratioTotal'));
						if ($ligne['type']!='source' && $champ!="colonneVide")
							$this->addClassCellule($champ,$ligne['type']);
						$this->codeListe .= "\t\t<td class=\"". $this->getClassCellule($champ)."\" ".$this->getStylesCellule($champ)." valign=\"top\">";
						$this->codeListe .= $valeur;
						$this->codeListe .= "</td>\n";
					}
					$this->codeListe .= "\t</tr>\n";

					if (isset($this->ligneSup[$i]) && !empty($this->ligneSup[$i]))
						$this->codeListe .= $this->ligneSup[$i];

					$i++;
				}


				// insertion de ligne finale
				$this->codeListe .= $this->ligneSupFin;


				$this->codeListe .= "</table>\n";


				// barre de navigation inférieure
				if ($this->navig==true && ($this->posNavig == "bas" || $this->posNavig == "double") )
					$this->codeListe .= $this->codeNavig;



				break;



			case "JSON":

				$listeJson = array();
				$i=0;
				$exValSsTotal = "";
				foreach ($this->liste as $ligne)
				{
					if ($ligne['type']!="source")
						continue;

					// nouvelle ligne de regroupement
					if ($this->displaySousTot && isset($this->paramsLigneSousTotal['colMod']))
					{
						$ssTotCur = $ligne['datas'][$this->paramsLigneSousTotal['colMod']];
						if ($ssTotCur!=$exValSsTotal)
							$i=0;
						$exValSsTotal = $ssTotCur;
					}

					foreach ($ligne['datas'] as $champ=>$value)
					{

						if ($champ=="colonneVide")
							continue;


						// si sousTotal -> regroupement
						if ($this->displaySousTot && isset($this->paramsLigneSousTotal['colMod']))
						{
							if ($champ!=$this->paramsLigneSousTotal['colMod'])
								$listeJson[$ligne['datas'][$this->paramsLigneSousTotal['colMod']]][$i][$champ]=$value;
						}
						// liste simple
						else
						{
							$listeJson[$i][$champ]=$value;
						}
					}

					$i++;
				}
// 				print "<pre>";print_r($listeJson);print "</pre>";exit();
				$this->codeListe = json_encode($listeJson);
				break;



			// tableau PHP
			case "ARRAY":
				$i=0;
				foreach ($this->liste as $ligne)
				{
					if ($ligne['type']!="source")
						continue;
					foreach ($ligne['datas'] as $champ=>$value)
					{
						if ($champ=="colonneVide")
							continue;
						$liste[$i][$champ]=$value;
					}
					$i++;
				}
				$this->codeListe = $liste;
				break;



			// export de liste de destinataires pour l'envoi de mail
			case "DEST_MAIL":
				try
				{
					if (!isset($this->colonnes[$this->destsParams['champEmail']]))
					{
						$exception = "Colonne ".$this->destsParams['champEmail']." non définie";
						throw new Exception($exception);
					}
					else
					{
						$i=0;
						$listeDests = array();
						foreach ($this->liste as $ligne)
						{
							if ($ligne['type']!="source")
								continue;
							foreach ($ligne['datasFormatees'] as $champ=>$value)
							{
								$liste[$i][$champ]=$value;
							}
							array_push($listeDests,trim(sprintf("%s %s <%s>",
								(isset($liste[$i][$this->destsParams['champNom']]) ? $liste[$i][$this->destsParams['champNom']] : ""),
								(isset($liste[$i][$this->destsParams['champPrenom']]) ? $liste[$i][$this->destsParams['champPrenom']] : ""),
								$liste[$i][$this->destsParams['champEmail']]
							)));
							$i++;
						}
						$this->codeListe = implode($this->destsParams['separateur'],$listeDests);
					}
				}
				catch (Exception $e){
					print "<br>".$e->getMessage();
				}
				break;


			// format de données pour Google Charts
			// https://developers.google.com/chart/interactive/docs/reference?hl=fr#dataparam
			case "DATA_TABLE":
				$dataTableCols = array();
				$dataTableRows = array();
//  				var_dump($this->liste);exit();
				// description (colonnes)
				foreach ($this->colonnes as $id=>$colonne)
				{
					if ($id=="colonneVide" || !in_array($id,$this->genCols))
						continue;
					switch ($colonne['type'])
					{
						case "TYPE_STRING":
							$type="string";
							break;
						case "TYPE_NOMBRE":
						case "TYPE_RATIO":
							$type="number";
							break;
					}
					array_push($dataTableCols,array("id"=>$id,"type"=>$type,"label"=>$colonne['libelle']));
				}

				// datas (lignes)
				foreach ($this->liste as $ligne)
				{
					if ($ligne['type']!="source")
						continue;

					$dataTableRow = array();
					foreach ($dataTableCols as $col)
					{
// 						print "<pre>";print_r($ligne);print "</pre>"; exit();

						$dataTableC = array(
								"v"=>$ligne['datas'][$col['id']],
								"f"=>$ligne['datasFormatees'][$col['id']]
								);
						array_push($dataTableRow,$dataTableC);
					}
					array_push($dataTableRows,array("c"=>$dataTableRow));
				}
				$dataTable = array("cols"=>$dataTableCols,"rows"=>$dataTableRows);
				$this->codeListe = json_encode($dataTable);

				break;


		}
		return ($this->codeListe);
	}




/**
 * genere le code Html pour le header seul
 * @return string
 */
		public function genereHeader()
		{
			$codeHeader = "\n<table id=\"".$this->listeName."\" class=\"liste ".$this->_getClass()."\">\n";
			$codeHeader .= "\t<tr>\n";
			// ecriture des entêtes de colonnes
			if (count($this->colonnes)>0)
			{
				foreach($this->colonnes as $champ=>$colonne)
					$codeHeader .= $this->_headListe($champ,$colonne['libelle'],$colonne['tri']);
			}
			$codeHeader .= "\t</tr>\n";
			$codeHeader .= "</table>\n";

			return($codeHeader);
		}



/**
 * génération du code HTML de la navigation et position
 * @param string $a_pos HAUT, BAS, DOUBLE (par défaut)
 * @return void
 */
		public function afficheNavig($a_pos="double")
		{

			if ($a_pos && strlen($a_pos)>0)
			{
				$this->codeNavig = "<div id=\"navig_liste\">";

				$this->codeNavig .= "<div><b>".$this->nbLignes . " r&eacute;sultat". ($this->nbLignes>1 ? "s" : "");
				$this->codeNavig .= !$this->displayAll ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;page :" : "";
				$this->codeNavig .= "</b></div>";


				if (!$this->displayAll)
				{
					$min = ((ceil($this->getConfListe('page')/$this->nbPagesNavig)-1)*$this->nbPagesNavig)+1;
					$max = min (ceil($this->nbLignes/$this->getConfListe('pas')),($min+$this->nbPagesNavig-1));

					if ($min > $this->nbPagesNavig) $this->codeNavig .= "<div><a href=\"".$this->_getUrlPage(1)."\"> << </a></div>";
					if ($min > $this->nbPagesNavig) $this->codeNavig .= "<div><a href=\"".$this->_getUrlPage($min-1)."\"> ... </a></div>";

					for ($l_page=$min; $l_page<=$max; $l_page++)
					{
						$this->codeNavig .= "<div". ($l_page==$this->getConfListe('page') ? " style=\"border:1px solid;\"" : "") .">";
						$this->codeNavig .= "<a href=\"".$this->_getUrlPage($l_page)."\">".$l_page."</a>";
						$this->codeNavig .= "</div>";
					}

					if ($max < ceil($this->nbLignes/$this->getConfListe('pas'))) $this->codeNavig .= "<div><a href=\"".$this->_getUrlPage($max+1)."\"> ... </a></div>";
					if ($max < ceil($this->nbLignes/$this->getConfListe('pas'))) $this->codeNavig .= "<div><a href=\"".$this->_getUrlPage((ceil($this->nbLignes/$this->getConfListe('pas')))) ."\"> >> </a></div>";

					// selection du nombre de lignes par page
					if ($this->selectPas == true)
						$this->codeNavig .= $this->codePas;
				}


				$this->codeNavig .= "</div>";
				$this->codeNavig .= "<div style=\"clear:both;height:0px;\"></div>";

				$this->navig=true;
				$this->posNavig = $a_pos;
			}
		}



/**
 * paramétrage du nombre de liens affichées dans la pagination
 * @param int $a_nb : nombre de pages à afficher
 * 		  ex avec $a_nb=10 : << ... 11 12 13 14 15 16 17 18 19 20 ... >>
 * @return void
 */
		public function setNbPagesNavig($a_nb)
		{
			$this->nbPagesNavig = $a_nb;
		}



/**
 * nombre de lignes affichées par page
 * @param string $a_liste = liste des nombres ex: array(10,20,30,50,100)
 * @return void
 */
		public function afficheSelectPas($a_liste=false)
		{
// 			print "<pre>";print_r($_SESSION['classe_liste']);print "</pre>";
			if (!$a_liste)
				$a_liste = array(10,20,30,50,100);
			// code de retour
			$code = "<div class=\"select_pas\">afficher ";
			$code .= "<select class=\"liste_select_pas\">";
			foreach ($a_liste as $item)
				$code .= "<option value=\"".$item."\"".($_SESSION['classe_liste']['pas']==$item ? " selected=\"selected\"" : "").">".$item."</option>";
			$code .= "</select> lignes";
			$code .= "</div>";
			$this->selectPas = true;
			$this->codePas = $code;
			$argsSup = $this->_removeArguments(array('pas','page'));
			// code jQuery
			$this->codeJquery = "$('.liste_select_pas').change(function(){location.href='".CURRENT_PAGE."?".(strlen($argsSup)>0 ? $argsSup."&" : "")."pas='+$(this).val()+'&page=1';});";

		}






/*_______________________________________________________________________________________________________________
																				FONCTIONS D'EXPORT
*/


/**
 * crée une nouvelle feuille et y insert la liste pour l'export XLS
 * @param string $a_nom nom de la feuille
 * @return void
 */
		function addFeuilleXls($a_nom)
		{
			$this->_prepareListe();
			array_push($this->feuillesXls,array(
				'nom'=>$a_nom,
				'colonnes'=>$this->colonnes,
				'datas'=>$this->liste
			));
			$this->liste = array();
		}





/**
 * export du tableau
 * @param string $a_format (CSV,PDF,XLS)
 * @param string $a_sens (P:portrait, L:landscape)
 * @param string $a_encoding type d'encoding
 * @param string $a_output upload du fichier (UPLOAD) ou enregistrement (SAVE)
 * @throws Exception
 * @return void
 */
		public function export($a_format,$a_sens="P",$a_encoding=null,$a_output="UPLOAD")
		{
			$this->sensExportPage = $a_sens;
			$this->encodeExport = $a_encoding;
			$this->outputExport=$a_output;
			$exception = "";
			try	{
				// export etiquettes
				if (in_array($a_format,array("ETIQUETTE","PDF")) && !class_exists("HTML2PDF"))
					$exception = "Classe HTML2PDF non trouvée";
				// export tableaux
				else if ($a_format!="DEST_MAIL" && !class_exists("PHPExcel"))
					$exception = "Classe PHPExcel non trouvée";
				// exception
				if (strlen($exception)>0)
					throw new Exception($exception);
				else
					$this->exportFile($a_format);
			}
			catch (Exception $e){
				print "<br>".$e->getMessage();
			}

		}




/**
 * ajout de contenu pour l'export
 * @param string $a_file fichier à inclure
 * @return void
 */
		public function addContenuExport($a_file)
		{
			$this->codeListe = file_get_contents($a_file);
		}




/**
 * nom du fichier d'export (sans extension)
 * @param string $a_name
 * @return void
 */
		public function setNomFichierExport($a_name)
		{
			$this->nomFichierExport = $a_name;
		}





/**
 * répertoire de sauvegarde des exports
 * @param string $a_dir
 * @return void
 */
		public function setExportDir($a_dir)
		{
			$this->exportDir = $a_dir;
		}





/**
 * creation du fichier export
 * @param string $a_format ETIQUETTE, PDF, XLS, CSV
 * @return void
 */
		public function exportFile($a_format)
		{
				// augmentation de la mémoire pour l'execution du script
				// pour les grosses listes
				ini_set("memory_limit",-1);

				if ($a_format=="ETIQUETTE")
				{
					$this->_prepareListe();

					$col=1;
					$etiquettes = "";

					$nbEtiquettesTot = count($this->liste);
					$nbEtiquettesParPage = $this->nbEtiquettesParPage[0]*$this->nbEtiquettesParPage[1];

					if (file_exists($this->labelsPrintPageCssFile))
						$buffer = "<style>".file_get_contents($this->labelsPrintPageCssFile)."</style>";
					$etiquette = 0;

					while($etiquette<$nbEtiquettesTot)
					{
						$etiquettesPage = "<table class=\"labelsPrintPage\">";
						// lignes
						for ($lig=1;$lig<=$this->nbEtiquettesParPage[1];$lig++)
						{
							$etiquettesPage .= "<tr>";
							// colonnes
							for ($col=1;$col<=$this->nbEtiquettesParPage[0];$col++)
							{
								// cases vides si on a déjà toutes nos étiquettes
								if ($etiquette>$nbEtiquettesTot-1)
									$etiquettesPage .= "<td>&nbsp;</td>";
								// contenu d'une étiquette
								else
								{
									$contenuEtiquette = "";
									foreach ($this->colonnes as $champ=>$colonne)
										$contenuEtiquette .= $this->liste[$etiquette]['datasFormatees'][$champ]."<br />";
									$etiquettesPage .= "<td>".$contenuEtiquette."</td>";
								}
								$etiquette++;
							}
							$etiquettesPage .= "</tr>";
						}
						$etiquettesPage .= "</table>";
						$buffer .= "<page>".$etiquettesPage."</page>";
					}

					// On génére
					$this->export = new HTML2PDF($this->sensExportPage,'A4','fr',array(0,0,0,0));
					$this->export->WriteHTML($buffer);
					$this->export->Output();
				}

				else if ($a_format=="PDF")
				{
					$buffer = $this->genereListe();
					$this->export = new HTML2PDF($this->sensExportPage,'A4','fr',array(0,0,0,0));
					if (!is_null($this->encodeExport))
						$this->export->setEncoding($this->encodeExport);

					$this->export->WriteHTML($buffer);

					switch ($this->outputExport)
					{
						case "UPLOAD":
							$this->export->Output();
							break;

						case "SAVE":
							$this->export->Output($this->exportDir.$this->nomFichierExport.".pdf","F");
							break;
					}
				}

				else if ($a_format=="DEST_MAIL")
				{
					header('Content-type: text/plain');
					print $this->genereListe($a_format);
				}

				else
				{
					$this->export = new PHPExcel();


					$nbFeuilles = count($this->feuillesXls);
					// on crée au moins une feuille XLS
					if ($nbFeuilles==0)
					{
						$this->addFeuilleXls(substr($this->nomFichierExport,0,30));
						$nbFeuilles = 1;
					}
					// on supprime la feuille créée par défaut
					$this->export->removeSheetByIndex(0);

					// on exporte toutes les feuilles
					for ($i=0;$i<$nbFeuilles;$i++)
					{
						// infos de la feuille à créer
						$feuille = $this->feuillesXls[$i];


						// si on a plusieurs feuilles, on exporte seulement les feuilles avec données
						if ($nbFeuilles>1 && count($feuille['datas'])==0)
							continue;

						$sheet = $this->export->createSheet();
						$sheet->setTitle($feuille['nom']);

						$noLigne = 1;
						// ligne d'entête
						$noColonne = 0;
						foreach($feuille['colonnes'] as $colonne)
						{
							$sheet->setCellValueByColumnAndRow($noColonne,$noLigne,$colonne['libelle']);
							$noColonne++;
						}
						$noLigne++;
// 						print "<pre>";print_r($feuille['colonnes']);print "</pre>";
// 						print "<pre>";print_r($feuille['datas']);print "</pre>";
// 						exit();
						// lignes de résultats
						foreach($feuille['datas'] as $data)
						{
							if ($data['type']=='source')
							{
								$noColonne = 0;
								foreach($feuille['colonnes'] as $champ=>$colonne)
								{
										$sheet->setCellValueByColumnAndRow($noColonne,$noLigne,$data['datasFormatees'][$champ]);
										$noColonne++;
								}
								$noLigne++;
							}
						}
					}

					switch($a_format)
					{
						case "XLS":
							$writer = new PHPExcel_Writer_Excel5($this->export);
							$extension = ".xls";
							$contentType = "application/vnd.ms-excel";
							$contentDispo = "inline;filename=".$this->nomFichierExport.$extension;
							break;

						case "CSV":
							$writer = new PHPExcel_Writer_CSV($this->export);
							$writer->setDelimiter(';');
							$writer->setEnclosure('');
							$writer->setUseBOM(true);
							$extension = ".csv";
							$contentType = "application/csv";
							$contentDispo = "inline;filename=".$this->nomFichierExport.$extension;
							break;
					}

					switch($this->outputExport)
					{
						case "UPLOAD":
							header("Content-type: ".$contentType);
							header("Content-Disposition:".$contentDispo);
							$writer->save('php://output');
							break;

						case "SAVE":
							try {
								if (!is_dir($this->exportDir))
									throw new Exception($this->exportDir." n'est pas un r&eacute;pertoire");
								else if (!is_writable($this->exportDir))
									throw new Exception("Impossible d'&eacute;crire sur ".$this->exportDir);
								else
								{
									$this->exportDir = rtrim($this->exportDir,"\\.");
									$writer->save($this->exportDir.$this->nomFichierExport.$extension);
								}
							}
							catch (Exception $e){
								print "<br>".$e->getMessage();
							}
							break;
					}

				}

		}





}
